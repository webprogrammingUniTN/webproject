# Package Description

## com.soapandtherest.shoppinglist.data.commons
- **ImplementationProvider:**
	Utility class designed to provide the implementation of a given interface. The implementation class name is determined using a "pattern" that must be given to the constructor.
	```java
	Constructor
	@param pattern
	```
	the implementation class name pattern (supposed to contain 2 "%s") for example "%s.impl.jdbc.%sImpl" where the first "%s" will be replaced by the interface package name the second "%s" will be replaced by the interface simple class name
- **ListItem:**
	This interface defines an item identified by a "value" and having a "label". This kind of item is designed to be used in a list populating a listbox or a combobox in a GUI.


## com.soapandtherest.shoppinglist.data.persistence
Questo package contiene in pratica una mappatura delle entities del database su delle interfacce contenenti ognuna le firme dei metodi necessari per ottenere tutti i record di una tabella, ottenere i record di una tabella in  base all'id, contare i recordi contenuti in una tabella, salvare un determinato record all'interno di una specifica tabella, aggiornare il record di una specifica tabella, creare un nuovo record, eliminare un record in base all'id, determinare se un determinato record esiste (il parametro sul quale è basato il metodo è l'id dell'ipotetico record) e determinare se una specifica entità esiste o meno.

##com.soapandtherest.shoppinglist.data.persistence.commons
Questo package contiene in pratica le classi necessarie per determinare il tipo di servizio che andrà ad interagire con il database (es. jdbc)<br />
NOTA: tale package sfrutta quindi la classe ImplementationProvider definita nel package data.commons per determinare il tipo servizio

## com.soapandtherest.shoppinglist.data.persistence.impl.fake
In questo package sono contenute le classi necessarie nella fase di test. Si può dire che sia uno specchio del package com.soapandtherest.shoppinglist.data.persistence.impl.jdbc con la differenza che anzichè utilizzare direttamente il database utilizzera una HashTable come struttura dati per operare i test su dei "fake" record che saranno inseriti in tale struttura

## com.soapandtherest.shoppinglist.data.persistence.impl-fake.commons
In questo pakage è contenuto il DAO che verrà utilizzato nella fase di testing

## com.soapandtherest.shoppinglist.data.persistence.impl.jdbc
In questo package sono contenute le classi che implementano le interfacce contenute nel package com.soapandtherest.shoppinglist.data.persistence, lo scopo delle classi in questo package inoltre hanno lo scopo di implementare l'interfacciamento con il db tramite jdbc ridefinendo i metodi contenuti all'interno della classe astratta GenericJdbcDAO nel package com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons

## com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons
In questo package è contenuta la classe astratta GenericDAO e la classe DataSourceProvider, le quali danno gli strumenti necessari e le operazioni comuni a tutte le entities per poter comunicare con il database, istanzia inoltre la connessione effettiva con quest'ultimo.

## com.soapandtherest.shoppinglist.data.record
Questo package contiene la mappatura delle entities in javabeans, ovvero in classi dove semplicemente si mappano i campi di ogni tabella in attributi, con tanto di metodi getter e setter per ogni attributo e infine il metodo toString. Tali classi possono essere usate sia come web form che come persistence record

## com.soapandtherest.shoppinglist.data.record.listitem
Questo package contiene la mappatura delle entites in classi che permettono attraverso l'interfaccia ListItem nel package com.soapandtherest.shoppinglist.data.commons di poter listare per ogni entità tutti i suoi attributi. Queste classi sono molto utili in quanto semplificano il lavoro nelle operazioni di popolazione di una listbox o una combobox a livello GUI.	
