## Config database
To config application database  you need to copy file `jdbc.example.properties` as `jdbc.properties` and change property, if you use the `sql/create.sql` script to create db and user you need to correct only the server address
## Database update
### Maven Command:
Maven update is automatic during compile time
### Manual Command:
```bash
liquibase-3/liquibase --driver=com.mysql.cj.jdbc.Driver --classpath="~/.m2/repository/mysql/mysql-connector-java/8.0.11/mysql-connector-java-8.0.11.jar" --changeLogFile="shoppinglist-data/src/main/resources/mysql-liquibase-changelog.xml" --url="jdbc:mysql://localhost:3306/shoppinglist_test?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC" --username="root" --password="password" update
```