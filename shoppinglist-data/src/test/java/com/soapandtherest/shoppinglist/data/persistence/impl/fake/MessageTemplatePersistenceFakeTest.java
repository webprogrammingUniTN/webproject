/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:41 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence.impl.fake;

import com.soapandtherest.shoppinglist.data.persistence.MessageTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.MessageTemplatePersistenceGenericTest;
import org.junit.Test;

/**
 * JUnit tests for MessageTemplate persistence service
 *
 * @author Telosys Tools
 *
 */
public class MessageTemplatePersistenceFakeTest extends MessageTemplatePersistenceGenericTest {

	@Test
	public void testPersistenceService() {
		System.out.println("test MessageTemplatePersistenceFake ");

		MessageTemplatePersistence persistenceService = new MessageTemplatePersistenceFake();

		testPersistenceServiceWithAutoincrementedKey(persistenceService);

	}
}
