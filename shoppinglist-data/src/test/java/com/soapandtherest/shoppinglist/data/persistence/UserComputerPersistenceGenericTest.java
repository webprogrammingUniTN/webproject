/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:43 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.soapandtherest.shoppinglist.data.record.UserComputerRecord;
import com.soapandtherest.shoppinglist.data.persistence.UserComputerPersistence;
import java.sql.SQLException;

/**
 * Generic test class for a persistence service
 *
 * @author Telosys
 *
 */
public class UserComputerPersistenceGenericTest {

	/**
	 * Generic test for a persistence service
	 *
	 * @param persistenceService
	 * @throws SQLException
	 */
	public void testPersistenceService(UserComputerPersistence persistenceService) {
		System.out.println("--- test UserComputerPersistence ");

		UserComputerRecord userComputer = new UserComputerRecord();
		// --- Key values
		userComputer.setIdComputer(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"); // "id_computer"
																															// :
																															// java.lang.String
		userComputer.setUserIduser(Long.valueOf(1000L)); // "user_iduser" : java.lang.Long
		// --- Other values
		userComputer.setRemember(Boolean.valueOf(true)); // "remember" : java.lang.Boolean
		userComputer.setCreateTime(java.sql.Timestamp.valueOf("2001-05-21 01:46:52")); // "create_time" : java.util.Date
		userComputer.setUpdateTime(java.sql.Timestamp.valueOf("2001-05-21 01:46:52")); // "update_time" : java.util.Date

		// --- DELETE
		System.out.println("Delete : " + userComputer);
		persistenceService.delete(userComputer); // Just to be sure it doesn't exist before insert

		long initialCount = persistenceService.countAll();
		System.out.println("Initial count = " + initialCount);

		// --- CREATE
		System.out.println("Create : " + userComputer);

		persistenceService.create(userComputer);

		assertTrue(persistenceService.exists(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Long.valueOf(1000L)));

		assertTrue(persistenceService.exists(userComputer));
		long count = persistenceService.countAll();
		System.out.println("Count = " + count);
		assertEquals(initialCount + 1, count);

		// --- FIND
		System.out.println("Find by id...");

		UserComputerRecord userComputer2 = persistenceService.findById(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Long.valueOf(1000L));

		System.out.println("Found : " + userComputer2);
		assertNotNull(userComputer2);
		assertTrue(userComputer2.getIdComputer().equals(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
		assertTrue(userComputer2.getUserIduser().equals(Long.valueOf(1000L)));
		assertTrue(persistenceService.exists(userComputer2));

		// --- UPDATE
		// --- Change values
		userComputer2.setRemember(Boolean.valueOf(false)); // "remember" : java.lang.Boolean
		userComputer2.setCreateTime(java.sql.Timestamp.valueOf("2002-05-21 02:46:52")); // "create_time" :
																						// java.util.Date
		userComputer2.setUpdateTime(java.sql.Timestamp.valueOf("2002-05-21 02:46:52")); // "update_time" :
																						// java.util.Date
		System.out.println("Update : " + userComputer2);
		assertTrue(persistenceService.update(userComputer2));

		// --- RELOAD AFTER UPDATE
		System.out.println("Find by id...");

		UserComputerRecord userComputer3 = persistenceService.findById(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Long.valueOf(1000L));
		assertNotNull(userComputer3);
		System.out.println("Found : " + userComputer3);

		// Check same data in the reloaded instance
		assertEquals(userComputer2.getRemember(), userComputer3.getRemember());
		// assertEquals(userComputer2.getCreateTime(), userComputer3.getCreateTime() );
		// // Cannot test timestamp (different class in initial value)
		// assertEquals(userComputer2.getUpdateTime(), userComputer3.getUpdateTime() );
		// // Cannot test timestamp (different class in initial value)

		// --- DELETE
		System.out.println("Delete : " + userComputer2);
		assertTrue(persistenceService.delete(userComputer2)); // Delete #1 : OK
		assertFalse(persistenceService.delete(userComputer2)); // Nothing (already deleted)
		assertFalse(persistenceService.deleteById(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Long.valueOf(1000L))); // Nothing (already deleted)

		long finalCount = persistenceService.countAll();
		System.out.println("Final count = " + finalCount);
		assertEquals(initialCount, finalCount);

		assertFalse(persistenceService.exists(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Long.valueOf(1000L)));
		assertFalse(persistenceService.exists(userComputer2));
		userComputer2 = persistenceService.findById(
				"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
				Long.valueOf(1000L));
		assertNull(userComputer2);

		System.out.println("Normal end of persistence service test.");
	}
}
