/*
 * Created on 2018-06-16 ( Date ISO 2018-06-16 - Time 12:55:35 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */

package com.soapandtherest.shoppinglist.data.record;

import org.junit.Assert;
import org.junit.Test;

/**
 * JUnit test case for bean ShoppingListTemplateHasProductTemplateRecord
 *
 * @author Telosys Tools Generator
 *
 */
public class ShoppingListTemplateHasProductTemplateRecordTest {

	@Test
	public void testSettersAndGetters() {

		System.out.println("Checking class ShoppingListTemplateHasProductTemplateRecord getters and setters ...");

		ShoppingListTemplateHasProductTemplateRecord shoppingListTemplateHasProductTemplateRecord = new ShoppingListTemplateHasProductTemplateRecord();

		// --- Test setter/getter for attribute
		// "shoppingListTemplateIdshoppingListTemplate" ( model type : Long /
		// wrapperType : Long )
		shoppingListTemplateHasProductTemplateRecord.setShoppingListTemplateIdshoppingListTemplate(Long.valueOf(1000L));
		Assert.assertEquals(Long.valueOf(1000L),
				shoppingListTemplateHasProductTemplateRecord.getShoppingListTemplateIdshoppingListTemplate()); // Not
		// primitive
		// type
		// in
		// model

		// --- Test setter/getter for attribute "productTemplateIdproductTemplate" (
		// model type : Long / wrapperType : Long )
		shoppingListTemplateHasProductTemplateRecord.setProductTemplateIdproductTemplate(Long.valueOf(1000L));
		Assert.assertEquals(Long.valueOf(1000L),
				shoppingListTemplateHasProductTemplateRecord.getProductTemplateIdproductTemplate()); // Not primitive
		// type in model

		// --- Test setter/getter for attribute "createTime" ( model type : Date /
		// wrapperType : Date )
		shoppingListTemplateHasProductTemplateRecord.setCreateTime(java.sql.Timestamp.valueOf("2001-05-21 01:46:52"));
		Assert.assertEquals(java.sql.Timestamp.valueOf("2001-05-21 01:46:52"),
				shoppingListTemplateHasProductTemplateRecord.getCreateTime()); // Not primitive type in model

		// --- Test setter/getter for attribute "updateTime" ( model type : Date /
		// wrapperType : Date )
		shoppingListTemplateHasProductTemplateRecord.setUpdateTime(java.sql.Timestamp.valueOf("2001-05-21 01:46:52"));
		Assert.assertEquals(java.sql.Timestamp.valueOf("2001-05-21 01:46:52"),
				shoppingListTemplateHasProductTemplateRecord.getUpdateTime()); // Not primitive type in model

	}

}
