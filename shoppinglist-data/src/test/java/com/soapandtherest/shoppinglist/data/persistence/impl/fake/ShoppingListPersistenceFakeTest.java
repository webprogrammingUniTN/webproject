/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:42 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence.impl.fake;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistenceGenericTest;
import org.junit.Test;

/**
 * JUnit tests for ShoppingList persistence service
 *
 * @author Telosys Tools
 *
 */
public class ShoppingListPersistenceFakeTest extends ShoppingListPersistenceGenericTest {

	@Test
	public void testPersistenceService() {
		System.out.println("test ShoppingListPersistenceFake ");

		ShoppingListPersistence persistenceService = new ShoppingListPersistenceFake();

		testPersistenceServiceWithAutoincrementedKey(persistenceService);

	}
}
