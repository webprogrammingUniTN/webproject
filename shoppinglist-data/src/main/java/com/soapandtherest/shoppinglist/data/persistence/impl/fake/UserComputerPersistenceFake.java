/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:43 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence.impl.fake;

import java.util.List;

import javax.inject.Named;

import com.soapandtherest.shoppinglist.data.record.UserComputerRecord;
import com.soapandtherest.shoppinglist.data.persistence.UserComputerPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.fake.commons.GenericFakeDAO;

/**
 * UserComputer persistence service - FAKE implementation
 *
 * @author Telosys
 *
 */
@Named("UserComputerPersistence")
public class UserComputerPersistenceFake extends GenericFakeDAO<UserComputerRecord> implements UserComputerPersistence {

	/**
	 * Constructor
	 */
	public UserComputerPersistenceFake() {
		super(UserComputerRecord.class);
	}

	/**
	 * Creates a new bean instance and set its primary key value(s)
	 *
	 * @param idComputer
	 * @param userIduser
	 * @return the new instance
	 */
	private UserComputerRecord newInstanceWithPrimaryKey(String idComputer, Long userIduser) {
		UserComputerRecord record = new UserComputerRecord();
		record.setIdComputer(idComputer);
		record.setUserIduser(userIduser);
		return record;
	}

	// -------------------------------------------------------------------------------------
	// Generic DAO abstract methods implementations
	// -------------------------------------------------------------------------------------
	@Override
	protected String getKey(UserComputerRecord record) {
		return buildKeyString(record.getIdComputer(), record.getUserIduser());
	}

	@Override
	protected void setAutoIncrementedKey(UserComputerRecord record, long value) {
		throw new IllegalStateException("Unexpected call to method 'setAutoIncrementedKey'");
	}

	// -------------------------------------------------------------------------------------
	// Persistence interface implementations
	// -------------------------------------------------------------------------------------
	@Override
	public long countAll() {
		return super.doCountAll();
	}

	@Override
	public UserComputerRecord create(UserComputerRecord record) {
		super.doCreate(record);
		return record;
	}

	@Override
	public boolean delete(UserComputerRecord record) {
		return super.doDelete(record);
	}

	@Override
	public boolean deleteById(String idComputer, Long userIduser) {
		UserComputerRecord record = newInstanceWithPrimaryKey(idComputer, userIduser);
		return super.doDelete(record);
	}

	@Override
	public boolean exists(UserComputerRecord record) {
		return super.doExists(record);
	}

	@Override
	public boolean exists(String idComputer, Long userIduser) {
		UserComputerRecord record = newInstanceWithPrimaryKey(idComputer, userIduser);
		return super.doExists(record);
	}

	@Override
	public List<UserComputerRecord> findAll() {
		return super.doFindAll();
	}

	@Override
	public UserComputerRecord findById(String idComputer, Long userIduser) {
		UserComputerRecord record = newInstanceWithPrimaryKey(idComputer, userIduser);
		return super.doFind(record);
	}

	@Override
	public UserComputerRecord save(UserComputerRecord record) {
		if (super.doExists(record)) {
			super.doUpdate(record);
		} else {
			super.doCreate(record);
		}
		return record;
	}

	@Override
	public boolean update(UserComputerRecord record) {
		return super.doUpdate(record);
	}
}
