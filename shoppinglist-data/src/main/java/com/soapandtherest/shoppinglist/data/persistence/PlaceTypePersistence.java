/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:41 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence;

import java.util.List;

import com.soapandtherest.shoppinglist.data.record.PlaceTypeRecord;

/**
 * Persistence Interface for PlaceTypeRecord.
 */
public interface PlaceTypePersistence {

	/**
	 * Tries to find an entity using its Id / Primary Key
	 * 
	 * @param placeIdentifier
	 * @return entity
	 */
	PlaceTypeRecord findById(String placeIdentifier);

	/**
	 * Finds all entities.
	 * 
	 * @return all entities
	 */
	List<PlaceTypeRecord> findAll();

	/**
	 * Counts all the records present in the database
	 * 
	 * @return
	 */
	public long countAll();

	/**
	 * Saves the given entity in the database (create or update)
	 * 
	 * @param entity
	 * @return entity
	 */
	PlaceTypeRecord save(PlaceTypeRecord entity);

	/**
	 * Updates the given entity in the database
	 * 
	 * @param entity
	 * @return true if the entity has been updated, false if not found and not
	 *         updated
	 */
	boolean update(PlaceTypeRecord entity);

	/**
	 * Creates the given entity in the database
	 * 
	 * @param entity
	 * @return
	 */
	PlaceTypeRecord create(PlaceTypeRecord entity);

	/**
	 * Deletes an entity using its Id / Primary Key
	 * 
	 * @param placeIdentifier
	 * @return true if the entity has been deleted, false if not found and not
	 *         deleted
	 */
	boolean deleteById(String placeIdentifier);

	/**
	 * Deletes an entity using the Id / Primary Key stored in the given object
	 * 
	 * @param entity
	 *            to be deleted (supposed to have a valid Id/PK )
	 * @return true if the entity has been deleted, false if not found and not
	 *         deleted
	 */
	boolean delete(PlaceTypeRecord entity);

	/**
	 * Returns true if an entity exists with the given Id / Primary Key
	 * 
	 * @param placeIdentifier
	 * @return
	 */
	public boolean exists(String placeIdentifier);

	/**
	 * Returns true if the given entity exists
	 * 
	 * @param entity
	 * @return
	 */
	public boolean exists(PlaceTypeRecord entity);

}
