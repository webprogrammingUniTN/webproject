/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:43 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence.impl.fake;

import java.util.List;

import javax.inject.Named;

import com.soapandtherest.shoppinglist.data.record.UserRecord;
import com.soapandtherest.shoppinglist.data.persistence.UserPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.fake.commons.GenericFakeDAO;

/**
 * User persistence service - FAKE implementation
 *
 * @author Telosys
 *
 */
@Named("UserPersistence")
public class UserPersistenceFake extends GenericFakeDAO<UserRecord> implements UserPersistence {

	/**
	 * Constructor
	 */
	public UserPersistenceFake() {
		super(UserRecord.class);
	}

	/**
	 * Creates a new bean instance and set its primary key value(s)
	 *
	 * @param iduser
	 * @return the new instance
	 */
	private UserRecord newInstanceWithPrimaryKey(Long iduser) {
		UserRecord record = new UserRecord();
		record.setIduser(iduser);
		return record;
	}

	// -------------------------------------------------------------------------------------
	// Generic DAO abstract methods implementations
	// -------------------------------------------------------------------------------------
	@Override
	protected String getKey(UserRecord record) {
		return buildKeyString(record.getIduser());
	}

	@Override
	protected void setAutoIncrementedKey(UserRecord record, long value) {
		// record.setIduser((Long)value);
		record.setIduser(value);
	}

	// -------------------------------------------------------------------------------------
	// Persistence interface implementations
	// -------------------------------------------------------------------------------------
	@Override
	public long countAll() {
		return super.doCountAll();
	}

	@Override
	public UserRecord create(UserRecord record) {
		super.doCreateAutoIncremented(record);
		return record;
	}

	@Override
	public boolean delete(UserRecord record) {
		return super.doDelete(record);
	}

	@Override
	public boolean deleteById(Long iduser) {
		UserRecord record = newInstanceWithPrimaryKey(iduser);
		return super.doDelete(record);
	}

	@Override
	public boolean exists(UserRecord record) {
		return super.doExists(record);
	}

	@Override
	public boolean exists(Long iduser) {
		UserRecord record = newInstanceWithPrimaryKey(iduser);
		return super.doExists(record);
	}

	@Override
	public List<UserRecord> findAll() {
		return super.doFindAll();
	}

	@Override
	public UserRecord findById(Long iduser) {
		UserRecord record = newInstanceWithPrimaryKey(iduser);
		return super.doFind(record);
	}

	@Override
	public UserRecord save(UserRecord record) {
		if (super.doExists(record)) {
			super.doUpdate(record);
		} else {
			super.doCreateAutoIncremented(record);
		}
		return record;
	}

	@Override
	public boolean update(UserRecord record) {
		return super.doUpdate(record);
	}
}
