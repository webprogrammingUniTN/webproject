/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:15:57 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */

package com.soapandtherest.shoppinglist.data.persistence.impl.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import com.soapandtherest.shoppinglist.data.record.DatabasechangeloglockRecord;
import com.soapandtherest.shoppinglist.data.persistence.DatabasechangeloglockPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons.GenericJdbcDAO;

/**
 * Databasechangeloglock persistence implementation
 *
 * @author Telosys
 *
 */
@Named("DatabasechangeloglockPersistence")
public class DatabasechangeloglockPersistenceJdbc extends GenericJdbcDAO<DatabasechangeloglockRecord>
		implements
			DatabasechangeloglockPersistence {

	private final static String SQL_SELECT_ALL = "select ID, LOCKED, LOCKGRANTED, LOCKEDBY from DATABASECHANGELOGLOCK";

	private final static String SQL_SELECT = "select ID, LOCKED, LOCKGRANTED, LOCKEDBY from DATABASECHANGELOGLOCK where ID = ?";

	private final static String SQL_INSERT = "insert into DATABASECHANGELOGLOCK ( ID, LOCKED, LOCKGRANTED, LOCKEDBY ) values ( ?, ?, ?, ? )";

	private final static String SQL_UPDATE = "update DATABASECHANGELOGLOCK set LOCKED = ?, LOCKGRANTED = ?, LOCKEDBY = ? where ID = ?";

	private final static String SQL_DELETE = "delete from DATABASECHANGELOGLOCK where ID = ?";

	private final static String SQL_COUNT_ALL = "select count(*) from DATABASECHANGELOGLOCK";

	private final static String SQL_COUNT = "select count(*) from DATABASECHANGELOGLOCK where ID = ?";

	private final static List<String> SEARCHABLE_FIELD;

	private final static List<String> SORTABLE_FIELD;

	static {
		SEARCHABLE_FIELD = new ArrayList<String>();
		SORTABLE_FIELD = new ArrayList<String>();
	}
	// ----------------------------------------------------------------------
	/**
	 * DAO constructor
	 */
	public DatabasechangeloglockPersistenceJdbc() {
		super();
	}

	// ----------------------------------------------------------------------
	@Override
	protected void setAutoIncrementedKey(DatabasechangeloglockRecord record, long value) {
		throw new IllegalStateException("Unexpected call to method 'setAutoIncrementedKey'");
	}

	// ----------------------------------------------------------------------
	@Override
	protected void setValuesForPrimaryKey(PreparedStatement ps, int i,
			DatabasechangeloglockRecord databasechangeloglock) throws SQLException {
		// --- Set PRIMARY KEY from bean to PreparedStatement ( SQL "WHERE key=?, ..." )
		setValue(ps, i++, databasechangeloglock.getId()); // "ID" : java.lang.Integer
	}

	// ----------------------------------------------------------------------
	@Override
	protected void setValuesForInsert(PreparedStatement ps, int i, DatabasechangeloglockRecord databasechangeloglock)
			throws SQLException {
		// --- Set PRIMARY KEY and DATA from bean to PreparedStatement ( SQL "SET x=?,
		// y=?, ..." )
		setValue(ps, i++, databasechangeloglock.getId()); // "ID" : java.lang.Integer
		setValue(ps, i++, databasechangeloglock.getLocked()); // "LOCKED" : java.lang.Boolean
		setValue(ps, i++, databasechangeloglock.getLockgranted()); // "LOCKGRANTED" : java.util.Date
		setValue(ps, i++, databasechangeloglock.getLockedby()); // "LOCKEDBY" : java.lang.String
	}

	// ----------------------------------------------------------------------
	@Override
	protected void setValuesForUpdate(PreparedStatement ps, int i, DatabasechangeloglockRecord databasechangeloglock)
			throws SQLException {
		// --- Set DATA from bean to PreparedStatement ( SQL "SET x=?, y=?, ..." )
		setValue(ps, i++, databasechangeloglock.getLocked()); // "LOCKED" : java.lang.Boolean
		setValue(ps, i++, databasechangeloglock.getLockgranted()); // "LOCKGRANTED" : java.util.Date
		setValue(ps, i++, databasechangeloglock.getLockedby()); // "LOCKEDBY" : java.lang.String
		// --- Set PRIMARY KEY from bean to PreparedStatement ( SQL "WHERE key=?, ..." )
		setValue(ps, i++, databasechangeloglock.getId()); // "ID" : java.lang.Integer
	}

	// ----------------------------------------------------------------------
	/**
	 * Creates a new instance of the bean and populates it with the given primary
	 * value(s)
	 *
	 * @param id;
	 * @return the new instance
	 */
	private DatabasechangeloglockRecord newInstanceWithPrimaryKey(Integer id) {
		DatabasechangeloglockRecord databasechangeloglock = new DatabasechangeloglockRecord();
		databasechangeloglock.setId(id);
		return databasechangeloglock;
	}

	// ----------------------------------------------------------------------
	@Override
	protected DatabasechangeloglockRecord newInstance() {
		return new DatabasechangeloglockRecord();
	}

	// ----------------------------------------------------------------------
	@Override
	protected DatabasechangeloglockRecord populateBean(ResultSet rs, DatabasechangeloglockRecord databasechangeloglock)
			throws SQLException {

		// --- Set data from ResultSet to Bean attributes
		databasechangeloglock.setId(rs.getInt("ID")); // java.lang.Integer
		if (rs.wasNull()) {
			databasechangeloglock.setId(null);
		} ; // not primitive number => keep null value if any
		databasechangeloglock.setLocked(rs.getBoolean("LOCKED")); // java.lang.Boolean
		databasechangeloglock.setLockgranted(rs.getDate("LOCKGRANTED")); // java.util.Date
		databasechangeloglock.setLockedby(rs.getString("LOCKEDBY")); // java.lang.String
		return databasechangeloglock;
	}

	// ----------------------------------------------------------------------
	/*
	 * (non-Javadoc)
	 *
	 * @see interface
	 */
	@Override
	public DatabasechangeloglockRecord findById(Integer id) {
		DatabasechangeloglockRecord databasechangeloglock = newInstanceWithPrimaryKey(id);
		if (super.doSelect(databasechangeloglock)) {
			return databasechangeloglock;
		} else {
			return null; // Not found
		}
	}
	// ----------------------------------------------------------------------
	/*
	 * (non-Javadoc)
	 *
	 * @see interface
	 */
	@Override
	public List<DatabasechangeloglockRecord> findAll() {
		return super.doSelectAll();
	}

	// ----------------------------------------------------------------------
	/**
	 * Loads the given bean, it is supposed to contains the primary key value(s) in
	 * its attribute(s)<br>
	 * If found, the given instance is populated with the values retrieved from the
	 * database<br>
	 * If not found, the given instance remains unchanged
	 *
	 * @param databasechangeloglock
	 * @return true if found, false if not found
	 */
	// @Override
	public boolean load(DatabasechangeloglockRecord databasechangeloglock) {
		return super.doSelect(databasechangeloglock);
	}

	// ----------------------------------------------------------------------
	/**
	 * Inserts the given bean in the database
	 *
	 * @param databasechangeloglock
	 */
	public long insert(DatabasechangeloglockRecord databasechangeloglock) {
		super.doInsert(databasechangeloglock);
		return 0L;
	}

	// ----------------------------------------------------------------------
	/*
	 * (non-Javadoc)
	 *
	 * @see interface
	 */
	@Override
	public DatabasechangeloglockRecord create(DatabasechangeloglockRecord databasechangeloglock) {
		insert(databasechangeloglock);
		return databasechangeloglock;
	}

	// ----------------------------------------------------------------------
	/*
	 * (non-Javadoc)
	 *
	 * @see interface
	 */
	@Override
	public boolean update(DatabasechangeloglockRecord databasechangeloglock) {
		int r = super.doUpdate(databasechangeloglock);
		return r > 0;

	}

	// ----------------------------------------------------------------------
	/*
	 * (non-Javadoc)
	 *
	 * @see interface
	 */
	@Override
	public DatabasechangeloglockRecord save(DatabasechangeloglockRecord databasechangeloglock) {
		if (super.doExists(databasechangeloglock)) {
			super.doUpdate(databasechangeloglock);
		} else {
			super.doInsert(databasechangeloglock);
		}
		return databasechangeloglock;
	}

	// ----------------------------------------------------------------------
	/*
	 * (non-Javadoc)
	 *
	 * @see interface
	 */
	@Override
	public boolean deleteById(Integer id) {
		DatabasechangeloglockRecord databasechangeloglock = newInstanceWithPrimaryKey(id);
		int r = super.doDelete(databasechangeloglock);
		return r > 0;
	}

	// ----------------------------------------------------------------------
	/*
	 * (non-Javadoc)
	 *
	 * @see interface
	 */
	@Override
	public boolean delete(DatabasechangeloglockRecord databasechangeloglock) {
		int r = super.doDelete(databasechangeloglock);
		return r > 0;
	}

	// ----------------------------------------------------------------------
	/**
	 * Checks the existence of a record in the database using the given primary key
	 * value(s)
	 *
	 * @param id;
	 * @return
	 */
	// @Override
	public boolean exists(Integer id) {
		DatabasechangeloglockRecord databasechangeloglock = newInstanceWithPrimaryKey(id);
		return super.doExists(databasechangeloglock);
	}
	// ----------------------------------------------------------------------
	/**
	 * Checks the existence of the given bean in the database
	 *
	 * @param databasechangeloglock
	 * @return
	 */
	// @Override
	public boolean exists(DatabasechangeloglockRecord databasechangeloglock) {
		return super.doExists(databasechangeloglock);
	}

	// ----------------------------------------------------------------------
	/**
	 * Counts all the records present in the database
	 *
	 * @return
	 */
	@Override
	public long countAll() {
		return super.doCountAll();
	}

	// ----------------------------------------------------------------------
	@Override
	protected String getSqlSelect() {
		return SQL_SELECT;
	}
	// ----------------------------------------------------------------------
	@Override
	protected String getSqlSelectAll() {
		return SQL_SELECT_ALL;
	}
	// ----------------------------------------------------------------------
	@Override
	protected String getSqlInsert() {
		return SQL_INSERT;
	}
	// ----------------------------------------------------------------------
	@Override
	protected String getSqlUpdate() {
		return SQL_UPDATE;
	}
	// ----------------------------------------------------------------------
	@Override
	protected String getSqlDelete() {
		return SQL_DELETE;
	}
	// ----------------------------------------------------------------------
	@Override
	protected String getSqlCount() {
		return SQL_COUNT;
	}
	// ----------------------------------------------------------------------
	@Override
	protected String getSqlCountAll() {
		return SQL_COUNT_ALL;
	}

	@Override
	protected List<String> getSearchableFields() {
		return SEARCHABLE_FIELD;
	}

	@Override
	protected List<String> getSortableFields() {
		return SORTABLE_FIELD;
	}

}
