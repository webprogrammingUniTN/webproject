package com.soapandtherest.shoppinglist.data.util;

import java.io.IOException;
import java.io.InputStream;

public class Properties {
	public static java.util.Properties loadPropertiesFromClassPath(String fileName, Class resourceClass) {
		java.util.Properties properties = new java.util.Properties();
		InputStream is = resourceClass.getResourceAsStream(fileName);
		if (is != null) {
			try {
				properties.load(resourceClass.getResourceAsStream(fileName));
			} catch (IOException e) {
				throw new RuntimeException("Cannot load '" + fileName + "'", e);
			}
			return properties;
		} else {
			throw new RuntimeException("Cannot found '" + fileName + "' (InputStream is null)");
		}
	}
}
