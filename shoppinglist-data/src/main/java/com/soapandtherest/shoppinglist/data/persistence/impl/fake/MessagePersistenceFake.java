/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:41 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence.impl.fake;

import java.util.List;

import javax.inject.Named;

import com.soapandtherest.shoppinglist.data.record.MessageRecord;
import com.soapandtherest.shoppinglist.data.persistence.MessagePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.fake.commons.GenericFakeDAO;

/**
 * Message persistence service - FAKE implementation
 *
 * @author Telosys
 *
 */
@Named("MessagePersistence")
public class MessagePersistenceFake extends GenericFakeDAO<MessageRecord> implements MessagePersistence {

	/**
	 * Constructor
	 */
	public MessagePersistenceFake() {
		super(MessageRecord.class);
	}

	/**
	 * Creates a new bean instance and set its primary key value(s)
	 *
	 * @param idmessage
	 * @return the new instance
	 */
	private MessageRecord newInstanceWithPrimaryKey(Long idmessage) {
		MessageRecord record = new MessageRecord();
		record.setIdmessage(idmessage);
		return record;
	}

	// -------------------------------------------------------------------------------------
	// Generic DAO abstract methods implementations
	// -------------------------------------------------------------------------------------
	@Override
	protected String getKey(MessageRecord record) {
		return buildKeyString(record.getIdmessage());
	}

	@Override
	protected void setAutoIncrementedKey(MessageRecord record, long value) {
		// record.setIdmessage((Long)value);
		record.setIdmessage(value);
	}

	// -------------------------------------------------------------------------------------
	// Persistence interface implementations
	// -------------------------------------------------------------------------------------
	@Override
	public long countAll() {
		return super.doCountAll();
	}

	@Override
	public MessageRecord create(MessageRecord record) {
		super.doCreateAutoIncremented(record);
		return record;
	}

	@Override
	public boolean delete(MessageRecord record) {
		return super.doDelete(record);
	}

	@Override
	public boolean deleteById(Long idmessage) {
		MessageRecord record = newInstanceWithPrimaryKey(idmessage);
		return super.doDelete(record);
	}

	@Override
	public boolean exists(MessageRecord record) {
		return super.doExists(record);
	}

	@Override
	public boolean exists(Long idmessage) {
		MessageRecord record = newInstanceWithPrimaryKey(idmessage);
		return super.doExists(record);
	}

	@Override
	public List<MessageRecord> findAll() {
		return super.doFindAll();
	}

	@Override
	public MessageRecord findById(Long idmessage) {
		MessageRecord record = newInstanceWithPrimaryKey(idmessage);
		return super.doFind(record);
	}

	@Override
	public MessageRecord save(MessageRecord record) {
		if (super.doExists(record)) {
			super.doUpdate(record);
		} else {
			super.doCreateAutoIncremented(record);
		}
		return record;
	}

	@Override
	public boolean update(MessageRecord record) {
		return super.doUpdate(record);
	}

	@Override
	public List<MessageRecord> findByShoppingListId(Long idshoppinglist) {
		throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose
																		// Tools | Templates.
	}
}
