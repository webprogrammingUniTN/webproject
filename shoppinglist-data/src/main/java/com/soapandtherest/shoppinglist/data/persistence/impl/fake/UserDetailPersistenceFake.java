/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:43 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.persistence.impl.fake;

import java.util.List;

import javax.inject.Named;

import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.data.persistence.UserDetailPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.fake.commons.GenericFakeDAO;

/**
 * UserDetail persistence service - FAKE implementation
 *
 * @author Telosys
 *
 */
@Named("UserDetailPersistence")
public class UserDetailPersistenceFake extends GenericFakeDAO<UserDetailRecord> implements UserDetailPersistence {

	/**
	 * Constructor
	 */
	public UserDetailPersistenceFake() {
		super(UserDetailRecord.class);
	}

	/**
	 * Creates a new bean instance and set its primary key value(s)
	 *
	 * @param userIduser
	 * @return the new instance
	 */
	private UserDetailRecord newInstanceWithPrimaryKey(Long userIduser) {
		UserDetailRecord record = new UserDetailRecord();
		record.setUserIduser(userIduser);
		return record;
	}

	// -------------------------------------------------------------------------------------
	// Generic DAO abstract methods implementations
	// -------------------------------------------------------------------------------------
	@Override
	protected String getKey(UserDetailRecord record) {
		return buildKeyString(record.getUserIduser());
	}

	@Override
	protected void setAutoIncrementedKey(UserDetailRecord record, long value) {
		// record.setUserIduser((Long)value);
		record.setUserIduser(value);
	}

	// -------------------------------------------------------------------------------------
	// Persistence interface implementations
	// -------------------------------------------------------------------------------------
	@Override
	public long countAll() {
		return super.doCountAll();
	}

	@Override
	public UserDetailRecord create(UserDetailRecord record) {
		super.doCreateAutoIncremented(record);
		return record;
	}

	@Override
	public boolean delete(UserDetailRecord record) {
		return super.doDelete(record);
	}

	@Override
	public boolean deleteById(Long userIduser) {
		UserDetailRecord record = newInstanceWithPrimaryKey(userIduser);
		return super.doDelete(record);
	}

	@Override
	public boolean exists(UserDetailRecord record) {
		return super.doExists(record);
	}

	@Override
	public boolean exists(Long userIduser) {
		UserDetailRecord record = newInstanceWithPrimaryKey(userIduser);
		return super.doExists(record);
	}

	@Override
	public List<UserDetailRecord> findAll() {
		return super.doFindAll();
	}

	@Override
	public UserDetailRecord findById(Long userIduser) {
		UserDetailRecord record = newInstanceWithPrimaryKey(userIduser);
		return super.doFind(record);
	}

	@Override
	public UserDetailRecord findByEmail(String email) {
		UserDetailRecord record = newInstanceWithPrimaryKey(1L);
		return super.doFind(record);
	}

	@Override
	public UserDetailRecord save(UserDetailRecord record) {
		if (super.doExists(record)) {
			super.doUpdate(record);
		} else {
			super.doCreateAutoIncremented(record);
		}
		return record;
	}

	@Override
	public boolean update(UserDetailRecord record) {
		return super.doUpdate(record);
	}
}
