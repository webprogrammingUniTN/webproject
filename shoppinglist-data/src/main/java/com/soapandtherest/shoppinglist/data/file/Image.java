package com.soapandtherest.shoppinglist.data.file;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.util.IOUtils;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import javax.imageio.ImageIO;
import net.coobird.thumbnailator.Thumbnails;

public class Image extends File {

	private static final String IMAGE_EXTENSION = "jpg";

	private static final int SMALL_IMAGE_WIDTH = 200;
	private static final int SMALL_IMAGE_HEIGHT = 125;

	private static final int MEDIUM_IMAGE_WIDTH = 480;
	private static final int MEDIUM_IMAGE_HEIGHT = 300;

	private static final int BIG_IMAGE_WIDTH = 800;
	private static final int BIG_IMAGE_HEIGHT = 500;
	public static final String UPLOAD_CONTENT_TYPE = "image/jpeg";
	public static final double OUTPUT_QUALITY = 0.8d;

	public enum ImageSize {
		SMALL("SMALL"), MEDIUM("MEDIUM"), BIG("BIG");

		private String name;

		// Constructor
		ImageSize(String name) {
			this.name = name;
		}

		public String toString() {
			return "_" + name;
		}
	}

	public Image(String objectKey) {
		super(objectKey);
	}

	public Image(String objectKey, Boolean test) {
		super(objectKey, test);
	}

	@Override
	protected Boolean isValidFile() {
		AWSFactory factory = AWSFactory.getInstance();
		AmazonS3 s3Client = factory.getS3Client();
		return s3Client.doesObjectExist(factory.getBucketName(), this.getObjectPath(ImageSize.SMALL));
	}

	@Override
	public void CompleteUpload(boolean publicFile) {
		if (!isUploadFile()) {
			throw new IllegalArgumentException("You try to complete a file that it is yet a complete file");
		}
		AWSFactory factory = AWSFactory.getInstance();
		AmazonS3 s3Client = factory.getS3Client();
		TransferManager manager = factory.getTransferManager();
		String oldPath = this.getObjectPath();
		InputStream inputStream = null;
		try {
			inputStream = s3Client.getObject(factory.getBucketName(), oldPath).getObjectContent();
			byte[] originalImage = IOUtils.toByteArray(inputStream);
			byte[] bigImageBytes = convertImageToByte(originalImage, BIG_IMAGE_WIDTH, BIG_IMAGE_HEIGHT);
			String hash = super.objectSha256(new ByteArrayInputStream(bigImageBytes));
			this.setObjectKey(hash);
			if (!s3Client.doesObjectExist(factory.getBucketName(), this.getObjectPath())) {
				uploadImage(factory.getBucketName(), new ByteArrayInputStream(bigImageBytes), ImageSize.BIG, manager);
				uploadImage(factory.getBucketName(),
						convertImage(originalImage, MEDIUM_IMAGE_WIDTH, MEDIUM_IMAGE_HEIGHT), ImageSize.MEDIUM,
						manager);
				uploadImage(factory.getBucketName(), convertImage(originalImage, SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT),
						ImageSize.SMALL, manager);
				super.InsertDatabaseFile(publicFile);
			}
		} catch (NoSuchAlgorithmException e) {
			logger.error("Impossible found algorithm for file hashing, {}", e);
			throw new RuntimeException("Impossible found algorithm for file hashing");
		} catch (IOException e) {
			logger.error("File hashing generate IO exception, {}", e);
			throw new RuntimeException("File hashing generate IO exception");
		} finally {
			s3Client.deleteObject(factory.getBucketName(), oldPath);
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				logger.error("Error during closing of S3 resource, {}", e);
				e.printStackTrace();
			}
		}
	}

	private InputStream convertImage(byte[] originalImage, int resultWidth, int resultHeight) throws IOException {
		byte[] bytes = convertImageToByte(originalImage, resultWidth, resultHeight);
		return new ByteArrayInputStream(bytes);
	}

	private byte[] convertImageToByte(byte[] originalImage, int resultWidth, int resultHeight) throws IOException {
		ByteArrayInputStream bis = new ByteArrayInputStream(originalImage);
		OutputStream bigImageStream = resize(bis, resultWidth, resultHeight);
		ByteArrayOutputStream buffer = (ByteArrayOutputStream) bigImageStream;
		return buffer.toByteArray();
	}

	private void uploadImage(String bucketName, InputStream image, ImageSize size, TransferManager transferManager) {
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentType(UPLOAD_CONTENT_TYPE);
		PutObjectRequest request = new PutObjectRequest(bucketName, this.getObjectPath(size), image, metadata);
		transferManager.upload(request);
	}

	/**
	 * Resizes an image to a absolute width and height (the image may not be
	 * proportional)
	 *
	 * @param stream
	 *            Stream of the original image
	 * @param scaledWidth
	 *            absolute width in pixels if set to zero image are scaled
	 *            proportionally
	 * @param scaledHeight
	 *            absolute height in pixels
	 * @throws IOException
	 */
	public static OutputStream resize(InputStream stream, int scaledWidth, int scaledHeight) throws IOException {
		BufferedImage inputImage = ImageIO.read(stream);

		int resultWidth;
		int resultHeight;
		if (scaledWidth != 0 && scaledHeight != 0) {
			resultWidth = scaledWidth;
			resultHeight = scaledHeight;
		} else if (scaledWidth != 0) {
			resultWidth = scaledWidth;
			resultHeight = (int) ((double) inputImage.getHeight() / inputImage.getWidth() * scaledWidth);
		} else {
			resultHeight = scaledHeight;
			resultWidth = (int) ((double) inputImage.getWidth() / inputImage.getHeight() * scaledHeight);
		}
		logger.debug("Result image width: ", resultWidth, " and result ");

		OutputStream outputStream = new ByteArrayOutputStream();

		Thumbnails.of(inputImage).size(resultWidth, resultHeight).outputFormat(IMAGE_EXTENSION)
				.outputQuality(OUTPUT_QUALITY).toOutputStream(outputStream);

		return outputStream;
	}

	@Override
	public URL GetPresignedURL() {
		return presignedUrl(getObjectPath(ImageSize.SMALL), HttpMethod.GET, 0);
	}

	public URL GetPresignedURL(ImageSize size) {
		return presignedUrl(getObjectPath(size), HttpMethod.GET, 0);
	}

	@Override
	protected String getObjectPath() {
		return this.getObjectPath(ImageSize.SMALL);
	}

	private String getObjectPath(ImageSize size) {
		return (isUploadFile() ? UPLOAD_FOLDER : FINAL_DESTINATION_FOLDER) + getObjectKey()
				+ (isUploadFile() ? "" : size.toString());
	}
}
