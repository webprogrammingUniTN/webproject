package com.soapandtherest.shoppinglist.data.file;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.soapandtherest.shoppinglist.data.util.Properties;

public class AWSFactory {
	private static final Boolean initS3SyncBlock = true;

	private AmazonS3 S3Client;

	private String bucketName;

	public TransferManager getTransferManager() {
		return transferManager;
	}

	private TransferManager transferManager;

	private static AWSCredentialsProvider getCredentialsProvider() {
		AWSCredentialsProvider credentialsProvider;
		String accessKey = s3Properties.getProperty("s3.accesskey", "");
		String secretKey = s3Properties.getProperty("s3.secretkey", "");
		if (accessKey.isEmpty() && secretKey.isEmpty()) {
			credentialsProvider = new InstanceProfileCredentialsProvider(false);
		} else {
			BasicAWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
			credentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
		}
		return credentialsProvider;
	}

	private static java.util.Properties getS3Properties() {
		if (s3Properties == null) {
			s3Properties = Properties.loadPropertiesFromClassPath(S3_PROPERTIES_FILE_NAME, AWSFactory.class);
		}
		return s3Properties;
	}

	private static java.util.Properties s3Properties = null;

	private static AWSFactory ourInstance = new AWSFactory();

	private final static String S3_PROPERTIES_FILE_NAME = "/s3.properties";

	public static AWSFactory getInstance() {
		return ourInstance;
	}

	private AWSFactory() {
		initS3Client();
	}

	private void initS3Client() {
		synchronized (initS3SyncBlock) {
			if (S3Client == null) {
				java.util.Properties properties = getS3Properties();
				ClientConfiguration clientConfiguration = new ClientConfiguration();
				clientConfiguration.setSignerOverride("AWSS3V4SignerType");

				AmazonS3ClientBuilder clientBuilder = AmazonS3ClientBuilder.standard();
				clientBuilder = setCustomUrl(properties, clientConfiguration, clientBuilder);
				clientBuilder = setRegion(properties, clientBuilder);
				S3Client = clientBuilder.withCredentials(getCredentialsProvider()).build();
				initTransferManager();
				bucketName = properties.getProperty("s3.bucketname");
			}
		}
	}

	private AmazonS3ClientBuilder setCustomUrl(java.util.Properties properties, ClientConfiguration clientConfiguration,
			AmazonS3ClientBuilder clientBuilder) {
		String s3Url = properties.getProperty("s3.s3url", "");
		if (!s3Url.isEmpty()) {
			clientBuilder = clientBuilder
					.withEndpointConfiguration(
							new AwsClientBuilder.EndpointConfiguration(s3Url, Regions.DEFAULT_REGION.name()))
					.withPathStyleAccessEnabled(true).withClientConfiguration(clientConfiguration);
		}
		return clientBuilder;
	}

	private AmazonS3ClientBuilder setRegion(java.util.Properties properties, AmazonS3ClientBuilder clientBuilder) {
		String s3Region = properties.getProperty("s3.region", "");
		if (!s3Region.isEmpty()) {
			clientBuilder = clientBuilder.withRegion(s3Region);
		}
		return clientBuilder;
	}

	private void initTransferManager() {
		TransferManagerBuilder transferManagerBuilder = TransferManagerBuilder.standard().withS3Client(getS3Client());
		transferManager = transferManagerBuilder.build();
	}

	public AmazonS3 getS3Client() {
		return S3Client;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void closeConnections() {
		com.amazonaws.http.IdleConnectionReaper.shutdown();
	}
}
