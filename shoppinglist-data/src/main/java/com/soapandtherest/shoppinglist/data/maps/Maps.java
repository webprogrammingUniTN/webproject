package com.soapandtherest.shoppinglist.data.maps;

import com.google.maps.GeoApiContext;
import com.google.maps.NearbySearchRequest;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlaceType;
import com.google.maps.model.PlacesSearchResult;
import com.soapandtherest.shoppinglist.data.util.Properties;
import java.io.IOException;
import java.util.List;

public class Maps {

	private static final String PLACES_PROPERTIES_FILE_NAME = "/places.properties";
	public static final java.util.Properties properties = Properties
			.loadPropertiesFromClassPath(PLACES_PROPERTIES_FILE_NAME, Maps.class);
	private static final String PLACES_APIKEY = "places.apikey";
	private static final int RADIUS = 5000;

	public static PlacesSearchResult[] nearbyShop(double lat, double lon, List<String> types) {
		GeoApiContext.Builder builder = new GeoApiContext.Builder();
		builder.apiKey(properties.getProperty(PLACES_APIKEY));
		GeoApiContext context = builder.build();
		NearbySearchRequest request = PlacesApi.nearbySearchQuery(context, new LatLng(lat, lon));
		request.radius(RADIUS);
		PlaceType[] placesType = new PlaceType[types.size()];
		for (int i = 0; i < types.size(); i++) {
			String type = types.get(i);
			if (type != null) {
				placesType[i] = PlaceType.valueOf(type);
			}
		}
		request.type(placesType);
		try {
			return request.await().results;
		} catch (ApiException | InterruptedException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static boolean validType(String type) {
		try {
			PlaceType.valueOf(type);
		} catch (IllegalArgumentException ex) {
			return false;
		}
		return true;
	}
}
