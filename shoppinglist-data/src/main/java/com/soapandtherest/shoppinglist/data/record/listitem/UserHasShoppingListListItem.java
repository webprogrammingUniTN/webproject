/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:43 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.record.listitem;

import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.data.commons.ListItem;

public class UserHasShoppingListListItem implements ListItem {

	private final String value;
	private final String label;

	public UserHasShoppingListListItem(UserHasShoppingListRecord userHasShoppingList) {
		super();

		this.value = "" + userHasShoppingList.getUserIduser() + "|"
				+ userHasShoppingList.getShoppingListIdshoppingList();

		// TODO : Define here the attributes to be displayed as the label
		this.label = userHasShoppingList.toString();
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
