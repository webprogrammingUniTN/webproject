/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:14:42 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */
package com.soapandtherest.shoppinglist.data.record.listitem;

import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.commons.ListItem;

public class ShoppingListListItem implements ListItem {

	private final String value;
	private final String label;

	public ShoppingListListItem(ShoppingListRecord shoppingList) {
		super();

		this.value = "" + shoppingList.getIdshoppingList();

		// TODO : Define here the attributes to be displayed as the label
		this.label = shoppingList.toString();
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public String getLabel() {
		return label;
	}

}
