package com.soapandtherest.shoppinglist.data.file;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.FilePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.FileRecord;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class File implements Serializable {
	protected static Logger logger = LoggerFactory.getLogger(File.class);

	private static final FilePersistenceJdbc persistance = new FilePersistenceJdbc();

	static final String UPLOAD_FOLDER = "upload/";

	static final String FINAL_DESTINATION_FOLDER = "files/";

	public File(String objectKey) {
		this(objectKey, true);
	}

	public File(String objectKey, Boolean test) {
		setObjectKey(objectKey);
		if (test) {
			if (!isValidFile()) {
				logger.info("File with object key " + objectKey + " not found");
				throw new IllegalArgumentException("File not found");
			}
		}
	}

	protected Boolean isValidFile() {
		AWSFactory factory = AWSFactory.getInstance();
		AmazonS3 s3Client = factory.getS3Client();
		if (!this.isUploadFile()) {
			return false;
		}
		return s3Client.doesObjectExist(factory.getBucketName(), this.getObjectPath());
	}

	// Unused function used for test if file can be shared with anyone
	/*
	 * private boolean checkIfFileIsPublic() { FileRecord record =
	 * persistance.findById(this.getObjectKey()); if (record != null) { return
	 * record.getPublicFile(); } return false; }
	 */

	public static File UploadPresignedURL(int contentLength) {
		File file = new File(generateUploadGuid(), false);
		file.setUrl(presignedUrl(file.getObjectPath(), HttpMethod.PUT, contentLength));
		return file;
	}

	public URL GetPresignedURL() {
		return presignedUrl(getObjectPath(), HttpMethod.GET, 0);
	}

	protected static URL presignedUrl(String objectPath, HttpMethod method, int contentLength)
			throws SdkClientException {
		try {
			AWSFactory factory = AWSFactory.getInstance();
			AmazonS3 s3Client = factory.getS3Client();

			// Set the presigned URL to expire after one hour.
			java.util.Date expiration = new java.util.Date();
			long expTimeMillis = expiration.getTime();
			expTimeMillis += 1000 * 60 * 60;
			expiration.setTime(expTimeMillis);

			// Generate the presigned URL.
			logger.info("Generating pre-signed URL of object " + objectPath);
			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
					factory.getBucketName(), objectPath).withMethod(method).withExpiration(expiration);
			if (method == HttpMethod.PUT) {
				generatePresignedUrlRequest.putCustomRequestHeader("Content-Length", String.valueOf(contentLength));
			}
			URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

			System.out.println("Pre-Signed URL: " + url.toString());
			return url;
		} catch (AmazonServiceException e) {
			logger.error("The call was transmitted successfully, but Amazon S3 couldn't process it, "
					+ "so it returned an error response, specific error: {}", e);
			throw e;
		} catch (SdkClientException e) {
			logger.error("Amazon S3 couldn't be contacted for a response, or the client couldn't"
					+ "parse the response from Amazon S3, specific error: {}", e);
			throw e;
		}
	}

	public void CompleteUpload() {
		CompleteUpload(true);
	}

	public void CompleteUpload(boolean privateFile) {
		logger.info("File completion");
		if (!isUploadFile()) {
			throw new IllegalArgumentException("You try to complete a file that it is yet a complete file");
		}
		AWSFactory factory = AWSFactory.getInstance();
		AmazonS3 s3Client = factory.getS3Client();
		String oldPath = this.getObjectPath();
		logger.debug("File old path is: ", oldPath);
		try {
			String hash = objectSha256(s3Client, factory);
			this.setObjectKey(hash);
			logger.debug("File new path is: ", this.getObjectPath());
			s3Client.copyObject(factory.getBucketName(), oldPath, factory.getBucketName(), this.getObjectPath());
			InsertDatabaseFile(privateFile);
		} catch (NoSuchAlgorithmException e) {
			logger.error("Impossible found algorithm for file hashing, {}", e);
			throw new RuntimeException("Impossible found algorithm for file hashing");
		} catch (IOException e) {
			logger.error("File hashing generate IO exception, ", e);
			throw new RuntimeException("File hashing generate IO exception");
		} finally {
			s3Client.deleteObject(factory.getBucketName(), oldPath);
		}
	}

	protected void InsertDatabaseFile(boolean publicFile) {
		FileRecord record = new FileRecord();
		record.setKey(this.getObjectKey());
		record.setIsUpload(this.isUploadFile());
		record.setPublicFile(publicFile);
		persistance.insert(record);
	}

	private String objectSha256(AmazonS3 s3Client, AWSFactory factory) throws IOException, NoSuchAlgorithmException {
		return objectSha256(this.objectKey, s3Client, factory);
	}

	private String objectSha256(String objectPath, AmazonS3 s3Client, AWSFactory factory)
			throws NoSuchAlgorithmException, IOException {
		InputStream inputStream = s3Client.getObject(factory.getBucketName(), objectPath).getObjectContent();
		return objectSha256(inputStream);
	}

	protected String objectSha256(InputStream inputStream) throws NoSuchAlgorithmException, IOException {
		MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
		DigestInputStream digestInputStream = new DigestInputStream(inputStream, messageDigest);
		byte[] buffer = new byte[4096];
		int count = 0;
		while (digestInputStream.read(buffer) > -1) {
			count++;
		}
		logger.info("total read: " + count);
		MessageDigest digest = digestInputStream.getMessageDigest();
		digestInputStream.close();
		byte[] md5 = digest.digest();
		StringBuilder sb = new StringBuilder();
		for (byte b : md5) {
			sb.append(String.format("%02X", b));
		}
		return sb.toString().toLowerCase();
	}

	public void DeleteFile() {
		AWSFactory factory = AWSFactory.getInstance();
		AmazonS3 s3Client = factory.getS3Client();
		s3Client.deleteObject(factory.getBucketName(), this.getObjectPath());
	}

	private String objectKey;

	public String getObjectKey() {
		return objectKey;
	}

	protected void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	public Boolean isUploadFile() {
		return getObjectKey().contains("-");
	}

	protected String getObjectPath() {
		return (isUploadFile() ? UPLOAD_FOLDER : FINAL_DESTINATION_FOLDER) + getObjectKey();
	}

	private static String generateUploadGuid() {
		return UUID.randomUUID().toString();
	}

	private URL url;

	public URL getUrl() {
		return url;
	}

	protected void setUrl(URL url) {
		this.url = url;
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof File) {
			return ((File) object).getObjectKey().equals(this.getObjectKey());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return objectKey.hashCode();
	}
}
