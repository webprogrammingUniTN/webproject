/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:15:57 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */

package com.soapandtherest.shoppinglist.data.persistence.impl.jdbc;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons.DAOTestUtil;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistenceGenericTest;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * JUnit tests for ShoppingListDAO
 *
 * @author Telosys Tools
 *
 */
public class ShoppingListPersistenceJdbcTest extends ShoppingListPersistenceGenericTest {

	private static final String CREATE_TABLE = "CREATE TABLE shopping_list ("
			+ "idshopping_list IDENTITY AUTO_INCREMENT NOT NULL," + "name VARCHAR NOT NULL,"
			+ "description VARCHAR NOT NULL," + "image VARCHAR NOT NULL," + "template_id BIGINT NOT NULL,"
			+ "create_time TIMESTAMP NOT NULL," + "update_time TIMESTAMP NOT NULL," + "PRIMARY KEY(idshopping_list)"
			+ ");";

	@BeforeClass
	public static void init() {
		DAOTestUtil.initDatabase(CREATE_TABLE, "shopping_list");
	}

	@Test
	public void testPersistenceService() {

		ShoppingListPersistence persistenceService = new ShoppingListPersistenceJdbc();

		testPersistenceServiceWithAutoincrementedKey(persistenceService);
	}

}
