/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:15:57 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */

package com.soapandtherest.shoppinglist.data.persistence.impl.jdbc;

import com.soapandtherest.shoppinglist.data.persistence.UserPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons.DAOTestUtil;
import com.soapandtherest.shoppinglist.data.persistence.UserPersistenceGenericTest;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * JUnit tests for UserDAO
 *
 * @author Telosys Tools
 *
 */
public class UserPersistenceJdbcTest extends UserPersistenceGenericTest {

	private static final String CREATE_TABLE = "CREATE TABLE user (" + "iduser IDENTITY AUTO_INCREMENT NOT NULL,"
			+ "anonymous BOOLEAN NOT NULL," + "create_time TIMESTAMP NOT NULL," + "update_time TIMESTAMP NOT NULL,"
			+ "PRIMARY KEY(iduser)" + ");";

	@BeforeClass
	public static void init() {
		DAOTestUtil.initDatabase(CREATE_TABLE, "user");
	}

	@Test
	public void testPersistenceService() {

		UserPersistence persistenceService = new UserPersistenceJdbc();

		testPersistenceServiceWithAutoincrementedKey(persistenceService);
	}

}
