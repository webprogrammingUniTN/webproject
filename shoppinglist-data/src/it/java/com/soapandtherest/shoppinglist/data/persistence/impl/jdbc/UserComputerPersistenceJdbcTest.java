/*
 * Created on 2018-07-11 ( Date ISO 2018-07-11 - Time 18:15:57 )
 * Generated by Telosys ( http://www.telosys.org/ ) version 3.0.0
 */

package com.soapandtherest.shoppinglist.data.persistence.impl.jdbc;

import com.soapandtherest.shoppinglist.data.persistence.UserComputerPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons.DAOTestUtil;
import com.soapandtherest.shoppinglist.data.persistence.UserComputerPersistenceGenericTest;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * JUnit tests for UserComputerDAO
 *
 * @author Telosys Tools
 *
 */
public class UserComputerPersistenceJdbcTest extends UserComputerPersistenceGenericTest {

	private static final String CREATE_TABLE = "CREATE TABLE user_computer (" + "id_computer VARCHAR NOT NULL,"
			+ "user_iduser BIGINT NOT NULL," + "remember BOOLEAN NOT NULL," + "create_time TIMESTAMP NOT NULL,"
			+ "update_time TIMESTAMP NOT NULL," + "PRIMARY KEY(id_computer,user_iduser)" + ");";

	@BeforeClass
	public static void init() {
		DAOTestUtil.initDatabase(CREATE_TABLE, "user_computer");
	}

	@Test
	public void testPersistenceService() {

		UserComputerPersistence persistenceService = new UserComputerPersistenceJdbc();

		testPersistenceService(persistenceService);
	}

}
