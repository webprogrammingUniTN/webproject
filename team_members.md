# Team members
Team members are:
- Matteo Bortolon 184590 [matteo.bortolon@studenti.unitn.it](mailto:matteo.bortolon@studenti.unitn.it)
- Beatrice Vergani 187855 [beatrice.vergani@studenti.unitn.it](mailto:beatrice.vergani@studenti.unitn.it)
- Mouslim Fatnassi 187982 [mouslim.fatnassi@studenti.unitn.it](mailto:mouslim.fatnassi@studenti.unitn.it)
- Gionni Reffo 186021 [gionni.reffo@studenti.unitn.it](mailto:gionni.reffo@studenti.unitn.it)
- Jessica Lucchetta 186646 [jessica.lucchetta@studenti.unitn.it](mailto:jessica.lucchetta@studenti.unitn.it)