package com.soapandtherest.shoppinglist.feature;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.soapandtherest.shoppinglist.testUtil.InitCucumber;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class IndexFeature {
	private String baseUrl = "/shoppinglist/";

	@Before
	public void before_scenario(Scenario scenario) throws Throwable {
		baseUrl = InitCucumber.getBaseUrl();
		Configuration.browser = "chrome";
		Configuration.headless = true;
		open(baseUrl);
	}

	@Given("^I am on index page$")
	public void i_am_on_index_page() throws Throwable {
		open(baseUrl);
	}

	@When("^I load page$")
	public void i_load_page() throws Throwable {
	}

	@Then("^I should see application title$")
	public void i_should_see_data() throws Throwable {
		$(".navbar-brand").shouldHave(Condition.text("JustShop"));
	}
}
