package com.soapandtherest.shoppinglist.testUtil;

public final class InitCucumber {
	public static String getBaseUrl() {
		return System.getProperty("integration-test-url");
	}
}
