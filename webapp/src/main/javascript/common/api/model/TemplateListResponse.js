export default class ShoppingListResponse {
  constructor(numberOfTotalElement, templates) {
    if (!arguments.length) {
      throw new Error('Not define constructor');
    } else if (arguments.length === 1) {
      const object = numberOfTotalElement;
      this.numberOfTotalElement = object.numberOfTotalElement;
      this.templates = object.templates;
    } else if (arguments.length >= 2) {
      this.numberOfTotalElement = numberOfTotalElement;
      this.templates = templates;
    }
  }
}
