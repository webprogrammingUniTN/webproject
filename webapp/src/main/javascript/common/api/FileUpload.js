import Request from './Request';

export default class FileUpload {
  static ApiAddress = '/files';

  static UploadUrl(file, progressFunction) {
    return new Promise((resolve, reject) => {
      const uploadObject = {
        byteContentSize: file.size,
      };
      Request.CreateRequest('POST', FileUpload.ApiAddress, uploadObject).then((result) => {
        console.log(result);
        Request.CreateRequest('PUT', result.url, file, progressFunction).then(() => {
          resolve(result);
        }).catch((rejectResult) => {
          reject(rejectResult);
        });
      }).catch((error) => {
        reject(error);
      });
    });
  }
}
