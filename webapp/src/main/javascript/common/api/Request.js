export default class Request {
  // Joins path segments.  Preserves initial '/' and resolves '..' and '.'
  // Does not support using '..' to go above/outside the root.
  // This means that join('foo', '../../bar') will not resolve to '../bar'
  static join(...args) {
    // Split the inputs into a list of path commands.
    let parts = [];
    for (let i = 0, l = args.length; i < l; i += 1) {
      parts = parts.concat(args[i].split('/'));
    }
    // Interpret the path commands to get the new resolved path.
    const newParts = [];
    for (let i = 0, l = parts.length; i < l; i += 1) {
      const part = parts[i];
      // Remove leading and trailing slashes
      // Also remove '.' segments
      if (!(!part || part === '.')) {
        // Interpret '..' to pop the last segment
        if (part === '..') newParts.pop();
        // Push new path segments.
        else newParts.push(part);
      }
    }
    // Preserve the initial slash if there was one.
    if (parts[0] === '') newParts.unshift('');
    // Turn back into a single string path.
    return newParts.join('/') || (newParts.length ? '/' : '.');
  }

  /*
   * Create request output a promise that execute an AJAX request with the
   * specific method ('GET', 'POST', 'PUT', 'DELETE') if the body is a Blob,
   * function send it as is else it try to stringify into JSON format, progress
   * function parameter is a function called to communicate file upload progress
   * as specific by XMLHttpRequest API
   */
  static CreateRequest(method, url, body, progressFunction) {
    return new Promise((resolve, reject) => {
      const blobUploadRequest = body instanceof Blob;
      const xmlRequest = new XMLHttpRequest();
      const urlWithContextPath = url.startsWith('http') ? url : Request.join(applicationParameter.contextURL, '/', url);
      xmlRequest.open(method, urlWithContextPath, true);
      xmlRequest.onreadystatechange = () => {
        Request.RequestResponse(resolve, reject, xmlRequest, blobUploadRequest);
      };
      if (progressFunction) {
        xmlRequest.onprogress = progressFunction;
      }
      if (body) {
        if (blobUploadRequest) {
          xmlRequest.send(body);
        } else {
          xmlRequest.setRequestHeader('Content-Type', 'application/json');
          xmlRequest.send(JSON.stringify(body));
        }
      } else {
        xmlRequest.send();
      }
    });
  }

  static RequestResponse(resolve, reject, xmlRequest, noResponseRequest) {
    if (xmlRequest.readyState === 4) {
      try {
        if (!noResponseRequest) {
          Request.ResponseElaboration(resolve, reject, xmlRequest);
        } else if (xmlRequest.status === 200) {
          resolve(null);
        } else {
          reject(new Error('Errore durante l\'upload del file'));
        }
      } catch (err) {
        console.log(err);
        reject(new Error('Errore sconosciuto prova piu\' tardi'));
      }
    }
  }

  static ErrorElaboration(reject, apiResponse) {
    if (apiResponse.message) {
      reject(new Error(apiResponse.message));
    } else {
      reject(new Error('Unknown object return "'.concat(JSON.stringify(apiResponse), '"')));
    }
  }

  static ResponseElaboration(resolve, reject, xmlRequest) {
    const response = JSON.parse(xmlRequest.responseText);
    // TODO: manage authentication error
    if (xmlRequest.status === 200) {
      resolve(response);
    } else {
      Request.ErrorElaboration(reject, response);
    }
  }

  static ParamEncoding(params) {
    const encodedQueryString = Object.keys(params)
      .map(k => encodeURIComponent(k).concat('=', encodeURIComponent(params[k])))
      .join('&');
    return encodedQueryString === '' ? encodedQueryString : '?'.concat(encodedQueryString);
  }
}
