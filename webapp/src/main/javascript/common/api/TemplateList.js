import Request from './Request';

export default class TemplateListRequest {
  static ApiAddress = '/template';

  static GetTemplateList(params) {
    return Request.CreateRequest('GET', TemplateListRequest.ApiAddress.concat(Request.ParamEncoding(params)));
  }

  static AddTemplateList(name, description, disabled, images, categories) {
    const template = { name, description, disabled, images, categories };
    return Request.CreateRequest('POST', TemplateListRequest.ApiAddress, template);
  }

  static UpdateTemplateList(id, name, description, disabled, images, categories) {
    const template = {
      id, name, description, disabled, images, categories,
    };
    return Request.CreateRequest('PUT', TemplateListRequest.ApiAddress, template);
  }

  static SingleTemplateList(id) {
    return Request.CreateRequest('GET', TemplateListRequest.ApiAddress.concat('/', id));
  }
}
