# PhotoCarousel Component
Photo Carousel is a Vue component that show a [photo carousel](https://getbootstrap.com/docs/4.0/components/carousel/) with specific slides to permit upload of one or more file into the server
## Props
* **uploadSlideEnable:** permit to upload a file
* **maxNumberOfUpload:** maximum number of upload, for this specific component instance
* **images:** It's an array of object each one with field (objectKey _(file ID)_, url _(file URL)_ and canRemove _(image can be delete)_) . At shopping list server side this structure is contain inside the class _com.soapandtherest.shoppinglist.view.File_. Array reference must be mutable because component add element to it in case of file upload
## Methods
* **getObjectsList()** Get the list of file correct upload that we can send to server (The list return don't include url field)
* **getSelectImage()** Get the current select image without url field, it's util for things that require to select one element from a predefine set image or upload a custom image but at the end we need to select only an image
## Events
* **uploadEnd:** communicate that all user upload was finished (useful for wait until all images uploaded)
* **uploadStart:** communicate that user start an upload (useful for wait until all images uploaded)
