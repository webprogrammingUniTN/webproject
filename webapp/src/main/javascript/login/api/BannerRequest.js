import Request from '../../common/api/Request';

export default class BannerRequest {
  static ApiAddress = '/banner';

  static IsAccepted() {
    return Request.CreateRequest('GET', BannerRequest.ApiAddress);
  }

  static Accept() {
    return Request.CreateRequest('POST', BannerRequest.ApiAddress);
  }
}
