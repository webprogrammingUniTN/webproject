import Request from '../../common/api/Request';

export default class EnterRequest {
  static ApiAddress = '/enter';

  static DoIt() {
    return Request.CreateRequest('POST', EnterRequest.ApiAddress);
  }

  static ReadUserInfo() {
    return Request.CreateRequest('GET', EnterRequest.ApiAddress);
  }
}
