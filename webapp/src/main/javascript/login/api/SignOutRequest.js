import Request from '../../common/api/Request';

export default class SignOutRequest {
  static ApiAddress = '/signout';

  static Bye() {
    return Request.CreateRequest('POST', SignOutRequest.ApiAddress);
  }
}
