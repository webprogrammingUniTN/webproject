import Request from '../../common/api/Request';

export default class ForgottenRequest {
  static ApiAddress = '/forgotten';

  static SendEmail(body) {
    return Request.CreateRequest('POST', ForgottenRequest.ApiAddress, body);
  }
}
