import Request from '../../common/api/Request';

export default class UserImageRequest {
  static ApiAddress = '/userimage';

  static Update(body) {
    return Request.CreateRequest('POST', UserImageRequest.ApiAddress, body);
  }
}
