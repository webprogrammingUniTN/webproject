import Request from '../../common/api/Request';

export default class AdminGrantsRequest {
  static ApiAddress = '/admingrants';

  static AddAdminGrants(body) {
    return Request.CreateRequest('POST', AdminGrantsRequest.ApiAddress, body);
  }

  static RemoveAdminGrants(body) {
    return Request.CreateRequest('DELETE', AdminGrantsRequest.ApiAddress, body);
  }
}
