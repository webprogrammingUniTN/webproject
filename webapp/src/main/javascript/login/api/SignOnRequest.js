import Request from '../../common/api/Request';

export default class SignOnRequest {
  static ApiAddress = '/signon';

  static ConfirmMail() {
    return Request.CreateRequest('GET', SignOnRequest.ApiAddress);
  }

  static Registration(body) {
    return Request.CreateRequest('POST', SignOnRequest.ApiAddress, body);
  }
}
