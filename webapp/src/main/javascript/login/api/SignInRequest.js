import Request from '../../common/api/Request';

export default class SignInRequest {
  static ApiAddress = '/signin';

  static DoIt(body) {
    return Request.CreateRequest('POST', SignInRequest.ApiAddress, body);
  }
}
