export default class LogoutNotification {
  static callbackList = [];

  static RegisterCallback(callback) {
    LogoutNotification.callbackList.push(callback);
  }

  static CallCalbacks() {
    LogoutNotification.callbackList.forEach((callback) => {
      callback();
    });
  }
}
