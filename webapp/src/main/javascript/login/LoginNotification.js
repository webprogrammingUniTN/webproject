export default class LoginNotification {
  static callbackList = [];

  static RegisterCallback(callback) {
    LoginNotification.callbackList.push(callback);
  }

  static CallCalbacks() {
    LoginNotification.callbackList.forEach((callback) => {
      callback();
    });
  }
}
