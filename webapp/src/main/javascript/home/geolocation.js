import NearbyShopRequest from './api/NearbyShop';

export default class Geolocation {
  static StartGeolocation() {
    if ('geolocation' in navigator) {
      /* geolocation is available */
      console.log('Geolocation is available');
      // const watchID =
      navigator.geolocation.watchPosition((position) => {
        console.log(position);
        NearbyShopRequest.SendPosition(position.coords.latitude, position.coords.longitude);
      });
    } else {
      console.log('Geolocation not available');
    }
  }
}
