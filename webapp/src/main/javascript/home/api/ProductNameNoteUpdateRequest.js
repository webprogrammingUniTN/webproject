import Request from '../../common/api/Request';

export default class ProductNameNoteUpdateRequest {
  static ApiAddress = '/productnamenoteupdate';

  static Update(body) {
    return Request.CreateRequest('POST', ProductNameNoteUpdateRequest.ApiAddress, body);
  }
}
