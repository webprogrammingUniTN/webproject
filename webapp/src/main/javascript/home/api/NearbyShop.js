import Request from '../../common/api/Request';

export default class NearbyShopRequest {
  static ApiAddress = '/nearbyshop';

  static SendPosition(lat, long) {
    return Request.CreateRequest('POST', NearbyShopRequest.ApiAddress, {
      lat,
      lon: long,
    });
  }
}

