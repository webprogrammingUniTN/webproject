import Request from '../../common/api/Request';

export default class SenderListRequest {
  static ApiAddress = '/senderlist';

  static GetSenderList(id) {
    return Request.CreateRequest('POST', SenderListRequest.ApiAddress.concat('/', id));
  }
}
