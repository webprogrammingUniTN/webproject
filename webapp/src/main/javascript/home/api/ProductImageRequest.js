import Request from '../../common/api/Request';

export default class ProductImageRequest {
  static ApiAddress = '/productimage';

  static Update(body) {
    return Request.CreateRequest('POST', ProductImageRequest.ApiAddress, body);
  }
}
