import Request from '../../common/api/Request';

export default class ProductListRequest {
  static ApiAddress = '/productlist';

  static GetProductList(id) {
    return Request.CreateRequest('GET', ProductListRequest.ApiAddress.concat('/', id));
  }
}
