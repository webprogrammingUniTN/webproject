import Request from '../../common/api/Request';

export default class MessageListRequest {
  static ApiAddress = '/messagelist';

  static GetMessageList(id) {
    return Request.CreateRequest('POST', MessageListRequest.ApiAddress.concat('/', id));
  }
}
