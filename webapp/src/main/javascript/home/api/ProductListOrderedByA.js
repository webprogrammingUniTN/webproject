import Request from '../../common/api/Request';

export default class ProductListOrderedByARequest {
  static ApiAddress = '/orderbyalphabet';

  static GetProductListOrderedByA(id) {
    return Request.CreateRequest('GET', ProductListOrderedByARequest.ApiAddress.concat('/', id));
  }
}
