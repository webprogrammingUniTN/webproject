import Request from '../../common/api/Request';

export default class ProductListOrderedByCatRequest {
  static ApiAddress = '/orderbycategory';

  static GetProductListOrderedByCat(id) {
    return Request.CreateRequest('GET', ProductListOrderedByCatRequest.ApiAddress.concat('/', id));
  }
}
