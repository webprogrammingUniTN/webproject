import Request from '../../common/api/Request';

// const applicationParameter = { contextURL: '/shoppinglist' };

export default class Messaging {
  static ApiAddress = Request.join(applicationParameter.contextURL, '/', 'messaging');

  static webSocket = null;

  static messageToSend = [];

  static WebSocketInit(onMessage, onClose, onFirstOpen) {
    if (Messaging.webSocket != null) {
      if (Messaging.webSocket.readyState !== Messaging.webSocket.CLOSED) {
        return;
      }
    }
    Messaging.webSocket = new WebSocket('ws://'.concat(location.host, this.ApiAddress));
    Messaging.webSocket.onclose = onClose;
    Messaging.webSocket.onmessage = (message) => {
      this.ParseMessage(onMessage, message);
    };
    Messaging.webSocket.onopen = () => {
      this.OnOpen();
      onFirstOpen();
    };
  }

  static WebSocketClose() {
    Messaging.webSocket.close();
    Messaging.webSocket = null;
  }

  static ParseMessage(callbackFunction, message) {
    const jsonData = JSON.parse(message.data);
    callbackFunction(jsonData);
  }

  static SendMessage(messageText, listID) {
    const objectToSend = {
      listId: listID,
      message: messageText,
    };
    if (Messaging.webSocket.readyState !== Messaging.webSocket.OPEN) {
      this.messageToSend.push(objectToSend);
      return;
    }
    Messaging.webSocket.send(JSON.stringify(objectToSend));
  }

  static OnOpen() {
    if (Messaging.webSocket.readyState !== Messaging.webSocket.OPEN) {
      while (this.messageToSend.length !== 0) {
        const message = this.messageToSend.shift();
        this.SendMessage(message.text, message.listId);
      }
    }
  }
}
