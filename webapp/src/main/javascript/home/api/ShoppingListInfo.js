import Request from '../../common/api/Request';

export default class ShoppingListInfoRequest {
  static ApiAddress = '/shoppinglistinfo';

  static GetShoppingListInfo(id) {
    return Request.CreateRequest('GET', ShoppingListInfoRequest.ApiAddress.concat('/', id));
  }
}
