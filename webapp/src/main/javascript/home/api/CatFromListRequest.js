import Request from '../../common/api/Request';

export default class CatFromListRequest {
  static ApiAddress = '/catfromlist';

  static GetCatList(shoppinglistId) {
    return Request.CreateRequest('GET', CatFromListRequest.ApiAddress.concat('/', shoppinglistId));
  }
}

