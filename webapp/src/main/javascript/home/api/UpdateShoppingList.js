import Request from '../../common/api/Request';

export default class UpdateShoppingListRequest {
  static ApiAddress = '/updateshoppinglist';

  static UpdateShoppingList(id, name, description, image, templateId) {
    console.log(image);
    const objectToSend = { id, name, description, image, templateId };
    console.log(objectToSend);
    return Request.CreateRequest('POST', UpdateShoppingListRequest.ApiAddress, objectToSend);
  }
}
