import Request from '../../common/api/Request';

export default class MessageTemplateListRequest {
  static ApiAddress = '/messagetemplatelist';

  static GetMessageTemplateList() {
    return Request.CreateRequest('GET', MessageTemplateListRequest.ApiAddress);
  }
}
