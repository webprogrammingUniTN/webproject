import Request from '../../common/api/Request';

export default class AllShoppingListsRequest {
  static ApiAddress = '/allshoppinglists';

  static GetAllShoppingLists() {
    return Request.CreateRequest('GET', AllShoppingListsRequest.ApiAddress);
  }
}
