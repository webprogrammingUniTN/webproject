import Request from '../../common/api/Request';

export default class ProductInListRequest {
  static ApiAddress = '/productinlist';

  static Add(body) {
    return Request.CreateRequest('PUT', ProductInListRequest.ApiAddress, body);
  }

  static Remove(body) {
    return Request.CreateRequest('DELETE', ProductInListRequest.ApiAddress, body);
  }

  static Done(body) {
    return Request.CreateRequest('POST', ProductInListRequest.ApiAddress, body);
  }
}

