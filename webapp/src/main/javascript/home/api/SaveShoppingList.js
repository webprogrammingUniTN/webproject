import Request from '../../common/api/Request';

export default class SaveShoppingListRequest {
  static ApiAddress = '/saveshoppinglist';

  static SaveShoppingList(name, description, image, templateId) {
    console.log(image);
    const objectToSend = {
      name, description, image, templateId,
    };
    console.log(objectToSend);
    return Request.CreateRequest('POST', SaveShoppingListRequest.ApiAddress, objectToSend);
  }
}
