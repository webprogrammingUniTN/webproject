export default class MessageListResponse {
  constructor(numberOfTotalElement, messages) {
    if (!arguments.length) {
      throw new Error('Not define constructor');
    } else if (arguments.length === 1) {
      const object = numberOfTotalElement;
      this.numberOfTotalElement = object.numberOfTotalElement;
      this.messages = object.messages;
    } else if (arguments.length >= 2) {
      this.numberOfTotalElement = numberOfTotalElement;
      this.messages = messages;
    }
  }
}
