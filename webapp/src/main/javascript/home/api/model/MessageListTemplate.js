export default class MessageListTemplate {
  static DESCRIPTION_FIELD_NAME = 'description';

  constructor(description) {
    if (!arguments.length) {
      this.description = '';
    } else if (arguments.length === 1) {
      const object = description;
      this.description = object.description;
    }
  }
}
