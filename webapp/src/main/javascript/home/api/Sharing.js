import Request from '../../common/api/Request';

export default class SharingRequest {
  static ApiAddressShoppingList = '/shoppinglist/';
  static ApiAddressSharing = '/sharing';

  static GetSharing(shoppingListId) {
    return Request.CreateRequest('GET', SharingRequest.ApiAddressShoppingList + shoppingListId + SharingRequest.ApiAddressSharing);
  }

  static AddSharing(shoppingListId, body) {
    return Request.CreateRequest('POST', SharingRequest.ApiAddressShoppingList + shoppingListId + SharingRequest.ApiAddressSharing, body);
  }
}
