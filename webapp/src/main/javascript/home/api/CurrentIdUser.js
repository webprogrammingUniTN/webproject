import Request from '../../common/api/Request';

export default class CurrentIdUserRequest {
  static ApiAddress = '/currentiduser';

  static GetCurrentIdUser() {
    return Request.CreateRequest('GET', CurrentIdUserRequest.ApiAddress);
  }
}
