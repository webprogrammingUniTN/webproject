import Request from '../../common/api/Request';

export default class CurrentListRequest {
  static ApiAddress = '/currentlist';

  static SetCurrentList(shoppinglistId) {
    return Request.CreateRequest('POST', CurrentListRequest.ApiAddress.concat('/', shoppinglistId));
  }
}
