import Request from '../../common/api/Request';

export default class AllProductListRequest {
  static ApiAddress = '/allproductlist';

  static GetAllProductList(id) {
    return Request.CreateRequest('GET', AllProductListRequest.ApiAddress.concat('/', id));
  }
}
