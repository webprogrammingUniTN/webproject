import Request from '../../common/api/Request';

export default class TemplateListRequest {
  static ApiAddress = '/templatelistinfo';

  static GetTemplateList() {
    return Request.CreateRequest('GET', TemplateListRequest.ApiAddress);
  }
}
