import Request from '../../common/api/Request';

export default class DeleteShoppingListRequest {
  static ApiAddress = '/deleteshoppinglist';

  static DeleteShoppingList(id) {
    return Request.CreateRequest('GET', DeleteShoppingListRequest.ApiAddress.concat('/', id));
  }
}
