export default class ShoppingListTemplate {
  static DESCRIPTION_FIELD_NAME = 'description';

  static NAME_FIELD_NAME = 'name';

  constructor(name, description, disabled, logo) {
    if (!arguments.length) {
      this.name = '';
      this.description = '';
      this.disabled = false;
      this.logo = [];
    } else if (arguments.length === 1) {
      const object = name;
      this.id = object.id;
      this.name = object.name;
      this.description = object.description;
      this.disabled = object.disabled;
      this.images = object.images;
      this.categories = object.category;
      this.geolocationIdentifier = object.geolocationIdentifier;
    } else if (arguments.length >= 4) {
      this.name = name;
      this.description = description;
      this.disabled = disabled;
      this.logo = logo;
    }
  }

  isValid(fieldName) {
    if (fieldName === ShoppingListTemplate.DESCRIPTION_FIELD_NAME || fieldName === undefined) {
      if (this.description == null) {
        return 'Description can\'t be null';
      }
      if (this.description.length <= 0) {
        return 'Description is empty';
      }
    }
    if (fieldName === ShoppingListTemplate.NAME_FIELD_NAME || fieldName === undefined) {
      if (this.name == null) {
        return 'Name can\'t be null';
      }
      if (this.name.length <= 0) {
        return 'Name is empty';
      }
    }
    if (fieldName === undefined) {
      if (this.images == null) {
        return 'Image can\'t be null';
      }
    }
    return '';
  }
}
