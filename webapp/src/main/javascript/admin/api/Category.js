import Request from '../../common/api/Request';

export default class CategoryRequest {
  static ApiAddress = '/category';

  static GetCategoryList(params) {
    return Request.CreateRequest('GET', CategoryRequest.ApiAddress.concat(Request.ParamEncoding(params)));
  }

  static AddCategory(name, description, disabled, logo) {
    const category = { name, description, disabled, logo };
    return Request.CreateRequest('POST', CategoryRequest.ApiAddress, category);
  }

  static UpdateCategory(id, name, description, disabled, logo) {
    const category = {
      id, name, description, disabled, logo,
    };
    return Request.CreateRequest('PUT', CategoryRequest.ApiAddress, category);
  }

  static SingleCategory(id) {
    return Request.CreateRequest('GET', CategoryRequest.ApiAddress.concat('/', id));
  }
}
