package com.soapandtherest.shoppinglist.tld.webpack;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import org.json.JSONException;
import org.json.JSONObject;

public class ManifestTag extends TagSupport {
	private String filename;

	public void setFilename(String filename) {
		this.filename = filename.trim();
	}
	@Override
	public int doStartTag() {
		JspWriter out = pageContext.getOut();
		try {
			int lastIndexOfPointIntoFilename = this.filename.lastIndexOf('.');
			String extension;
			String manifestResourceURI;
			if (lastIndexOfPointIntoFilename == -1) {
				manifestResourceURI = filename;
				extension = "unknown";
			} else {
				manifestResourceURI = getManifestResource(this.filename, pageContext);
				extension = this.filename.substring(lastIndexOfPointIntoFilename + 1);
			}
			if (!manifestResourceURI.isEmpty()) {
				if (extension.equals("css")) {
					out.println("<link rel=\"stylesheet\" href=\"" + manifestResourceURI + "\"/>");
				} else {
					out.println("<script src=\"" + getManifestResource(this.filename, pageContext) + "\"></script>");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	private final static Boolean blockReading = false;
	private static Map<String, String> importManifest = null;
	private static String getManifestResource(String filename, javax.servlet.jsp.PageContext context) {
		if (importManifest == null) {
			readManifest(context.getServletContext().getRealPath("/"));
		}
		return importManifest.getOrDefault(filename, "");
	}
	private static void readManifest(String contextPath) {
		synchronized (blockReading) {
			if (importManifest != null) {
				return;
			}
			String manifestString = "{}";
			try {
				BufferedReader br = new BufferedReader(
						new FileReader(String.valueOf(Paths.get(contextPath, "manifest.json"))));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();
				while (line != null) {
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
					manifestString = sb.toString();
				}
				JSONObject obj = new JSONObject(manifestString);
				Map<String, String> manifest = new LinkedHashMap<>();
				obj.keys().forEachRemaining((Object key) -> {
					if (key instanceof String) {
						try {
							String value = obj.getString((String) key);
							manifest.put((String) key, value);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				});
				importManifest = manifest;
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
