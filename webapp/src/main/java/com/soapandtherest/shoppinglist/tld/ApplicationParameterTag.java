package com.soapandtherest.shoppinglist.tld;

import com.google.gson.Gson;
import java.io.IOException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class ApplicationParameterTag extends TagSupport {
	@Override
	public int doStartTag() {
		Gson gson = new Gson();
		JspWriter out = pageContext.getOut();
		try {
			Parameter parameter = new Parameter(pageContext.getServletContext().getContextPath());
			String result = gson.toJson(parameter);
			out.println("<script>var applicationParameter = " + result + ";</script>");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	private static class Parameter {
		/**
		 * Contain the application base URL
		 */
		private String contextURL;

		public Parameter(String contextURL) {
			this.contextURL = contextURL;
		}

		public String getContextURL() {
			return contextURL;
		}

		public void setContextURL(String contextURL) {
			this.contextURL = contextURL;
		}
	}
}
