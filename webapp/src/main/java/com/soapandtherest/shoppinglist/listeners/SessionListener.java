package com.soapandtherest.shoppinglist.listeners;

import com.soapandtherest.shoppinglist.controllers.File;
import java.util.ArrayList;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent httpSessionEvent) {
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
		Object uploadFilesList = httpSessionEvent.getSession().getAttribute(File.FILES_LIST_SESSION_KEY);
		ArrayList sessionFiles = null;
		if (uploadFilesList instanceof ArrayList) {
			sessionFiles = (ArrayList) httpSessionEvent.getSession().getAttribute(File.FILES_LIST_SESSION_KEY);
			for (Object file : sessionFiles) {
				if (file instanceof com.soapandtherest.shoppinglist.data.file.File) {
					File.removeFile(httpSessionEvent.getSession(),
							(com.soapandtherest.shoppinglist.data.file.File) file, true);
				}
			}
		}
	}
}
