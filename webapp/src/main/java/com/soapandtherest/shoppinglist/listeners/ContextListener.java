package com.soapandtherest.shoppinglist.listeners;

import com.soapandtherest.shoppinglist.cron.CronJobHandler;
import com.soapandtherest.shoppinglist.data.file.AWSFactory;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons.DataSourceProvider;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		// AWS Factory startup
		AWSFactory.getInstance();
		CronJobHandler.scheduleJob();
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		DataSourceProvider.closeConnectionPool();
		CronJobHandler.stopJob();
		AWSFactory.getInstance().closeConnections();
	}
}
