package com.soapandtherest.shoppinglist.exception;

public class UnAuthorizedException extends Exception {
	public UnAuthorizedException(String message) {
		super(message);
	}
}
