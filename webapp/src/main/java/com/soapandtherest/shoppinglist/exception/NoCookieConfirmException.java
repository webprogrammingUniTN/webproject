package com.soapandtherest.shoppinglist.exception;

public class NoCookieConfirmException extends Exception {
	public NoCookieConfirmException(String message) {
		super(message);
	}
}
