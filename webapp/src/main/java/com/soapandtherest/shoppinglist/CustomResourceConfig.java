package com.soapandtherest.shoppinglist;

import com.soapandtherest.shoppinglist.filters.AuthenticationFilter;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;

@ApplicationPath("/")
public class CustomResourceConfig extends ResourceConfig {
	public CustomResourceConfig() {
		this.packages("com.soapandtherest.shoppinglist.controllers", "com.soapandtherest.shoppinglist.utilProvider",
				"com.soapandtherest.shoppinglist.errorhandling");

		this.property(JspMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/jsp/");
		// Utility register
		this.register(JspMvcFeature.class);
		// Authentication register
		this.register(AuthenticationFilter.class);
		// this.register(MultiPartFeature.class);
	}
}
