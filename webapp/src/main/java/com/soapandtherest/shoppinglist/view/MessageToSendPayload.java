/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

/**
 *
 * @author mousl
 */
public class MessageToSendPayload {
	private String message;
	private long listId;

	public MessageToSendPayload(String message, long listId) {
		this.message = message;
		this.listId = listId;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the listId
	 */
	public long getListId() {
		return listId;
	}

	/**
	 * @param listId
	 *            the listId to set
	 */
	public void setListId(long listId) {
		this.listId = listId;
	}

}
