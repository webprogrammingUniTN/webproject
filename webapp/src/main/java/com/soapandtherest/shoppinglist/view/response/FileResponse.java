package com.soapandtherest.shoppinglist.view.response;

import java.net.URL;

public class FileResponse {
	private URL url;

	private String objectKey;

	public FileResponse(URL url, String objectKey) {
		this.url = url;
		this.objectKey = objectKey;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}
}
