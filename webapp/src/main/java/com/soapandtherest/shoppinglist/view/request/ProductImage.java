package com.soapandtherest.shoppinglist.view.request;

public class ProductImage {

	private com.soapandtherest.shoppinglist.view.File avatar;
	private Long idProduct;

	/**
	 * @return the avatar
	 */
	public com.soapandtherest.shoppinglist.view.File getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar
	 *            the avatar to set
	 */
	public void setAvatar(com.soapandtherest.shoppinglist.view.File avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return the idProduct
	 */
	public Long getIdProduct() {
		return idProduct;
	}

	/**
	 * @param idProduct
	 *            the idProduct to set
	 */
	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

}
