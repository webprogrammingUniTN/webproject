package com.soapandtherest.shoppinglist.view.response;

public class CatFromListResponse extends StandardResponse {

	private Long slId;
	private String slName;
	private Long slTemplateId;
	private String slTemplateName;
	private Long[] catIds;
	private String[] catNames;
	private Long[] productTemplateIds;
	private String[] productTemplateNames;
	private Long[] productTemplateCatIds;
	private String[] productTemplateCatNames;
	private Long[] productIds;
	private String[] productNames;
	private Long[] productCatIds;
	private String[] productCatNames;
	private Item[] items;

	public CatFromListResponse(int code, String message) {
		super(code, message);
	}

	public CatFromListResponse() {
		super(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

	public void setError(int code, String message) {
		super.setCode(code);
		super.setMessage(message);
	}

	public Long getSlId() {
		return slId;
	}

	public void setSlId(Long slId) {
		this.slId = slId;
	}

	public String getSlName() {
		return slName;
	}

	public void setSlName(String slName) {
		this.slName = slName;
	}

	public Long getSlTemplateId() {
		return slTemplateId;
	}

	public void setSlTemplateId(Long slTemplateId) {
		this.slTemplateId = slTemplateId;
	}

	public String getSlTemplateName() {
		return slTemplateName;
	}

	public void setSlTemplateName(String slTemplateName) {
		this.slTemplateName = slTemplateName;
	}

	public Long[] getCatIds() {
		return catIds;
	}

	public void setCatIds(Long[] catIds) {
		this.catIds = catIds;
	}

	public String[] getCatNames() {
		return catNames;
	}

	public void setCatNames(String[] catNames) {
		this.catNames = catNames;
	}

	public Long[] getProductTemplateIds() {
		return productTemplateIds;
	}

	public void setProductTemplateIds(Long[] productTemplateIds) {
		this.productTemplateIds = productTemplateIds;
	}

	public String[] getProductTemplateNames() {
		return productTemplateNames;
	}

	public void setProductTemplateNames(String[] productTemplateNames) {
		this.productTemplateNames = productTemplateNames;
	}

	public Long[] getProductTemplateCatIds() {
		return productTemplateCatIds;
	}

	public void setProductTemplateCatIds(Long[] productTemplateCatIds) {
		this.productTemplateCatIds = productTemplateCatIds;
	}

	public String[] getProductTemplateCatNames() {
		return productTemplateCatNames;
	}

	public void setProductTemplateCatNames(String[] productTemplateCatNames) {
		this.productTemplateCatNames = productTemplateCatNames;
	}

	public Long[] getProductIds() {
		return productIds;
	}

	public void setProductIds(Long[] productIds) {
		this.productIds = productIds;
	}

	public String[] getProductNames() {
		return productNames;
	}

	public void setProductNames(String[] productNames) {
		this.productNames = productNames;
	}

	public Long[] getProductCatIds() {
		return productCatIds;
	}

	public void setProductCatIds(Long[] productCatIds) {
		this.productCatIds = productCatIds;
	}

	public String[] getProductCatNames() {
		return productCatNames;
	}

	public void setProductCatNames(String[] productCatNames) {
		this.productCatNames = productCatNames;
	}

	public Item[] getItems() {
		return items;
	}

	public void setItems(Item[] items) {
		this.items = items;
	}
}
