/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view.response;

/**
 *
 * @author Gionni_2
 */
public class Item {
	private Long id;
	private String cat;
	private String name;
	private String description;
	private com.soapandtherest.shoppinglist.view.File[] image;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getCat() {
		return cat;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	/**
	 * @return the image
	 */
	public com.soapandtherest.shoppinglist.view.File[] getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(com.soapandtherest.shoppinglist.view.File[] image) {
		this.image = image;
	}

}
