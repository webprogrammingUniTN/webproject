/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

/**
 *
 * @author vbeatrice
 */
import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.persistence.ProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.record.ProductRecord;
import com.soapandtherest.shoppinglist.data.record.ProductTemplateRecord;
import com.soapandtherest.shoppinglist.data.persistence.ProductTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductCategoriesPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListHasProductRecord;

public class Product implements Comparable<Product> {
	protected String name;
	private Long idproduct_category;
	private String catName;
	private String note;
	private Long idProduct;
	private com.soapandtherest.shoppinglist.view.File[] logo;
	private transient Image logo_image;
	private com.soapandtherest.shoppinglist.view.File[] image;
	private transient Image image_image;

	public Product(String name, Long product_categories_idproduct_categories) {
		this.name = name;
		this.idproduct_category = product_categories_idproduct_categories;
	}

	public Product() {

	}

	public Product(ProductRecord record) {
		this.name = record.getName();
		this.idproduct_category = this.getIdProductCategory(record.getProductTemplateIdproductTemplate());
		this.catName = getCatName(idproduct_category);
		this.note = record.getNote();
		this.idProduct = record.getIdproduct();

		try {
			logo_image = new Image(record.getLogo());
			logo = new com.soapandtherest.shoppinglist.view.File[1];
			logo[0] = new com.soapandtherest.shoppinglist.view.File(logo_image.GetPresignedURL(Image.ImageSize.SMALL),
					record.getLogo());
		} catch (Exception e) {
			logo_image = null;
			logo = null;
		}

		try {
			image_image = new Image(record.getImage());
			image = new com.soapandtherest.shoppinglist.view.File[1];
			image[0] = new com.soapandtherest.shoppinglist.view.File(image_image.GetPresignedURL(Image.ImageSize.SMALL),
					record.getImage());
		} catch (Exception e) {
			image_image = null;
			image = null;
		}

	}

	public Long getIdproduct_category() {
		return idproduct_category;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Product o) {
		if (this.equals(o))
			return 0;
		if (idproduct_category < o.getIdproduct_category())
			return -1;
		else
			return 1;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note
	 *            the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the idProduct
	 */
	public Long getIdProduct() {
		return idProduct;
	}

	/**
	 * @param idProduct
	 *            the idProduct to set
	 */
	public void setIdProduct(Long idProduct) {
		this.idProduct = idProduct;
	}

	/**
	 * @return the logo
	 */
	public com.soapandtherest.shoppinglist.view.File[] getLogo() {
		return logo;
	}

	/**
	 * @param logo
	 *            the logo to set
	 */
	public void setLogo(com.soapandtherest.shoppinglist.view.File[] logo) {
		this.logo = logo;
	}

	/**
	 * @return the image
	 */
	public com.soapandtherest.shoppinglist.view.File[] getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(com.soapandtherest.shoppinglist.view.File[] image) {
		this.image = image;
	}

	private String getImageUrl(Image img) {
		if (img == null)
			return "";
		return img.GetPresignedURL(Image.ImageSize.SMALL).toString();
	}

	private Long getIdProductCategory(Long productTemplateIdProductTemplate) {
		ProductTemplatePersistence persistenceService = new ProductTemplatePersistenceJdbc();
		ProductTemplateRecord record = persistenceService.findById(productTemplateIdProductTemplate);
		return record.getProductCategoriesIdproductCategories();
	}

	private String getCatName(Long idCat) {
		ProductCategoriesPersistence persistenceService = new ProductCategoriesPersistenceJdbc();
		ProductCategoriesRecord record = persistenceService.findById(idCat);
		return record.getName();
	}

	/**
	 * @return the catName
	 */
	public String getCatName() {
		return catName;
	}

	/**
	 * @param catName
	 *            the catName to set
	 */
	public void setCatName(String catName) {
		this.catName = catName;
	}
}
