package com.soapandtherest.shoppinglist.view.request;

import static com.soapandtherest.shoppinglist.view.request.AuthenticationUtil.validateEmail;

public class Forgotten {

	public Forgotten(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private String email;

	public String isValid() {
		if (!validateEmail(email)) {
			return "Email non valida.";
		}
		return "";
	}
}
