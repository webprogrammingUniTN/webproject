package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.persistence.ProductTemplatePersistence;
import com.soapandtherest.shoppinglist.data.record.ProductTemplateRecord;
import javax.servlet.http.HttpSession;

public class ProductTemplate extends NameDescriptionLogoDisableView {
	private Long id;

	public ProductTemplate(ProductTemplateRecord record) {
		super(record.getName(), record.getDescription(), record.getLogo(), record.getDisabled());
		this.setId(record.getIdproductTemplate());
	}

	public String isValidForAdd(HttpSession session) {
		id = null;
		return super.isValidForAdd(session, false);
	}

	public String isValidForUpdate(HttpSession session, ProductTemplateRecord oldProductTemplate) {
		return super.isValidForUpdate(session, oldProductTemplate.getLogo(), false);
	}

	public ProductTemplateRecord getProductTemplateRecord(long categoryId) {
		ProductTemplateRecord record = new ProductTemplateRecord();
		record.setDescription(this.getDescription());
		record.setIdproductTemplate(this.id);
		record.setName(this.getName());
		record.setLogo(this.getLogoFile().getObjectKey());
		record.setProductCategoriesIdproductCategories(categoryId);
		record.setDisabled(this.getDisabled());
		return record;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void save(ProductTemplatePersistence persistence, long categoryId) {
		super.saveImage();
		ProductTemplateRecord record = this.getProductTemplateRecord(categoryId);
		if (this.getId() == null) {
			record = persistence.create(record);
			this.setId(record.getIdproductTemplate());
		} else {
			persistence.update(record);
		}
	}
}
