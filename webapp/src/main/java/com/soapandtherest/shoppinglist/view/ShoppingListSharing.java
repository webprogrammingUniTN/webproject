package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import java.util.Date;

public class ShoppingListSharing {

	private Long userIduser;
	private Long shoppingListIdshoppingList;
	private Boolean listOwner;
	private Boolean canDeleteElement;
	private Boolean canEditElement;

	public ShoppingListSharing(UserHasShoppingListRecord record) {
		this.userIduser = record.getUserIduser();
		this.shoppingListIdshoppingList = record.getShoppingListIdshoppingList();
		this.listOwner = record.getListOwner();
		this.canEditElement = record.getCanEditElement();
		this.canDeleteElement = record.getCanDeleteElement();
	}

	public Long getUserIduser() {
		return userIduser;
	}

	public Long getShoppingListIdshoppingList() {
		return shoppingListIdshoppingList;
	}

	public Boolean getListOwner() {
		return listOwner;
	}

	public Boolean getCanDeleteElement() {
		return canDeleteElement;
	}

	public Boolean getCanEditElement() {
		return canEditElement;
	}
}
