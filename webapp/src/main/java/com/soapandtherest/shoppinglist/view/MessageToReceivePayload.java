/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.websocket.Messaging;

/**
 *
 * @author mousl
 */
public class MessageToReceivePayload {
	private boolean currentUser;
	private String currentUserName;
	private Long idList;
	private MessageTemplate msg;

	public MessageToReceivePayload(boolean currentUser, String currentUserName, Long idList, MessageTemplate msg) {
		this.currentUser = currentUser;
		this.currentUserName = currentUserName;
		this.idList = idList;
		this.msg = msg;
	}
}
