/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view.request;

/**
 *
 * @author Gionni_2
 */
public class ProductInList {
	private Long shoppinglistId;
	private Long productId;
	private String createTime;

	public void setShoppinglistId(Long shoppinglistId) {
		this.shoppinglistId = shoppinglistId;
	}

	public Long getShoppinlistId() {
		return shoppinglistId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getProductId() {
		return productId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
