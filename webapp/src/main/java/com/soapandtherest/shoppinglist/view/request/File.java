package com.soapandtherest.shoppinglist.view.request;

public class File {
	private int byteContentSize;

	public File(int byteContentSize) {
		this.byteContentSize = byteContentSize;
	}

	public int getByteContentSize() {
		return byteContentSize;
	}

	public void setByteContentSize(int byteContentSize) {
		this.byteContentSize = byteContentSize;
	}
}
