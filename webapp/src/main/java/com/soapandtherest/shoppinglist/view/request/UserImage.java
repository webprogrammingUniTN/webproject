package com.soapandtherest.shoppinglist.view.request;

public class UserImage {

	private com.soapandtherest.shoppinglist.view.File avatar;

	/**
	 * @return the avatar
	 */
	public com.soapandtherest.shoppinglist.view.File getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar
	 *            the avatar to set
	 */
	public void setAvatar(com.soapandtherest.shoppinglist.view.File avatar) {
		this.avatar = avatar;
	}

}
