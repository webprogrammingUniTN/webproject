/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.file.Image.ImageSize;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplateHasFilePersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListTemplateHasFilePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasFileRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.util.ImageChecking;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author mousl
 */
public class ShoppingList {
	private static ShoppingListTemplateHasFilePersistence persistence = new ShoppingListTemplateHasFilePersistenceJdbc();
	private static ShoppingListPersistence persistenceShoppinglist = new ShoppingListPersistenceJdbc();
	private Long id;
	private String name;
	private String description;
	private File image;
	private long templateId;
	private Image file;
	private List<Image> files;
	private boolean list_owner;
	private boolean can_delete_element;
	private boolean can_edit_element;
	private boolean can_edit_list_detail;
	private boolean can_delete_list;
	private boolean can_add_element;

	public ShoppingList(Long id, String name, String description, File image, long templateId, Image file,
			boolean list_owner, boolean can_delete_element, boolean can_edit_element, boolean can_edit_list_detail,
			boolean can_delete_list, boolean can_add_element) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.file = file;
		this.templateId = templateId;
		this.image = image;
		this.list_owner = list_owner;
		this.can_delete_element = can_delete_element;
		this.can_edit_element = can_edit_element;
		this.can_edit_list_detail = can_edit_list_detail;
		this.can_delete_list = can_delete_list;
		this.can_add_element = can_add_element;
	}

	public ShoppingList(Long id, String name, String description, File image, long templateId, Image file) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.file = file;
		this.templateId = templateId;
		this.image = image;
	}

	public ShoppingList(ShoppingListRecord record, UserHasShoppingListRecord userRecord) {
		this.id = record.getIdshoppingList();
		this.name = record.getName();
		this.description = record.getDescription();
		this.templateId = record.getTemplateId();
		this.file = new Image(record.getImage(), Boolean.FALSE);
		this.image = new File(file.GetPresignedURL(ImageSize.MEDIUM), file.getObjectKey());
		this.list_owner = userRecord.getListOwner();
		this.can_delete_element = userRecord.getCanDeleteElement();
		this.can_edit_element = userRecord.getCanEditElement();
		this.can_edit_list_detail = userRecord.getCanEditListDetail();
		this.can_delete_list = userRecord.getCanDeleteList();
		this.can_add_element = userRecord.getCanAddElement();
	}

	public ShoppingList(ShoppingListRecord record) {
		this.id = record.getIdshoppingList();
		this.name = record.getName();
		this.description = record.getDescription();
		this.templateId = record.getTemplateId();
		this.file = new Image(record.getImage(), Boolean.FALSE);
		this.image = new File(file.GetPresignedURL(ImageSize.MEDIUM), file.getObjectKey());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the image
	 */
	public File getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(File image) {
		this.image = image;
	}

	/**
	 * @return the templateId
	 */
	public long getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(long templateId) {
		this.templateId = templateId;
	}

	public String isValid(HttpSession session) {
		this.file = new Image(this.image.getObjectKey());
		if (description == null) {
			return "Description can't be null";
		}
		if (description.isEmpty()) {
			return "Description is empty";
		}
		if (name == null) {
			return "Name can't be null";
		}
		if (name.isEmpty()) {
			return "Name is empty";
		}
		if (image == null) {
			return "Image can't be null";
		}
		Image imageFile = new Image(image.getObjectKey());
		String imageValidationResult = "";
		if (this.id != null) {
			ShoppingListRecord template = persistenceShoppinglist.findById(this.id);
			imageValidationResult = image.isValidWithListCheck(imageFile, session, new CheckTemplateImageAndOldImage(
					persistence.findByTemplateId(this.templateId), template.getImage()));
		} else {
			List<ShoppingListTemplateHasFileRecord> records = persistence.findByTemplateId(this.templateId);
			imageValidationResult = image.isValidWithListCheck(imageFile, session,
					new CheckTemplateImageAndOldImage(records, ""));
		}
		if (!imageValidationResult.isEmpty()) {
			return imageValidationResult;
		}

		return "";
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	public Image getFile() {
		return file;
	}

	protected class CheckTemplateImageAndOldImage implements ImageChecking {
		private List<ShoppingListTemplateHasFileRecord> templateImage;
		private String idOldImage;

		public CheckTemplateImageAndOldImage(List<ShoppingListTemplateHasFileRecord> templateImage, String idOldImage) {
			this.templateImage = templateImage;
			this.idOldImage = idOldImage;
		}

		@Override
		public boolean checkImageExistIntoList(Image image) {
			if (idOldImage.equals(image.getObjectKey())) {
				return true;
			}
			for (ShoppingListTemplateHasFileRecord record : templateImage) {
				if (record.getKey().equals(image.getObjectKey())) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * @return the files
	 */
	public List<Image> getFiles() {
		return files;
	}

	/**
	 * @param files
	 *            the files to set
	 */
	public void setFiles(List<Image> files) {
		this.files = files;
	}
}
