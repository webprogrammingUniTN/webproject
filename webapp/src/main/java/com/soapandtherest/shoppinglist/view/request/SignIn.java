package com.soapandtherest.shoppinglist.view.request;

import com.soapandtherest.shoppinglist.controllers.ShoppingListProductTemplate;
import static com.soapandtherest.shoppinglist.view.request.AuthenticationUtil.PasswordCheckResult.PWD_OK;
import static com.soapandtherest.shoppinglist.view.request.AuthenticationUtil.validateEmail;
import static com.soapandtherest.shoppinglist.view.request.AuthenticationUtil.validatePassword;
import org.slf4j.LoggerFactory;

public class SignIn {

	private static org.slf4j.Logger logger = LoggerFactory.getLogger(ShoppingListProductTemplate.class);

	private String email;
	private String password;
	private String rememberMe;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(String rememberMe) {
		this.rememberMe = rememberMe;
	}

	public SignIn(String email, String password, String rememberMe) {
		this.email = email;
		this.password = password;
		this.rememberMe = rememberMe;
	}

	public String isValid() {
		if (!validateEmail(email)) {
			logger.error("Email: not valid syntax");
			return "Utente e/o password non validi!";
		}
		if (validatePassword(password) != PWD_OK) {
			logger.error("Password: not valid syntax");
			return "Utente e/o Password non validi!";
		}
		return "";
	}

	public Boolean isRememberMe() {
		return rememberMe.equals("Y");
	}

}
