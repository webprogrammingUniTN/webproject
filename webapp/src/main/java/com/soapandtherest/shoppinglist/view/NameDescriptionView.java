package com.soapandtherest.shoppinglist.view;

public abstract class NameDescriptionView {
	private String name;

	private String description;

	public NameDescriptionView(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	protected String isValid() {
		if (this.name == null) {
			return "Name can't be null";
		}
		if (name.isEmpty()) {
			return "Name is empty";
		}
		if (description == null) {
			return "Description can't be null";
		}
		if (description.isEmpty()) {
			return "Description is empty";
		}
		return "";
	}
}
