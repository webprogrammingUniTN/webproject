/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.record.MessageTemplateRecord;

/**
 *
 * @author mousl
 */
public class MessageTemplate {
	private String messagetemplate;
	private Long userSender;

	public MessageTemplate(String messagetemplate) {
		this.messagetemplate = messagetemplate;
	}

	public MessageTemplate(MessageTemplateRecord record, Long userSender) {
		this.messagetemplate = record.getMessageDescription();
		this.userSender = userSender;
	}

	public MessageTemplate(MessageTemplateRecord record) {
		this.messagetemplate = record.getMessageDescription();
	}

	/**
	 * @return the messagetemplate
	 */
	public String getMessageTemplate() {
		return messagetemplate;
	}

	/**
	 * @param messagetemplate
	 *            the messagetemplate to set
	 */
	public void setMessageTemplate(String messagetemplate) {
		this.messagetemplate = messagetemplate;
	}

	/**
	 * @return the user_sender
	 */
	public Long getUser_sender() {
		return userSender;
	}
}
