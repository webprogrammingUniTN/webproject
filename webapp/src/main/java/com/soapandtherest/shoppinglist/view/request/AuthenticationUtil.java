package com.soapandtherest.shoppinglist.view.request;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

abstract class AuthenticationUtil {
	private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern
			.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

	enum PasswordCheckResult {
		PWD_NULL(-1), PWD_WITH_SPACES(-2), PWD_CHARSET(-3), PWD_OK(0);

		private int value;

		// Constructor
		PasswordCheckResult(int value) {
			this.value = value;
		}
	}

	static boolean validateEmail(String emailStr) {
		if (emailStr == null)
			return false;
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	static PasswordCheckResult validatePassword(String pwd) {
		if (pwd == null)
			return PasswordCheckResult.PWD_NULL;
		int Num = 0;
		int Low = 0;
		int Upp = 0;
		int Spec = 0;
		int Spaces = 0;
		for (int i = 0; i < pwd.length(); i++) {
			char c = pwd.charAt(i);
			if ((c >= '0') && (c <= '9'))
				Num++;
			if ((c >= 'a') && (c <= 'z'))
				Low++;
			if ((c >= 'A') && (c <= 'Z'))
				Upp++;
			if ((c == '!') || (c == '@') || (c == '#') || (c == '$') || (c == '£') || (c == '%') || (c == '&')
					|| (c == '^') || (c == '+') || (c == '-') || (c == '*') || (c == '/') || (c == '\\') || (c == '|')
					|| (c == '"') || (c == '_'))
				Spec++;
			if (c == ' ')
				Spaces++;
		}
		if (Spaces != 0) {
			return PasswordCheckResult.PWD_WITH_SPACES;
		}
		int nSet = 0;
		if (Num > 0)
			nSet++;
		if (Low > 0)
			nSet++;
		if (Upp > 0)
			nSet++;
		if (Spec > 0)
			nSet++;
		return (nSet >= 3) ? PasswordCheckResult.PWD_OK : PasswordCheckResult.PWD_CHARSET;
	}
}
