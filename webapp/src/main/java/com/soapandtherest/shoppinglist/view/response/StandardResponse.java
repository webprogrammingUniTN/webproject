package com.soapandtherest.shoppinglist.view.response;

public class StandardResponse {
	public static final int INVALID_FORMAT_CODE = 10;
	public static final int UNAUTHORIZED_FORMAT_CODE = 40;
	public static final int VALID_CODE_RESPONSE = 20;
	public static final String VALID_MESSAGE_RESPONSE = "OK";
	private int code;
	private String message;

	public StandardResponse(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
