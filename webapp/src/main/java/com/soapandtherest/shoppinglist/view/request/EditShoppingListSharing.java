package com.soapandtherest.shoppinglist.view.request;

import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;

public class EditShoppingListSharing {
	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public Boolean getCanEditListDetail() {
		return canEditListDetail;
	}

	public void setCanEditListDetail(Boolean canEditListDetail) {
		this.canEditListDetail = canEditListDetail;
	}

	public Boolean getCanDeleteElement() {
		return canDeleteElement;
	}

	public void setCanDeleteElement(Boolean canDeleteElement) {
		this.canDeleteElement = canDeleteElement;
	}

	public Boolean getCanAddElement() {
		return canAddElement;
	}

	public void setCanAddElement(Boolean canAddElement) {
		this.canAddElement = canAddElement;
	}

	public Boolean getCanDeleteList() {
		return canDeleteList;
	}

	public void setCanDeleteList(Boolean canDeleteList) {
		this.canDeleteList = canDeleteList;
	}

	public Boolean getCanEditElement() {
		return canEditElement;
	}

	public void setCanEditElement(Boolean canEditElement) {
		this.canEditElement = canEditElement;
	}

	private String userEmail;
	private Boolean canEditListDetail;
	private Boolean canDeleteList;
	private Boolean canDeleteElement;
	private Boolean canAddElement;
	private Boolean canEditElement;

	public EditShoppingListSharing(String userEmail, Boolean canEditListDetail, Boolean canDeleteElement,
			Boolean canAddElement, Boolean canDeleteList, Boolean canEditElement) {
		this.userEmail = userEmail;
		this.canEditListDetail = canEditListDetail;
		this.canDeleteElement = canDeleteElement;
		this.canAddElement = canAddElement;
		this.canDeleteList = canDeleteList;
		this.canEditElement = canEditElement;
	}

	public EditShoppingListSharing(UserHasShoppingListRecord record, String userEmail) {
		this.setCanAddElement(record.getCanAddElement());
		this.setCanDeleteElement(record.getCanDeleteElement());
		this.setCanDeleteList(record.getCanDeleteList());
		this.setCanEditElement(record.getCanEditElement());
		this.setCanEditListDetail(record.getCanEditListDetail());
		this.setUserEmail(userEmail);
	}

	protected EditShoppingListSharing(EditShoppingListSharing element) {
		this.userEmail = element.userEmail;
		this.canEditListDetail = element.canEditListDetail;
		this.canDeleteElement = element.canDeleteElement;
		this.canAddElement = element.canAddElement;
		this.canDeleteList = element.canDeleteList;
		this.canEditElement = element.canEditElement;
	}

	public UserHasShoppingListRecord getUserHasShoppingListRecord() {
		UserHasShoppingListRecord record = new UserHasShoppingListRecord();
		record.setCanAddElement(this.getCanAddElement());
		record.setCanDeleteElement(this.getCanDeleteElement());
		record.setCanDeleteList(this.getCanDeleteList());
		record.setCanEditElement(this.getCanEditElement());
		record.setCanEditListDetail(this.getCanEditListDetail());
		return record;
	}

	public String isValid() {
		if (canEditListDetail == null && canDeleteList == null && canDeleteElement == null && canAddElement == null
				&& canEditElement == null && userEmail == null) {
			return "Given value isn't valid";
		}
		return "";
	}
}
