package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateRecord;

public class ShoppingListTemplate extends NameDescriptionView {

	protected Long id;

	public ShoppingListTemplate(String name, String description) {
		super(name, description);
	}

	public ShoppingListTemplate(ShoppingListTemplateRecord record) {
		super(record.getName(), record.getDescription());
		this.id = record.getIdshoppingListTemplate();
	}

	public String isValid() {
		return super.isValid();
	}

	public ShoppingListTemplateRecord getShoppingListTemplateRecord() {
		ShoppingListTemplateRecord recordTemplateShoppinglist = new ShoppingListTemplateRecord();
		if (this.id != null) {
			recordTemplateShoppinglist.setIdshoppingListTemplate(this.id);
		}
		recordTemplateShoppinglist.setName(this.getName());
		recordTemplateShoppinglist.setDescription(this.getDescription());
		return recordTemplateShoppinglist;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
