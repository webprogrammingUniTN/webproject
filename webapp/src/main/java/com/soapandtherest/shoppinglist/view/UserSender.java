/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;

/**
 *
 * @author mousl
 */
public class UserSender {
	private String name;

	public UserSender(String name) {
		this.name = name;
	}

	public UserSender(UserDetailRecord record) {
		this.name = record.getName();
	}

	public String getName() {
		return name;
	}
}
