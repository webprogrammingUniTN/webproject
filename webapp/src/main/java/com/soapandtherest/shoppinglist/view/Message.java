/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.record.MessageRecord;

/**
 *
 * @author mousl
 */
public class Message {

	private long message_template_idmessage;

	public Message(MessageRecord record) {
		message_template_idmessage = record.getMessageTemplateIdmessage();
	}

	/**
	 * @return the message_template_idmessage
	 */
	public long getMessage_template_idmessage() {
		return message_template_idmessage;
	}

	/**
	 * @param message_template_idmessage
	 *            the message_template_idmessage to set
	 */
	public void setMessage_template_idmessage(long message_template_idmessage) {
		this.message_template_idmessage = message_template_idmessage;
	}

}
