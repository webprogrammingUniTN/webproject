package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.util.SinglePhotoChecking;
import javax.servlet.http.HttpSession;

public abstract class NameDescriptionLogoDisableView extends NameDescriptionView {

	private com.soapandtherest.shoppinglist.view.File logo;

	private transient Image logoFile;

	private Boolean disabled;

	public NameDescriptionLogoDisableView(String name, String description, String logoID, Boolean disabled) {
		super(name, description);
		this.disabled = disabled;
		if (logoID != null) {
			this.logoFile = new Image(logoID, false);
			this.logo = new File(this.logoFile.GetPresignedURL(), this.logoFile.getObjectKey());
		}
	}

	public File getLogo() {
		return logo;
	}

	public void setLogo(File logo) {
		this.logo = logo;
	}

	public Image getLogoFile() {
		return logoFile;
	}

	public void setLogoFile(Image logoFile) {
		this.logoFile = logoFile;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	protected String isValid() {
		String validation = super.isValid();
		if (!validation.isEmpty()) {
			return validation;
		}
		if (disabled == null) {
			return "Disabled can't be null";
		}
		return "";
	}

	String isValidForAdd(HttpSession session, boolean logoCanBeNull) {
		String validation = this.isValid();
		if (!validation.isEmpty()) {
			return validation;
		}
		if (this.logo != null) {
			this.logoFile = new Image(this.logo.getObjectKey());
			String fileValidation = this.logo.isValidForAddImage(this.logoFile, session);
			if (!fileValidation.isEmpty()) {
				return fileValidation;
			}
		} else {
			if (!logoCanBeNull) {
				return "Logo can't be null";
			}
		}
		return "";
	}

	String isValidForUpdate(HttpSession session, String oldPhotoId, boolean logoCanBeNull) {
		String validation = this.isValid();
		if (!validation.isEmpty()) {
			return validation;
		}
		if (this.logo != null) {
			this.logoFile = new Image(this.logo.getObjectKey());
			SinglePhotoChecking productTemplatePhotosChecking = new SinglePhotoChecking(oldPhotoId);
			String fileValidation = this.logo.isValidWithListCheck(this.logoFile, session,
					productTemplatePhotosChecking);
			if (!fileValidation.isEmpty()) {
				return fileValidation;
			}
		} else {
			if (!logoCanBeNull) {
				return "Logo can't be null";
			}
		}

		return "";
	}

	void saveImage() {
		if (this.logoFile != null) {
			if (this.logoFile.isUploadFile()) {
				this.logoFile.CompleteUpload();
			}
		}
	}
}
