package com.soapandtherest.shoppinglist.view.request;

public class FileRequest {
	private int byteContentSize;

	public FileRequest(int byteContentSize) {
		this.byteContentSize = byteContentSize;
	}

	public int getByteContentSize() {
		return byteContentSize;
	}

	public void setByteContentSize(int byteContentSize) {
		this.byteContentSize = byteContentSize;
	}
}
