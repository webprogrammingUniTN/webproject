package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.persistence.ProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import javax.servlet.http.HttpSession;

public class CategoryComplete extends NameDescriptionLogoDisableView {

	private Long id;

	public CategoryComplete(ProductCategoriesRecord record) {
		super(record.getName(), record.getDescription(), record.getLogo(), record.getDisabled());
		this.id = record.getIdproductCategories();
	}

	protected String isValid() {
		return super.isValid();
	}

	private ProductCategoriesRecord getProductCategoriesRecord() {
		ProductCategoriesRecord record = new ProductCategoriesRecord();
		record.setIdproductCategories(this.id);
		record.setName(this.getName());
		record.setDescription(this.getDescription());
		if (this.getLogo() != null) {
			record.setLogo(this.getLogo().getObjectKey());
		} else {
			record.setLogo(null);
		}
		record.setDisabled(this.getDisabled());
		return record;
	}

	public Long getId() {
		return id;
	}

	private void setId(Long idproductCategories) {
		this.id = idproductCategories;
	}

	public String isValidForAdd(HttpSession session) {
		id = null;
		return super.isValidForAdd(session, true);
	}

	public String isValidForUpdate(HttpSession session, ProductCategoriesRecord oldCategoryRecord) {
		return super.isValidForUpdate(session, oldCategoryRecord.getLogo(), true);
	}

	public void save(ProductCategoriesPersistence persistence) {
		super.saveImage();
		ProductCategoriesRecord record = this.getProductCategoriesRecord();
		if (this.getId() == null) {
			record = persistence.create(record);
			this.setId(record.getIdproductCategories());
		} else {
			persistence.update(record);
		}
	}
}
