package com.soapandtherest.shoppinglist.view.response;

import java.util.List;

public class PartialElementList<T> {
	private long numberOfTotalElement;
	private List<T> elements;

	public PartialElementList(long numberOfTotalElement, List<T> templates) {
		setElements(templates);
		setNumberOfTotalElement(numberOfTotalElement);
	}

	public long getNumberOfTotalElement() {
		return numberOfTotalElement;
	}

	public void setNumberOfTotalElement(long numberOfTotalElement) {
		this.numberOfTotalElement = numberOfTotalElement;
	}

	public List<T> getElements() {
		return elements;
	}

	public void setElements(List<T> templates) {
		this.elements = templates;
	}
}
