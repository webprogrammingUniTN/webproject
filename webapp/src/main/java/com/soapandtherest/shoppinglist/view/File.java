package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasFileRecord;
import com.soapandtherest.shoppinglist.util.ImageChecking;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpSession;

public class File {
	private URL url;

	private String objectKey;

	public File(URL url, String objectKey) {
		this.url = url;
		this.objectKey = objectKey;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getObjectKey() {
		return objectKey;
	}

	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}

	@Override
	public int hashCode() {
		return this.objectKey.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		File file = (File) o;
		return Objects.equals(objectKey, file.objectKey);
	}

	public String isValidForAddImage(Image image, HttpSession session) {
		if (!image.isUploadFile()) {
			return "A specific file isn't a valid file";
		} else {
			if (!com.soapandtherest.shoppinglist.controllers.File.checkFileExist(session, image.getObjectKey())) {
				return "A specific file isn't a valid file";
			}
		}
		return "";
	}

	public String isValidWithListCheck(Image image, HttpSession session, ImageChecking imageChecking) {
		if (!image.isUploadFile()) {
			boolean exist = imageChecking.checkImageExistIntoList(image);
			if (!exist) {
				return "You haven't necessary authorization for this file";
			}
		} else {
			if (!com.soapandtherest.shoppinglist.controllers.File.checkFileExist(session, image.getObjectKey())) {
				return "A specific file isn't a valid file";
			}
		}
		return "";
	}
}
