package com.soapandtherest.shoppinglist.view.request;

import static com.soapandtherest.shoppinglist.view.request.AuthenticationUtil.validateEmail;
import static com.soapandtherest.shoppinglist.view.request.AuthenticationUtil.validatePassword;

import com.soapandtherest.shoppinglist.view.request.AuthenticationUtil.PasswordCheckResult;

public class SignOn {

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public SignOn(String name, String surname, String email, String password, String password2) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.password = password;
		this.password2 = password2;
	}

	public String isValid() {
		if ((name == null) || (name.length() < 3)) {
			return "Nome: deve avere almeno 3 caratteri.";
		}
		if ((surname == null) || (surname.length() < 3)) {
			return "Cognome: deve avere almeno 3 caratteri.";
		}
		if (!validateEmail(email)) {
			return "Indirizzo Email non valido.";
		}
		if ((password == null) || (password.length() < 8)) {
			return "Password: deve avere almeno 8 caratteri.";
		}
		if ((password2 == null) || (password2.length() < 8)) {
			return "Password di conferma: deve avere almeno 8 caratteri.";
		}
		if (!password.equals(password2)) {
			return "Password di conferma: differisce dalla password.";
		}
		PasswordCheckResult EsitoPwd = validatePassword(password);
		if (EsitoPwd == PasswordCheckResult.PWD_WITH_SPACES) {
			return "Password: non deve contenere spazi.";
		}
		if (EsitoPwd == PasswordCheckResult.PWD_CHARSET) {
			return "Password: deve contenere almeno tre dei seguenti set: numeri, minuscola, maiuscole, caratteri speciali.";
		}
		return "";
	}

	private String name;
	private String surname;
	private String email;
	private String password;
	private String password2;
}
