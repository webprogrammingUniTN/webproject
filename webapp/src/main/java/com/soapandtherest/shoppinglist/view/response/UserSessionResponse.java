package com.soapandtherest.shoppinglist.view.response;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;

public class UserSessionResponse extends StandardResponse {

	public static final String ANONYMOUS_NAME = "anonimo";

	private String name;
	private String surname;
	private String email;
	private String remember;
	private String admin;
	private com.soapandtherest.shoppinglist.view.File[] avatar;
	private transient Image image;

	public UserSessionResponse(int code, String message) {
		super(code, message);
	}

	public UserSessionResponse(String name) {
		super(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
		this.name = name;
	}

	public UserSessionResponse() {
		super(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
		this.name = ANONYMOUS_NAME;
		this.surname = "";
		this.email = "";
		this.remember = "N";
		this.admin = "N";
		this.image = null;
		this.avatar = null;
	}

	public UserSessionResponse(UserDetailRecord detail, String rememberMe) {
		super(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
		name = detail.getName();
		surname = detail.getSurname();
		email = detail.getEmail();

		try {
			image = new Image(detail.getAvatarImage());
			avatar = new com.soapandtherest.shoppinglist.view.File[1];
			avatar[0] = new com.soapandtherest.shoppinglist.view.File(image.GetPresignedURL(Image.ImageSize.SMALL),
					detail.getAvatarImage());
		} catch (Exception e) {
			image = null;
			avatar = null;
		}

		remember = rememberMe;
		if (detail.getAdmin().equals(Boolean.TRUE))
			admin = "Y";
		else
			admin = "N";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public com.soapandtherest.shoppinglist.view.File getAvatar() {
		return avatar[0];
	}

	public void setAvatar(com.soapandtherest.shoppinglist.view.File avatar) {
		this.avatar[0] = avatar;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getRemember() {
		return remember;
	}

	public void setRemember(String remember) {
		this.remember = remember;
	}

	public String getAdmin() {
		return admin;
	}

	public void setAdmin(String admin) {
		this.admin = admin;
	}

}
