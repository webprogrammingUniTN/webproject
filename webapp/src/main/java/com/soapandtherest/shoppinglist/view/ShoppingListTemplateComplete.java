package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.file.Image.ImageSize;
import com.soapandtherest.shoppinglist.data.persistence.PlaceTypePersistence;
import com.soapandtherest.shoppinglist.data.persistence.ProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplateHasFilePersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplateHasProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplatePersistence;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasFileRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasProductCategoriesRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateRecord;
import com.soapandtherest.shoppinglist.util.ImageChecking;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ShoppingListTemplateComplete extends ShoppingListTemplate {
	private static final Logger logger = LogManager.getLogger(ShoppingListTemplateComplete.class.getName());

	protected List<com.soapandtherest.shoppinglist.view.File> images;

	protected List<Category> category;

	protected boolean disabled;

	protected String geolocationIdentifier;

	private transient boolean imagesIsUrl = false;

	private transient List<Image> files;
	private transient List<ShoppingListTemplateHasFileRecord> fileToRemove;

	public ShoppingListTemplateComplete(String name, String description,
			List<com.soapandtherest.shoppinglist.view.File> images, List<Category> category, boolean disabled,
			String geolocationIdentifier) {
		super(name, description);
		this.images = images;
		this.category = category;
		this.disabled = disabled;
		this.geolocationIdentifier = geolocationIdentifier;
	}

	public ShoppingListTemplateComplete(ShoppingListTemplateRecord record,
			List<ShoppingListTemplateHasFileRecord> images, List<ProductCategoriesRecord> categories) {
		super(record);
		this.disabled = record.getDisabled();
		this.images = new LinkedList<>();
		this.files = new LinkedList<>();
		this.geolocationIdentifier = record.getPlaceIdentifier();
		// TODO: recover current user level
		for (ShoppingListTemplateHasFileRecord image : images) {
			this.images.add(new com.soapandtherest.shoppinglist.view.File(null, image.getKey()));
			Image imageFiles = new Image(image.getKey(), false);
			this.getFiles().add(imageFiles);
		}
		this.category = new LinkedList<>();
		for (ProductCategoriesRecord category : categories) {
			this.category.add(new Category(category));
		}
	}

	@Override
	public String isValid() {
		return this.isValidForAdd(null, null, null);
	}

	public String isValidForAdd(HttpSession session, ProductCategoriesPersistence persistence,
			PlaceTypePersistence placeTypePersistence) {
		String result = super.isValid();
		if (!result.isEmpty()) {
			return result;
		}
		this.id = null;
		if (getFiles() == null) {
			this.files = new LinkedList<>();
		}
		if (this.images == null) {
			return "Images can't be null";
		}
		if (getCategory() == null) {
			return "Category can't be null";
		}
		String validCategory = testCategory(persistence);
		if (!validCategory.isEmpty()) {
			return validCategory;
		}
		for (com.soapandtherest.shoppinglist.view.File image : this.images) {
			Image imageFile = new Image(image.getObjectKey());
			String imageValidationResult = image.isValidForAddImage(imageFile, session);
			if (!imageValidationResult.isEmpty()) {
				return imageValidationResult;
			}
			this.getFiles().add(imageFile);
		}
		if (!placeTypePersistence.exists(this.geolocationIdentifier)) {
			return "Invalid place identifier";
		}
		return "";
	}

	public String isValidForUpdate(ShoppingListTemplatePersistence persistence,
			ShoppingListTemplateHasFilePersistence persistenceFile, HttpSession session,
			ProductCategoriesPersistence persistenceCategory, PlaceTypePersistence placeTypePersistence) {
		String result = super.isValid();
		if (!result.isEmpty()) {
			return result;
		}
		if (getFiles() == null) {
			this.files = new LinkedList<>();
		}
		if (!persistence.exists(this.id)) {
			return "Given id not exist";
		}
		List<ShoppingListTemplateHasFileRecord> yetExistFile = persistenceFile.findByTemplateId(this.id);
		ShoppingListTemplatePhotosChecking photosChecking = new ShoppingListTemplatePhotosChecking(yetExistFile);
		for (com.soapandtherest.shoppinglist.view.File image : this.images) {
			Image imageFiles = new Image(image.getObjectKey());
			String resultImageValidation = image.isValidWithListCheck(imageFiles, session, photosChecking);
			if (!resultImageValidation.isEmpty()) {
				return resultImageValidation;
			}
			this.getFiles().add(imageFiles);
		}
		this.fileToRemove = yetExistFile;
		String validCategory = testCategory(persistenceCategory);
		if (!validCategory.isEmpty()) {
			return validCategory;
		}
		if (!placeTypePersistence.exists(this.geolocationIdentifier)) {
			return "Invalid place identifier";
		}
		return "";
	}

	private String testCategory(ProductCategoriesPersistence persistence) {
		for (Category category : getCategory()) {
			boolean result = category.existOnDb(persistence);
			if (result) {
				return "Category isn't valid";
			}
		}
		return "";
	}

	public void generatePreSignedURLs(ImageSize size) {
		if (imagesIsUrl) {
			return;
		}
		if (this.getFiles() == null) {
			this.isValid();
		}
		for (int i = 0; i < this.files.size(); i++) {
			try {
				this.images.get(i).setUrl(new URL(this.files.get(i).GetPresignedURL(size).toString()));
			} catch (MalformedURLException e) {
				logger.error("Unexpected malformed url exception, ", e.getMessage());
			}
		}
		this.imagesIsUrl = true;
	}

	public void save(ShoppingListTemplatePersistence persistence,
			ShoppingListTemplateHasFilePersistence persistenceFile,
			ShoppingListTemplateHasProductCategoriesPersistence categoryPersistence) {
		ShoppingListTemplateRecord record = this.getShoppingListTemplateRecord();
		record = persistence.save(record);
		this.id = record.getIdshoppingListTemplate();
		if (this.getFiles() == null) {
			this.isValid();
		}
		Set<com.soapandtherest.shoppinglist.view.File> newImagesKey = new HashSet<>();
		for (Image image : getFiles()) {
			if (image.isUploadFile()) {
				image.CompleteUpload();
				newImagesKey.add(new com.soapandtherest.shoppinglist.view.File(null, image.getObjectKey()));
				ShoppingListTemplateHasFileRecord fileRecord = new ShoppingListTemplateHasFileRecord();
				fileRecord.setIdshoppingListTemplate(record.getIdshoppingListTemplate());
				fileRecord.setKey(image.getObjectKey());
				persistenceFile.save(fileRecord);
			} else {
				newImagesKey.add(new com.soapandtherest.shoppinglist.view.File(null, image.getObjectKey()));
			}
		}
		this.images = new ArrayList<>(newImagesKey);
		if (fileToRemove != null) {
			for (ShoppingListTemplateHasFileRecord fileRecord : fileToRemove) {
				persistenceFile.delete(fileRecord);
			}
		}
		categoryPersistence.deleteAllByShoppingListId(record.getIdshoppingListTemplate());
		for (Category cat : this.getCategory()) {
			ShoppingListTemplateHasProductCategoriesRecord bindingCategoryRecord = cat
					.getShoppingListTemplateHasProductCategoriesRecord(record.getIdshoppingListTemplate());
			categoryPersistence.save(bindingCategoryRecord);
		}
	}

	public List<ShoppingListTemplateHasFileRecord> getListShoppingListTemplateHasFileRecord(long shoppingListID) {
		if (this.getFiles() == null) {
			this.isValid();
		}
		List<ShoppingListTemplateHasFileRecord> records = new LinkedList<>();
		for (Image image : this.getFiles()) {
			ShoppingListTemplateHasFileRecord record = new ShoppingListTemplateHasFileRecord();
			record.setIdshoppingListTemplate(shoppingListID);
			record.setKey(image.getObjectKey());
			records.add(record);
		}
		return records;
	}

	public List<Image> getFiles() {
		return files;
	}

	public List<ShoppingListTemplateHasFileRecord> getFileToRemove() {
		return fileToRemove;
	}

	@Override
	public ShoppingListTemplateRecord getShoppingListTemplateRecord() {
		ShoppingListTemplateRecord record = super.getShoppingListTemplateRecord();
		record.setDisabled(this.disabled);
		record.setPlaceIdentifier(this.geolocationIdentifier);
		return record;
	}

	private class ShoppingListTemplatePhotosChecking implements ImageChecking {
		private List<ShoppingListTemplateHasFileRecord> yetExistImage;
		ShoppingListTemplatePhotosChecking(List<ShoppingListTemplateHasFileRecord> yetExistImage) {
			this.yetExistImage = yetExistImage;
		}

		@Override
		public boolean checkImageExistIntoList(Image image) {
			for (ShoppingListTemplateHasFileRecord file : yetExistImage) {
				if (image.getObjectKey().equals(file.getKey())) {
					yetExistImage.remove(file);
					return true;
				}
			}
			return false;
		}
	}

	public List<Category> getCategory() {
		return category;
	}

	public void setCategory(List<Category> category) {
		this.category = category;
	}
}
