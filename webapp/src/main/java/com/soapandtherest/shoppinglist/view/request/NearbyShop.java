package com.soapandtherest.shoppinglist.view.request;

public class NearbyShop {
	private double lat;
	private double lon;

	public NearbyShop(double lat, double lon) {
		this.lat = lat;
		this.lon = lon;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double distance(NearbyShop point) {
		double R = 6371e3; // metres
		double Phi1 = Math.toRadians(this.lat);
		double Phi2 = Math.toRadians(point.lat);
		double DeltaPhi = Math.toRadians(point.lat - this.lat);
		double DeltaLambda = Math.toRadians(point.lon - this.lon);

		double a = Math.sin(DeltaPhi / 2) * Math.sin(DeltaPhi / 2)
				+ Math.cos(Phi1) * Math.cos(Phi2) * Math.sin(DeltaLambda / 2) * Math.sin(DeltaLambda / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

		return R * c;
	}

	public String isValid() {
		if (this.lat > 90 || this.lat < -90 || this.lon > 180 || this.lon < -180) {
			return "given lan or lon aren't valid";
		}
		return "";
	}
}
