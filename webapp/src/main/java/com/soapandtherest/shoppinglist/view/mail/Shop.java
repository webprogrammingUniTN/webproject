package com.soapandtherest.shoppinglist.view.mail;

public class Shop {

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMapsurl() {
		return mapsurl;
	}

	public void setMapsurl(String mapsurl) {
		this.mapsurl = mapsurl;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	private String name;
	private String type;
	private String mapsurl;
	private String image;

	public Shop(String name, String type, String mapsurl, String image) {
		this.name = name;
		this.type = type;
		this.mapsurl = mapsurl;
		this.image = image;
	}
}
