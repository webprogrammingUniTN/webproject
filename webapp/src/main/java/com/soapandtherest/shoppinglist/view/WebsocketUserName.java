/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;

/**
 *
 * @author mousl
 */
public class WebsocketUserName {
	private Long userId;
	private String userName;

	public WebsocketUserName(Long userId) {
		this.userName = getUserName(userId);
	}

	public String getUserName(Long userId) {
		return new UserDetailPersistenceJdbc().findById(userId).getName();
	}

	public String getUserName() {
		return userName;
	}

}
