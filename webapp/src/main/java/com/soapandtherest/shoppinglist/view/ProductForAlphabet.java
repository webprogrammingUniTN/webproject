/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.record.ProductRecord;

/**
 *
 * @author mousl
 */
public class ProductForAlphabet extends Product {

	public ProductForAlphabet(ProductRecord record) {
		super(record);
	}

	@Override
	public int compareTo(Product o) {
		return name.compareTo(o.getName());
	}

}
