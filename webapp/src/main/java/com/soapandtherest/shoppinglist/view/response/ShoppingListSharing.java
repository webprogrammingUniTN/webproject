package com.soapandtherest.shoppinglist.view.response;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.util.user.UserInterface;
import com.soapandtherest.shoppinglist.util.user.UserInterfaceFactory;
import com.soapandtherest.shoppinglist.view.request.EditShoppingListSharing;

public class ShoppingListSharing extends EditShoppingListSharing {

	private static final UserInterface userUtil = UserInterfaceFactory.BuildUserInterface();

	private transient Long UserId;
	private String userImage;
	private transient Image userImageReference;
	private Long shoppingListIdshoppingList;
	private Boolean listOwner;

	public ShoppingListSharing(UserHasShoppingListRecord record) {
		super(record, "");
		UserDetailRecord userDetail = userUtil.findUserById(record.getUserIduser());
		this.setUserEmail(userDetail.getEmail());
		this.setShoppingListIdshoppingList(record.getShoppingListIdshoppingList());
		this.setListOwner(record.getListOwner());
		this.setUserImageReference(new Image(userDetail.getAvatarImage(), false));
		this.setUserImage(getUserImageUrl());
	}

	public ShoppingListSharing(EditShoppingListSharing element, Long shoppingListIdshoppingList) {
		super(element);
		this.listOwner = false;
		this.shoppingListIdshoppingList = shoppingListIdshoppingList;
	}

	public String getUserImage() {
		return userImage;
	}

	public String getUserImageUrl() {
		if (this.userImageReference == null) {
			return "";
		}
		return userImageReference
				.GetPresignedURL(com.soapandtherest.shoppinglist.controllers.ShoppingListSharing.AVATAR_IMAGE_SIZE)
				.toString();
	}

	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}

	public Image getUserImageReference() {
		return userImageReference;
	}

	public void setUserImageReference(Image userImageReference) {
		this.userImageReference = userImageReference;
	}

	public Long getShoppingListIdshoppingList() {
		return shoppingListIdshoppingList;
	}

	public void setShoppingListIdshoppingList(Long shoppingListIdshoppingList) {
		this.shoppingListIdshoppingList = shoppingListIdshoppingList;
	}

	public Boolean getListOwner() {
		return listOwner;
	}

	public void setListOwner(Boolean listOwner) {
		this.listOwner = listOwner;
	}

	public Long getUserId() {
		return UserId;
	}

	public void setUserId(Long userId) {
		UserId = userId;
	}

	@Override
	public UserHasShoppingListRecord getUserHasShoppingListRecord() {
		UserHasShoppingListRecord record = super.getUserHasShoppingListRecord();
		record.setShoppingListIdshoppingList(this.getShoppingListIdshoppingList());
		record.setUserIduser(this.getUserId());
		record.setListOwner(this.getListOwner());
		return record;
	}

	@Override
	public String isValid() {
		String valid = super.isValid();
		if (!valid.isEmpty()) {
			return valid;
		}
		UserDetailRecord userToShare = userUtil.getUserByEmail(this.getUserEmail());
		if (userToShare == null) {
			return "User that you have insert isn't present on our system";
		}
		this.userImageReference = new Image(userToShare.getAvatarImage(), false);
		this.userImage = this.getUserImageUrl();
		this.setUserId(userToShare.getUserIduser());
		return "";
	}
}
