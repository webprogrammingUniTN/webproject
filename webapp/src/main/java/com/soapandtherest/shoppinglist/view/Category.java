package com.soapandtherest.shoppinglist.view;

import com.soapandtherest.shoppinglist.data.persistence.ProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasProductCategoriesRecord;

public class Category extends NameDescriptionView {

	private Long id;

	public Category(ProductCategoriesRecord record) {
		super(record.getName(), record.getDescription());
		this.id = record.getIdproductCategories();
	}

	protected String isValid() {
		return super.isValid();
	}

	public Long getId() {
		return id;
	}

	private void setId(Long idproductCategories) {
		this.id = idproductCategories;
	}

	public boolean existOnDb(ProductCategoriesPersistence persistence) {
		return persistence.exists(this.id);
	}

	public ShoppingListTemplateHasProductCategoriesRecord getShoppingListTemplateHasProductCategoriesRecord(
			Long idShoppingList) {
		ShoppingListTemplateHasProductCategoriesRecord record = new ShoppingListTemplateHasProductCategoriesRecord();
		record.setProductCategoriesIdproductCategories(id);
		record.setShoppingListTemplateIdshoppingListTemplate(idShoppingList);
		return record;
	}
}
