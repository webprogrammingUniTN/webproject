/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserComputerPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserComputerRecord;

/**
 *
 * @author Gionni_2
 */
public class UserComputer {

	// USER_COMPUTER
	public static final boolean REMEMBER = true;
	public static final boolean NOT_REMEMBER = false;

	public static void set(String idComputer, Long idUser, Boolean remember) {
		UserComputerPersistenceJdbc userComputerProvider = new UserComputerPersistenceJdbc();
		UserComputerRecord userComputer = new UserComputerRecord();
		userComputer.setIdComputer(idComputer);
		userComputer.setUserIduser(idUser);
		userComputer.setRemember(remember);
		userComputerProvider.save(userComputer);
	}

	public static void clearRememberMe(String idComputer, Long idUser) {
		set(idComputer, idUser, NOT_REMEMBER);
	}

	public static boolean getSet(String idComputer, Long idUser, Boolean remember) {
		UserComputerPersistenceJdbc userComputerProvider = new UserComputerPersistenceJdbc();
		UserComputerRecord userComputer = new UserComputerRecord();
		userComputer.setIdComputer(idComputer);
		userComputer.setUserIduser(idUser);
		if (!userComputerProvider.load(userComputer) || (userComputer.getUserIduser() == null))
			return false;
		if ((userComputer.getRemember() == null) || (!userComputer.getRemember().equals(remember))) {
			userComputer.setRemember(remember);
			userComputerProvider.save(userComputer);
		}
		return true;
	}

	public static Boolean getRemember(String idComputer, Long idUser) {
		UserComputerPersistenceJdbc userComputerProvider = new UserComputerPersistenceJdbc();
		UserComputerRecord userComputer = new UserComputerRecord();
		userComputer.setIdComputer(idComputer);
		userComputer.setUserIduser(idUser);
		if (!userComputerProvider.load(userComputer))
			return null;
		if ((userComputer.getUserIduser() != null) && (userComputer.getRemember() == null)) {
			// Serve nel caso il record ci sia ma abbia il campo "remember" a null
			userComputer.setRemember(NOT_REMEMBER);
			userComputerProvider.save(userComputer);
		}
		return userComputer.getRemember();
	}

}
