package com.soapandtherest.shoppinglist.util.sharing;

import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserRecord;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

class SharingUtilFake implements SharingInterface {
	@Override
	public List<UserHasShoppingListRecord> userHasShoppingList(Long userId) {
		List<UserHasShoppingListRecord> records = new LinkedList<>();
		UserHasShoppingListRecord record = getUserHasShoppingListRecord();
		records.add(record);
		return records;
	}

	@Override
	public List<Long> userHasShoppingListId(Long userId) {
		List<Long> idCollections = new ArrayList<>();
		idCollections.add(1L);
		return idCollections;
	}

	@Override
	public void addOwnerAuth(UserRecord user, ShoppingListRecord record) {
	}

	@Override
	public UserHasShoppingListRecord userHasShoppingList(Long userId, Long shoppingListId) {
		return getUserHasShoppingListRecord();
	}

	private UserHasShoppingListRecord getUserHasShoppingListRecord() {
		UserHasShoppingListRecord record = new UserHasShoppingListRecord();
		record.setCanDeleteElement(true);
		record.setCanEditElement(true);
		record.setCanAddElement(true);
		record.setCanEditListDetail(true);
		record.setCanDeleteList(true);
		record.setCreateTime(Calendar.getInstance().getTime());
		record.setUpdateTime(Calendar.getInstance().getTime());
		record.setListOwner(false);
		record.setShoppingListIdshoppingList(1L);
		record.setUserIduser(1L);
		return record;
	}
}
