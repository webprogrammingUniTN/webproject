/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

/**
 *
 * @author Gionni_2
 */
import com.google.common.io.BaseEncoding;
import java.nio.charset.Charset;
import javax.crypto.SecretKeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.spec.PBEKeySpec;
import java.security.spec.KeySpec;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.concurrent.ThreadLocalRandom;

public class PasswordHashing {
	private final double HASHING_TIME_MULTIPLIER = 1.303257137537003d;
	private final int KEY_LENGTH = 256;
	private final Charset UTF8_CHARSET = Charset.forName("UTF-8");

	/*
	 * HashPassword convert the plain password in the hash store in the database
	 */
	public String hashPassword(String salt, int hashingTime, String clearPassword)
			throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException {
		// Compute the hash of the provided password, using the same salt,
		// iteration count, and hash length
		KeySpec spec = new PBEKeySpec(clearPassword.toCharArray(), salt.getBytes("UTF-8"),
				(int) (hashingTime * HASHING_TIME_MULTIPLIER), KEY_LENGTH);
		SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		// This need to be convert to hex you can use guava library (yet include inside
		// pom) https://plus.google.com/+googleguava/posts/99kVcKG5bgm
		return Base64.getEncoder().encodeToString(f.generateSecret(spec).getEncoded());
	}

	public int generateRandomInt(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	public char getBase62(int num) {
		if (num < 10)
			return (char) (num + 48);
		if (num < 36)
			return (char) (num - 10 + 65);
		return (char) (num - 36 + 97);
	}

	public String saltGenerate(int saltLength) {
		String salt = "";
		for (int i = 0; i < saltLength; i++) {
			salt += getBase62(generateRandomInt(0, 61));
		}
		return salt;
	}

}
