/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gionni_2
 */
public class InitSession {

	public static void setSession(HttpServletRequest request) {

		String cookieUUID = UserCookie.getUUID(request);
		Long idAnonymousUser = UserCookie.getAnonymousIdUser(request);
		Long idRegisteredUser = UserCookie.getRegistredIdUser(request);

		// Se UUID e idAnonymousUser non sono entrambi valorizzati, allora
		// non è possibile il SignOn automatico.
		// In tal caso creo un nuovo utente anonimo e valorizzo i cookies
		// Normalmente questo avviene al primo accesso dal browser
		if ((cookieUUID == null) || (idAnonymousUser == null)) {
			User.newAnonymous(request);
			return;
		}

		// Se l'utente non è registrato allora verifico se creare un nuovo
		// utente anonimo e comunque restituisco una sessione anonima
		if (idRegisteredUser == null) {
			User.newAnonymous(request, idAnonymousUser, cookieUUID);
			return;
		}

		// Se UUID e idRegisteredUser sono valorizzati, allora tento il SignOn
		// automatico
		// cercando se l'utente ha selezionato il "remember"
		// Se l'utente non esiste, creo un nuovo utente anonimo
		Boolean remember = UserComputer.getRemember(cookieUUID, idRegisteredUser);
		if (remember == null) {
			User.newAnonymous(request, idAnonymousUser, cookieUUID);
			return;
		} else if (remember.equals(UserComputer.NOT_REMEMBER)) {
			UserSession.reset(request, idAnonymousUser);
			return;
		}

		// L'utente esiste e aveva selezionato "remember": recupero i dati di dettaglio
		// usando l'idUser...
		Boolean anonymous = User.getAnonymous(idRegisteredUser);
		if (anonymous) {
			UserSession.reset(request, idAnonymousUser);
			return;
		}

		UserDetailPersistenceJdbc detailProvider = new UserDetailPersistenceJdbc();
		UserDetailRecord detail = new UserDetailRecord();
		detail.setUserIduser(idRegisteredUser);
		if (!detailProvider.load(detail) || (detail.getUserIduser() == null))
			UserSession.reset(request, idAnonymousUser);
		else
			UserSession.set(request, idRegisteredUser, anonymous, remember, detail);
	}

	public static void checkCookies(HttpServletRequest request, HttpServletResponse response) {

		// Controllo l'esistenza del cookie UUID
		// Se non esiste: lo genero ex-novo
		String cookieUUID = UserCookie.getSetUUID(request, response);

		Long idAnonymousUser = UserCookie.getAnonymousIdUser(request);
		Long idRegisteredUser = UserCookie.getRegistredIdUser(request);

		if (UserSession.isAnonymous(request)) {
			// Se idAnonymousUser non è valorizzato, allora
			// non è possibile il SignOn automatico.
			// In tal caso creo (se serve) un nuovo utente anonimo e valorizzo i cookies
			// Normalmente questo avviene al primo accesso dal browser
			if (idAnonymousUser == null) {
				idAnonymousUser = UserSession.getIdUser(request);
				if (idAnonymousUser == null)
					User.newAnonymous(request, response);
				else {
					UserCookie.setAnonymousIdUser(request, response, idAnonymousUser);
					UserSession.reset(request, idAnonymousUser);
				}
			}
			if (idRegisteredUser != null)
				UserCookie.clearRegisteredIdUser(request, response);
			return;
		}

		// Se l'utente non è registrato allora verifico se creare un nuovo
		// utente anonimo e comunque restituisco una sessione anonima
		if (idRegisteredUser == null) {
			idRegisteredUser = UserSession.getIdUser(request);
			if (idRegisteredUser == null) {
				User.newAnonymous(request, response);
				return;
			}
			UserCookie.setRegisteredIdUser(request, response, idRegisteredUser);
		}

		// Se UUID e idRegisteredUser sono valorizzati, allora tento il SignOn
		// automatico
		// cercando se l'utente ha selezionato il "remember"
		// Se l'utente non esiste, creo un nuovo utente anonimo e valorizzo i cookies
		Boolean remember = UserComputer.getRemember(cookieUUID, idRegisteredUser);
		if (remember == null) {
			User.newAnonymous(request, response, idAnonymousUser, cookieUUID);
			return;
		} else if (remember.equals(UserComputer.NOT_REMEMBER)) {
			UserSession.reset(request, idAnonymousUser);
			return;
		}

		// L'utente esiste e aveva selezionato "remember": recupero i dati di dettaglio
		// usando l'idUser...
		Boolean anonymous = User.getAnonymous(idRegisteredUser);
		if (anonymous) {
			UserSession.reset(request, idAnonymousUser);
			return;
		}

		UserDetailPersistenceJdbc detailProvider = new UserDetailPersistenceJdbc();
		UserDetailRecord detail = new UserDetailRecord();
		detail.setUserIduser(idRegisteredUser);
		if (!detailProvider.load(detail) || (detail.getUserIduser() == null))
			UserSession.reset(request, idAnonymousUser);
		else
			UserSession.set(request, idRegisteredUser, anonymous, remember, detail);
	}

}
