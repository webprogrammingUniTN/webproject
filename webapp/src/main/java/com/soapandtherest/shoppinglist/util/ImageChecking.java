package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.file.Image;

public interface ImageChecking {
	boolean checkImageExistIntoList(Image image);
}
