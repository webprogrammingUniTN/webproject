/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import static java.lang.Long.parseLong;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Gionni_2
 */
public class UserSession {

	public static void setRedLight(HttpServletRequest request) {
		request.getSession().setAttribute("greenLight", "N");
	}

	public static boolean isGreenLight(HttpServletRequest request) {
		String gl = get(request, "greenLight");
		if (gl == null)
			return false;
		return gl.equals("Y");
	}

	public static void set(HttpServletRequest request, Long idUser, Boolean anonymous, Boolean rememberMe,
			UserDetailRecord userDetail) {
		HttpSession session = request.getSession();
		session.setAttribute("iduser", idUser);
		session.setAttribute("anonymous", anonymous ? "Y" : "N");
		session.setAttribute("remember", rememberMe ? "Y" : "N");
		session.setAttribute("name", userDetail.getName());
		session.setAttribute("surname", userDetail.getSurname());
		session.setAttribute("email", userDetail.getEmail());
		session.setAttribute("admin", userDetail.getAdmin().toString());
		if (userDetail.getAdmin())
			session.setAttribute("user_role", AuthRoles.ADMINISTRATOR);
		else
			session.setAttribute("user_role", AuthRoles.STANDARD);
		session.setAttribute("greenLight", "Y");
	}

	public static void reset(HttpServletRequest request, Long idAnonymousUser) {
		HttpSession session = request.getSession();
		session.setAttribute("iduser", idAnonymousUser);
		session.setAttribute("anonymous", "Y");
		session.setAttribute("remember", "N");
		session.setAttribute("name", "anonimo");
		session.setAttribute("surname", "");
		session.setAttribute("email", "");
		session.setAttribute("admin", "N");
		session.setAttribute("user_role", AuthRoles.ANONYMOUS);
		session.setAttribute("greenLight", "Y");
	}

	public static boolean isAnonymous(HttpServletRequest request) {
		String anonymous = get(request, "anonymous");
		return ((anonymous != null) && anonymous.equals("Y"));
	}

	public static String get(HttpServletRequest request, String key) {
		HttpSession session = request.getSession();
		Object value = session.getAttribute(key);
		if (value == null)
			return null;
		return value.toString();
	}

	public static Long getLong(HttpServletRequest request, String key) {
		String value = get(request, key);
		if (value == null)
			return null;
		try {
			return parseLong(value);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public static void set(HttpServletRequest request, String key, String value) {
		request.getSession().setAttribute(key, value);
	}

	public static void set(HttpServletRequest request, String key, Long value) {
		request.getSession().setAttribute(key, value.toString());
	}

	public static Long getIdUser(HttpServletRequest request) {
		Long id = getLong(request, "iduser");
		int cycles = 0;
		while ((!isGreenLight(request) || (id == null)) && (cycles < 20)) {
			cycles++;
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			id = getLong(request, "iduser");
		}
		return id;
	}

	public static void setCurrentList(HttpServletRequest request, Long idShoppingList) {
		set(request, "currentListId", idShoppingList);
	}

	public static Long getCurrentList(HttpServletRequest request) {
		return getLong(request, "currentListId");
	}
}
