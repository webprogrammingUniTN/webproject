/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author vbeatrice
 */
public class ListChat {

	public static Long[] getShoppingListID() {
		ShoppingListPersistence persistenceService = new ShoppingListPersistenceJdbc();

		List<ShoppingListRecord> listRecord = persistenceService.findAll();
		List<Long> IDlist = new LinkedList<>();
		for (ShoppingListRecord record : listRecord) {
			IDlist.add(record.getIdshoppingList());
		}

		Long[] IDlistArray = new Long[IDlist.size()];
		return IDlist.toArray(IDlistArray);
	}

}
