package com.soapandtherest.shoppinglist.util.sharing;

import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserRecord;
import java.util.ArrayList;
import java.util.List;

class SharingUtilJdbc implements SharingInterface {
	private static UserHasShoppingListPersistence persistenceService = new UserHasShoppingListPersistenceJdbc();
	@Override
	public List<UserHasShoppingListRecord> userHasShoppingList(Long userId) {
		return persistenceService.findByUserId(userId);
	}

	@Override
	public List<Long> userHasShoppingListId(Long userId) {
		List<UserHasShoppingListRecord> userHasShoppingListRecords = this.userHasShoppingList(userId);
		List<Long> resultId = new ArrayList<>(userHasShoppingListRecords.size());
		for (UserHasShoppingListRecord userHasShoppingList : userHasShoppingListRecords) {
			resultId.add(userHasShoppingList.getShoppingListIdshoppingList());
		}
		return resultId;
	}

	@Override
	public void addOwnerAuth(UserRecord user, ShoppingListRecord shoppingList) {
		UserHasShoppingListRecord record = new UserHasShoppingListRecord();
		record.setCanDeleteList(true);
		record.setCanEditListDetail(true);
		record.setCanAddElement(true);
		record.setCanEditElement(true);
		record.setCanDeleteElement(true);
		record.setListOwner(true);
		record.setShoppingListIdshoppingList(shoppingList.getIdshoppingList());
		record.setUserIduser(user.getIduser());
	}

	@Override
	public UserHasShoppingListRecord userHasShoppingList(Long userId, Long shoppingListId) {
		return persistenceService.findById(userId, shoppingListId);
	}
}
