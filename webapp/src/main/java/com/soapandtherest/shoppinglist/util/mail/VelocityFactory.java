package com.soapandtherest.shoppinglist.util.mail;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

class VelocityFactory {
	private static final Boolean initVelocitySyncBlock = true;

	public VelocityEngine getEngine() {
		return engine;
	}

	private VelocityEngine engine;

	private static VelocityFactory ourInstance = new VelocityFactory();

	public static VelocityFactory getInstance() {
		return ourInstance;
	}

	private VelocityFactory() {
		initVelocityEngine();
	}

	private void initVelocityEngine() {
		synchronized (initVelocitySyncBlock) {
			if (engine == null) {
				engine = new VelocityEngine();
				engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
				engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
				engine.init();
			}
		}
	}
}
