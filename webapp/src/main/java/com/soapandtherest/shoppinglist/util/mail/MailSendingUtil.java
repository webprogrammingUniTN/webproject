package com.soapandtherest.shoppinglist.util.mail;

import java.io.StringWriter;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

public class MailSendingUtil {
	private final static String MAIL_PROPERTIES_FILE_NAME = "/mail.properties";
	private final static String TEMPLATE_FOLDER = "/template";
	private final static Properties properties = com.soapandtherest.shoppinglist.data.util.Properties
			.loadPropertiesFromClassPath(MAIL_PROPERTIES_FILE_NAME, MailSendingUtil.class);

	public static void mailSending(String toEmail, String subject, String templateName, VelocityContext emailContext) {
		// Create a Session object to represent a mail session with the specified
		// properties.
		Session session = Session.getDefaultInstance(properties);
		Transport transport = null;
		try {
			// Create a message with the specified information.
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(properties.getProperty("mail.from.address"),
					properties.getProperty("mail.from.name")));
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
			msg.setSubject(subject);
			msg.setText(getMail(templateName, emailContext, TemplateType.TEXT));
			msg.setContent(getMail(templateName, emailContext, TemplateType.HTML), "text/html");

			// Create a transport.
			transport = session.getTransport();

			// Connect to Amazon SES using the SMTP username and password you specified
			// above.
			transport.connect(properties.getProperty("mail.serverurl"), properties.getProperty("mail.auth.username"),
					properties.getProperty("mail.auth.password"));

			// Send the email.
			transport.sendMessage(msg, msg.getAllRecipients());
		} catch (Exception ex) {
			System.out.println("The email was not sent.");
			System.out.println("Error message: " + ex.getMessage());
		} finally {
			if (transport != null) {
				// Close and terminate the connection.
				try {
					transport.close();
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static String getMail(String templateName, VelocityContext context, TemplateType templateType) {
		Template t = VelocityFactory.getInstance().getEngine()
				.getTemplate(TEMPLATE_FOLDER + "/" + templateName + templateType.toString() + ".vm");

		StringWriter writer = new StringWriter();
		t.merge(context, writer);
		return writer.toString();
	}

	public enum TemplateType {
		TEXT("TEXT"), HTML("HTML");

		private String name;

		// Constructor
		TemplateType(String name) {
			this.name = name;
		}

		public String toString() {
			return "_" + name;
		}
	}

	public static String getServiceHost() {
		return properties.getProperty("mail.servicehost", "shoppinglist.wilpot.eu");
	}
}
