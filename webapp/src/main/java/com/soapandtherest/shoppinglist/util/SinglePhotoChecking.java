package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.file.Image;
import java.util.ArrayList;
import java.util.List;

public class SinglePhotoChecking implements ImageChecking {
	private List<String> idsToCheck;
	public SinglePhotoChecking(String currentImageId) {
		idsToCheck = new ArrayList<>();
		idsToCheck.add(currentImageId);
	}

	@Override
	public boolean checkImageExistIntoList(Image image) {
		return this.idsToCheck.contains(image.getObjectKey());
	}
}
