package com.soapandtherest.shoppinglist.util.user;

import com.soapandtherest.shoppinglist.data.persistence.UserDetailPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;

class UserUtilJdbc implements UserInterface {
	private static UserDetailPersistence persistenceService = new UserDetailPersistenceJdbc();

	@Override
	public UserDetailRecord getUserByEmail(String email) {
		return persistenceService.findByEmail(email);
	}

	@Override
	public UserDetailRecord findUserById(Long id) {
		return persistenceService.findById(id);
	}
}
