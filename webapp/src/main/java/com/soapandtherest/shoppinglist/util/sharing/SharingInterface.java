package com.soapandtherest.shoppinglist.util.sharing;

import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserRecord;
import java.util.List;

public interface SharingInterface {
	List<UserHasShoppingListRecord> userHasShoppingList(Long userId);
	List<Long> userHasShoppingListId(Long userId);
	void addOwnerAuth(UserRecord user, ShoppingListRecord shoppingList);
	UserHasShoppingListRecord userHasShoppingList(Long userId, Long shoppingListId);
}
