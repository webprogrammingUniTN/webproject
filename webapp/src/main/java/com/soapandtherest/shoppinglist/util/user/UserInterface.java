package com.soapandtherest.shoppinglist.util.user;

import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;

public interface UserInterface {
	UserDetailRecord getUserByEmail(String email);
	UserDetailRecord findUserById(Long id);
}
