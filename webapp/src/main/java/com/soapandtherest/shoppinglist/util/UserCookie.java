/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

import static java.lang.Long.parseLong;
import java.util.UUID;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gionni_2
 */
public class UserCookie {

	// Durata dei cookie: 7 giorni
	static final int COOKIE_DURATION = 7 * 24 * 60 * 60;
	// Durata dei cookie di accettazione: 1 anno
	static final int COOKIE_ACCEPT_DURATION = 365 * 24 * 60 * 60;

	public static void set(HttpServletRequest request, HttpServletResponse response, String name, String value,
			int duration) {
		if ((value != null) && (!value.equals(get(request, name)) || (duration == 0))) {
			Cookie cookie = new Cookie(name, value);
			cookie.setMaxAge(duration);
			// if (duration == 0) {
			// cookie.setValue("");
			// cookie.setPath("/");
			// }
			response.addCookie(cookie);
		}
	}

	public static void set(HttpServletRequest request, HttpServletResponse response, String name, String value) {
		set(request, response, name, value, COOKIE_DURATION);
	}

	public static void clear(HttpServletRequest request, HttpServletResponse response, String name) {
		if (request != null) {
			Cookie[] cookies = request.getCookies();
			if (cookies != null)
				for (Cookie c : cookies)
					if (c.getName().equals(name))
						set(request, response, name, c.getValue(), 0);
		}
	}

	public static String get(HttpServletRequest request, String name) {
		if (request != null) {
			Cookie[] cookies = request.getCookies();
			if (cookies != null)
				for (Cookie c : cookies)
					if (c.getName().equals(name))
						return c.getValue();
		}
		return null;
	}

	public static Long getLong(HttpServletRequest request, String name) {
		String c = UserCookie.get(request, name);
		if (c != null)
			return parseLong(c);
		return null;
	}

	public static void setRegisteredIdUser(HttpServletRequest request, HttpServletResponse response, Long idUser) {
		set(request, response, "IDUSER_REGISTERED", idUser.toString());
	}

	public static Long getRegistredIdUser(HttpServletRequest request) {
		return getLong(request, "IDUSER_REGISTERED");
	}

	public static void clearRegisteredIdUser(HttpServletRequest request, HttpServletResponse response) {
		clear(request, response, "IDUSER_REGISTERED");

		// pulizia vecchio cookie
		if (get(request, "IDUSER") != null)
			clear(request, response, "IDUSER");
	}

	public static void setAnonymousIdUser(HttpServletRequest request, HttpServletResponse response, Long idUser) {
		set(request, response, "IDUSER_ANONYMOUS", idUser.toString());
	}

	public static Long getAnonymousIdUser(HttpServletRequest request) {
		return getLong(request, "IDUSER_ANONYMOUS");
	}

	public static String getSetUUID(HttpServletRequest request, HttpServletResponse response) {
		String cookieUUID = UserCookie.get(request, "UUID");
		if (cookieUUID == null) {
			cookieUUID = UUID.randomUUID().toString();
			set(request, response, "UUID", cookieUUID);
		}
		return cookieUUID;
	}

	public static String getUUID(HttpServletRequest request) {
		return get(request, "UUID");
	}

	public static void accept(HttpServletRequest request, HttpServletResponse response) {
		if (!isAccepted(request)) {
			Cookie cookie = new Cookie("COOKIE_ACCEPT", "Y");
			cookie.setMaxAge(COOKIE_ACCEPT_DURATION);
			response.addCookie(cookie);
		}
	}

	public static boolean isAccepted(HttpServletRequest request) {
		String accepted = UserCookie.get(request, "COOKIE_ACCEPT");
		return (accepted != null) && (accepted.equals("Y"));
	}

}
