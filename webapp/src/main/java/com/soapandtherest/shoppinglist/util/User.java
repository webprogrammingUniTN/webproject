/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserRecord;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Gionni_2
 */
public class User {

	// USER
	public static final boolean ANONYMOUS = true;
	public static final boolean NOT_ANONYMOUS = false;

	public static Boolean getAnonymous(Long idUser) {
		UserPersistenceJdbc userProvider = new UserPersistenceJdbc();
		UserRecord user = new UserRecord();
		user.setIduser(idUser);
		if (!userProvider.load(user))
			return null;
		if ((user.getIduser() != null) && (user.getAnonymous() == null)) {
			// Serve nel caso il record ci sia ma abbia il campo "anonymous" a null
			user.setAnonymous(ANONYMOUS);
			userProvider.save(user);
		}
		return user.getAnonymous();
	}

	public static long newAnonymous(HttpServletRequest request, HttpServletResponse response) {
		UserPersistenceJdbc userProvider = new UserPersistenceJdbc();
		UserRecord user = new UserRecord();
		user.setAnonymous(ANONYMOUS);
		Long idAnonymousUser = userProvider.create(user).getIduser();
		String idComputer = UserCookie.getSetUUID(request, response);
		UserCookie.setAnonymousIdUser(request, response, idAnonymousUser);
		UserComputer.getSet(idComputer, idAnonymousUser, UserComputer.NOT_REMEMBER);
		UserSession.reset(request, idAnonymousUser);
		return idAnonymousUser;
	}

	public static long newAnonymous(HttpServletRequest request) {
		UserPersistenceJdbc userProvider = new UserPersistenceJdbc();
		UserRecord user = new UserRecord();
		user.setAnonymous(ANONYMOUS);
		Long idAnonymousUser = userProvider.create(user).getIduser();
		String idComputer = UserCookie.getUUID(request);
		UserComputer.getSet(idComputer, idAnonymousUser, UserComputer.NOT_REMEMBER);
		UserSession.reset(request, idAnonymousUser);
		return idAnonymousUser;
	}

	public static long newAnonymous(HttpServletRequest request, HttpServletResponse response, Long idAnonymousUser,
			String uuid) {
		UserPersistenceJdbc userProvider = new UserPersistenceJdbc();
		UserRecord user = new UserRecord();
		user.setIduser(idAnonymousUser);
		if (!userProvider.load(user))
			return newAnonymous(request, response);
		user.setAnonymous(ANONYMOUS);
		userProvider.save(user);
		UserComputer.set(uuid, idAnonymousUser, UserComputer.NOT_REMEMBER);
		UserCookie.set(request, response, "UUID", uuid);
		UserCookie.setAnonymousIdUser(request, response, idAnonymousUser);
		UserSession.reset(request, idAnonymousUser);
		return idAnonymousUser;
	}

	public static long newAnonymous(HttpServletRequest request, Long idAnonymousUser, String uuid) {
		UserPersistenceJdbc userProvider = new UserPersistenceJdbc();
		UserRecord user = new UserRecord();
		user.setIduser(idAnonymousUser);
		if (!userProvider.load(user))
			return newAnonymous(request);
		user.setAnonymous(ANONYMOUS);
		userProvider.save(user);
		UserComputer.set(uuid, idAnonymousUser, UserComputer.NOT_REMEMBER);
		UserSession.reset(request, idAnonymousUser);
		return idAnonymousUser;
	}
}
