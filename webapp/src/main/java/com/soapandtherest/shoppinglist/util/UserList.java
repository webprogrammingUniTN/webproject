/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.util;

import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Gionni_2
 */
public class UserList {

	public static UserHasShoppingListRecord userHasShoppingList(HttpServletRequest request, Long idList) {
		Long idUser = UserSession.getIdUser(request);
		if (idUser == null)
			return null;

		// Il parametro vince sulla sessione. Passando -1 si forza l'uso del CurrentList
		// di sessione.
		if ((idList == null) || (idList <= 0)) {
			idList = UserSession.getCurrentList(request);
			if (idList == null)
				return null;
		}

		return userHasShoppingList(idUser, idList);
	}

	public static UserHasShoppingListRecord userHasShoppingList(Long idUser, Long idList) {
		return new UserHasShoppingListPersistenceJdbc().findById(idUser, idList);
	}

}
