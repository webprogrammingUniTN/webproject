package com.soapandtherest.shoppinglist.filters;

public enum AuthRoles {
	ADMINISTRATOR("ADMINISTRATOR"), STANDARD("STANDARD"), ANONYMOUS("ANONYMOUS");

	private String name;

	// Constructor
	AuthRoles(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
}
