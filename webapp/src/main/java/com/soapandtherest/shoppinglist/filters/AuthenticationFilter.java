package com.soapandtherest.shoppinglist.filters;

import com.soapandtherest.shoppinglist.util.InitSession;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import org.slf4j.LoggerFactory;

/**
 * This filter verify the access permissions for a user based on username and
 * passowrd provided in request
 */
@Provider
public class AuthenticationFilter implements javax.ws.rs.container.ContainerRequestFilter {
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);

	private static final String NOT_AUTH_MESSAGE = "You aren't authorized to executed this request";
	private static final String COOKIE_DIRECTIVE_MESSAGE = "Please confirm the cookie directive, before proceed";

	@Context
	private ResourceInfo resourceInfo;

	@Context
	private HttpServletRequest servletRequest;

	@Override
	public void filter(ContainerRequestContext requestContext) {
		logger.info("Request uri: " + requestContext.getUriInfo().getPath());
		Method method = resourceInfo.getResourceMethod();

		// Verify user access
		if (method.isAnnotationPresent(RolesAllowed.class)) {
			RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
			Set<AuthRoles> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));

			HttpSession session = servletRequest.getSession(false);

			// TODO: resolve cookie management
			/*
			 * // Test cookie acceptance if (session == null ||
			 * method.isAnnotationPresent(WithoutCookie.class)) { throw new
			 * NotAuthorizedException(COOKIE_DIRECTIVE_MESSAGE); }
			 */

			if (session == null) {
				session = servletRequest.getSession(true);
			}

			AuthRoles currentRole = AuthRoles.ANONYMOUS;

			if (null == session.getAttribute("iduser"))
				InitSession.setSession(servletRequest);

			if (null != session.getAttribute("user_role")) {
				Object role = session.getAttribute("user_role");
				if (role != null) {
					if (role instanceof AuthRoles) {
						currentRole = (AuthRoles) role;
					}
				}
			}
			// Is user valid?
			if (!isUserAllowed(currentRole, rolesSet)) {
				logger.info("Request refused for AuthRoles");
				logger.info("Request auth: " + rolesSet.toString() + " Current auth: " + currentRole.toString());
				throw new NotAuthorizedException(NOT_AUTH_MESSAGE);
			}
		} else {
			logger.info("Request refused for no annotation");
			throw new NotAuthorizedException(NOT_AUTH_MESSAGE);
		}
	}

	/**
	 * Check if a user can execute a request
	 *
	 * @param rolesSet
	 * @return
	 */
	private boolean isUserAllowed(AuthRoles userCurrentRole, final Set<AuthRoles> rolesSet) {
		boolean isAllowed = false;

		// Step 2. Verify user role
		if (rolesSet.contains(userCurrentRole)) {
			isAllowed = true;
		}
		return isAllowed;
	}
}
