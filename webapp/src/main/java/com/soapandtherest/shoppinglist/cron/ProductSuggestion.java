package com.soapandtherest.shoppinglist.cron;

import com.soapandtherest.shoppinglist.data.persistence.ProductPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListHasProductPersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserDetailPersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListHasProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListHasProductRecord;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.util.mail.MailSendingUtil;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class ProductSuggestion implements org.quartz.Job {
	private static final Logger logger = LogManager.getLogger(ProductSuggestion.class.getName());
	private static final String SUBJECT_PRODUCT_SUGGESTION = "";

	private static final ProductPersistence persistenceService = new ProductPersistenceJdbc();
	private static final ShoppingListHasProductPersistence bindingService = new ShoppingListHasProductPersistenceJdbc();
	private static final UserHasShoppingListPersistence userHasShoppingListPersistenceService = new UserHasShoppingListPersistenceJdbc();
	private static final UserDetailPersistence userPersistenceService = new UserDetailPersistenceJdbc();

	public ProductSuggestion() {
	}
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("Start expiration cron job");
		List<ShoppingListHasProductRecord> expiredProducts = bindingService.findAllExpiredProduct();
		logger.info("Expired element: " + expiredProducts.size());
		for (ShoppingListHasProductRecord expiredProduct : expiredProducts) {
			ProductRecord expiredProductRecord = persistenceService.findById(expiredProduct.getProductIdproduct());
			List<UserHasShoppingListRecord> userRecords = userHasShoppingListPersistenceService
					.findByShoppingListId(expiredProduct.getShoppingListIdshoppingList());
			for (UserHasShoppingListRecord userHasShoppingListRecord : userRecords) {
				if (userHasShoppingListRecord.getCanAddElement() || userHasShoppingListRecord.getListOwner()) {
					UserDetailRecord userRecord = userPersistenceService
							.findById(userHasShoppingListRecord.getUserIduser());
					VelocityContext mailContext = new VelocityContext();
					context.put("peopleName", userRecord.getName());
					context.put("productName", expiredProductRecord.getName());
					MailSendingUtil.mailSending(userRecord.getEmail(), SUBJECT_PRODUCT_SUGGESTION, "productsuggestion",
							mailContext);
				}
			}
		}
		logger.info("End expiration cron job");
	}
}
