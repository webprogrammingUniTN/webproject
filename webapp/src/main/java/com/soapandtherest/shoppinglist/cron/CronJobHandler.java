package com.soapandtherest.shoppinglist.cron;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

public abstract class CronJobHandler {
	private static final SchedulerFactory sf = new StdSchedulerFactory();
	private static Scheduler sched = null;
	private static final Logger logger = LogManager.getLogger(CronJobHandler.class.getName());

	public static void scheduleJob() {
		try {
			if (sched == null) {
				sched = sf.getScheduler();
			}
			// define the job and tie it to our MyJob class
			JobDetail job = newJob(ProductSuggestion.class).withIdentity("jobSuggestion", "groupSuggestion").build();

			// Trigger the job to run now, and then repeat every 40 seconds
			Trigger trigger = newTrigger().withIdentity("triggerSuggestion", "groupSuggestion").startNow()
					.withSchedule(simpleSchedule().withIntervalInMinutes(1).repeatForever()).build();
			// Tell quartz to schedule the job using our trigger
			sched.scheduleJob(job, trigger);
			sched.start();
		} catch (SchedulerException e) {
			logger.error("Error during the the startup of the scheduler, specific error: " + e);
		}
	}
	public static void stopJob() {
		try {
			if (sched.isStarted()) {
				sched.shutdown(true);
			}
		} catch (SchedulerException e) {
			logger.error("Error during the the shutdown of the scheduler, specific error: " + e);
		}
		/*
		 * try { sched.clear(); } catch (SchedulerException e) { logger.
		 * error("Error during the the shutdown of the scheduler, specific error: " +
		 * e); }
		 */
	}
}
