/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.websocket;

import com.google.gson.Gson;
import com.soapandtherest.shoppinglist.view.MessageToSendPayload;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author mousl
 */
public class MessageDecoder implements Decoder.Text<MessageToSendPayload> {
	private static Gson gson = new Gson();

	@Override
	public MessageToSendPayload decode(String s) throws DecodeException {
		return gson.fromJson(s, MessageToSendPayload.class);
	}

	@Override
	public boolean willDecode(String s) {
		return (s != null);
	}

	@Override
	public void init(EndpointConfig endpointConfig) {
		// Custom initialization logic
	}

	@Override
	public void destroy() {
		// Close resources
	}
}
