/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.websocket;

import com.google.gson.Gson;
import com.soapandtherest.shoppinglist.data.persistence.MessagePersistence;
import com.soapandtherest.shoppinglist.data.persistence.MessageTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.MessagePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.MessageTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.MessageRecord;
import com.soapandtherest.shoppinglist.data.record.MessageTemplateRecord;
import com.soapandtherest.shoppinglist.view.MessageTemplate;
import com.soapandtherest.shoppinglist.util.ListChat;
import com.soapandtherest.shoppinglist.util.sharing.SharingInterface;
import com.soapandtherest.shoppinglist.util.sharing.SharingInterfaceFactory;
import com.soapandtherest.shoppinglist.view.MessageToReceivePayload;
import com.soapandtherest.shoppinglist.view.MessageToSendPayload;
import com.soapandtherest.shoppinglist.view.WebsocketUserName;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import javax.ws.rs.core.Context;
import okhttp3.WebSocket;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mousl
 */
@ServerEndpoint(value = "/messaging", decoders = MessageDecoder.class, encoders = MessageEncoder.class, configurator = HttpSessionConfigurator.class)
public class Messaging {

	private static org.slf4j.Logger logger = LoggerFactory.getLogger(Messaging.class);

	static Map<Session, Long> chatroomUsers = Collections.synchronizedMap(new HashMap<Session, Long>());
	static Map<Long, List<Session>> usersMap = new HashMap<>();
	static SharingInterface IDListHasUserInterface = SharingInterfaceFactory.BuildSharingInterface();

	Long[] IDlistArray = ListChat.getShoppingListID();
	List<Long> IDListHasUserList;
	Long[] IDListHasUser;

	long currentSessionUserId;

	public Long[] ListToArray() {
		return IDListHasUserList.toArray(IDListHasUser);
	}

	@OnOpen
	public void handleOpen(Session userSession, EndpointConfig config) {
		HttpSession httpSession = (HttpSession) config.getUserProperties().get("httpSession");
		this.currentSessionUserId = (long) httpSession.getAttribute("iduser");
		IDListHasUserList = IDListHasUserInterface.userHasShoppingListId(this.currentSessionUserId);
		IDListHasUser = new Long[IDListHasUserList.size()];

		for (Long idList : IDListHasUserList) {
			if (!usersMap.containsKey(idList)) {
				usersMap.put(idList, new LinkedList<>());
			}
			usersMap.get(idList).add(userSession);
		}
		chatroomUsers.put(userSession, currentSessionUserId);
	}

	@OnMessage
	public void handleMessage(MessageToSendPayload message, Session userSession) throws IOException {
		try {
			List<Session> sessionsToSend = usersMap.get(message.getListId());
			for (Session sess : sessionsToSend) {
				Long sessionUserId = chatroomUsers.get(sess);
				String userName = new WebsocketUserName(currentSessionUserId).getUserName();
				MessageToReceivePayload msg = new MessageToReceivePayload(sessionUserId == this.currentSessionUserId,
						userName, message.getListId(), new MessageTemplate(message.getMessage()));
				sess.getBasicRemote().sendText(buildJsonData(msg));
			}
			saveMessage(message);
		} catch (Exception e) {
			logger.error("Error on message receiving" + e.toString());
		}
	}

	@OnClose
	public void handleClose(Session userSession) {
		IDListHasUser = ListToArray();
		/*
		 * for (Map.Entry<Long, List<Session>> entry : usersMap.entrySet()) { for (int j
		 * = 0; j < IDListHasUser.length; j++) { if (Objects.equals(entry.getKey(),
		 * IDListHasUser[j])) { chatroomUsers.remove(userSession);
		 * entry.setValue(chatroomUsers); } } }
		 */
		for (Long idList : IDListHasUserList) {
			usersMap.get(idList).remove(userSession);
			if (usersMap.get(idList).isEmpty()) {
				usersMap.remove(idList);
			}
		}
		chatroomUsers.remove(userSession);
	}

	@OnError
	public void handleError(Throwable t) {
		System.out.println("Error " + t.toString());
	}

	private String buildJsonData(MessageToReceivePayload msg) {
		Gson gson = new Gson();
		return gson.toJson(msg);
	}

	private void saveMessage(MessageToSendPayload message) {
		MessagePersistence persistenceService = new MessagePersistenceJdbc();
		MessageTemplatePersistence persistenceTemplateService = new MessageTemplatePersistenceJdbc();

		List<MessageTemplateRecord> messageTemplateIDList = persistenceTemplateService
				.findByMessageDescription(message.getMessage());
		long messageTemplateID = messageTemplateIDList.get(0).getIdmessage();

		MessageRecord record = new MessageRecord();
		record.setMessageTemplateIdmessage(messageTemplateID);
		record.setUserSender(this.currentSessionUserId);
		record.setShoppingListIdshoppingList(message.getListId());

		persistenceService.save(record);
	}

	public long getCurrentSessionUserId() {
		return currentSessionUserId;
	}

}
