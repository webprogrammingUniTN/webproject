/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.websocket;

import com.google.gson.Gson;
import com.soapandtherest.shoppinglist.view.MessageToSendPayload;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author mousl
 */
public class MessageEncoder implements Encoder.Text<MessageToSendPayload> {
	private static Gson gson = new Gson();

	@Override
	public String encode(MessageToSendPayload message) throws EncodeException {
		return gson.toJson(message);
	}

	@Override
	public void init(EndpointConfig endpointConfig) {
		// Custom initialization logic
	}

	@Override
	public void destroy() {
		// Close resources
	}
}
