package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/loadimage")
public class LoadImage {
	@POST
	@RolesAllowed({AuthRoles.ADMINISTRATOR})
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public com.soapandtherest.shoppinglist.view.File uploadImage(@Context HttpServletRequest req,
			com.soapandtherest.shoppinglist.view.File request) {
		Image imageToUpload = new Image(request.getObjectKey());
		String data = request.isValidForAddImage(imageToUpload, req.getSession());
		if (!data.isEmpty()) {
			throw new IllegalArgumentException(data);
		}
		imageToUpload.CompleteUpload(true);
		return new com.soapandtherest.shoppinglist.view.File(imageToUpload.GetPresignedURL(),
				imageToUpload.getObjectKey());
	}
}
