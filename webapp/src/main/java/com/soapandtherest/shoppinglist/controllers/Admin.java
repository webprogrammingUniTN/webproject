package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.server.mvc.Viewable;
import org.slf4j.LoggerFactory;

@Path("/admin")
public class Admin {

	private static org.slf4j.Logger logger = LoggerFactory.getLogger(Admin.class);

	@GET
	@RolesAllowed({AuthRoles.ADMINISTRATOR})
	@Produces(MediaType.TEXT_HTML)
	public Viewable adminJSP() {
		logger.info("Request /admin endpoint");
		return new Viewable("/admin");
	}
}
