package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserComputer;
import com.soapandtherest.shoppinglist.util.UserCookie;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/signout")
public class SignOut {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	// It is included the anonymous role to rapair an illegal state machine
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR, AuthRoles.ANONYMOUS})

	public StandardResponse Bye() {
		Long idUser = UserSession.getIdUser(request);
		String idComputer = UserCookie.getUUID(request);
		if ((idUser != null) && (idComputer != null))
			UserComputer.clearRememberMe(idComputer, idUser);
		UserSession.reset(request, UserCookie.getAnonymousIdUser(request));
		UserCookie.clearRegisteredIdUser(request, response);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

}
