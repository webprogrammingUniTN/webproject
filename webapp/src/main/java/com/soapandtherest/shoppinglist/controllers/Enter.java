package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.InitSession;
import com.soapandtherest.shoppinglist.util.User;
import com.soapandtherest.shoppinglist.util.UserComputer;
import com.soapandtherest.shoppinglist.util.UserCookie;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import com.soapandtherest.shoppinglist.view.response.UserSessionResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/enter")
public class Enter {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse DoIt() {
		InitSession.checkCookies(request, response);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public UserSessionResponse ReadUserInfo() {
		Long idUser = UserSession.getIdUser(request);

		UserDetailRecord detail = new UserDetailRecord();
		detail.setUserIduser(idUser);
		if (new UserDetailPersistenceJdbc().load(detail))
			return new UserSessionResponse(detail, UserSession.get(request, "remember"));
		return new UserSessionResponse();
	}
}
