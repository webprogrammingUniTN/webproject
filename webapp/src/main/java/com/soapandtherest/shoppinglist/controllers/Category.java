package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductCategoriesPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.CategoryComplete;
import com.soapandtherest.shoppinglist.view.response.PartialElementList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.slf4j.LoggerFactory;

@Path("/category")
public class Category {
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(ShoppingListProductTemplate.class);

	private static ProductCategoriesPersistence persistenceService = new ProductCategoriesPersistenceJdbc();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public PartialElementList<com.soapandtherest.shoppinglist.view.Category> getCategoryList(
			@QueryParam("filter") String filter, @DefaultValue("1") @QueryParam("currentPage") int currentPage,
			@DefaultValue("15") @QueryParam("perPage") int perPage, @QueryParam("sortBy") String sortBy,
			@DefaultValue("false") @QueryParam("sortDesc") Boolean sortDesc) {
		List<ProductCategoriesRecord> categoriesRecord = persistenceService.findByFilterAndOrSortAndLimit(filter,
				sortBy, sortDesc, perPage, currentPage);
		List<com.soapandtherest.shoppinglist.view.Category> categoryList = new LinkedList<>();
		for (ProductCategoriesRecord record : categoriesRecord) {
			categoryList.add(new com.soapandtherest.shoppinglist.view.Category(record));
		}
		long numberOfTemplate = persistenceService.countAll();
		return new PartialElementList<>(numberOfTemplate, categoryList);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public CategoryComplete addProductTemplate(CategoryComplete category, @Context HttpServletRequest request) {
		String validation = category.isValidForAdd(request.getSession());
		if (!validation.isEmpty()) {
			throw new IllegalArgumentException(validation);
		}
		category.save(persistenceService);
		return category;
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public CategoryComplete updateProductTemplate(CategoryComplete category, @Context HttpServletRequest request) {
		ProductCategoriesRecord record = persistenceService.findById(category.getId());
		if (record == null) {
			throw new IllegalArgumentException("Given id isn't valid");
		}
		String validation = category.isValidForUpdate(request.getSession(), record);
		if (!validation.isEmpty()) {
			throw new IllegalArgumentException(validation);
		}
		category.save(persistenceService);
		return category;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@RolesAllowed({AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public CategoryComplete singleCategory(@PathParam("id") Long id) {
		ProductCategoriesRecord record = persistenceService.findById(id);
		if (record == null) {
			throw new IllegalArgumentException("Given id isn't valid");
		}
		return new CategoryComplete(record);
	}
}
