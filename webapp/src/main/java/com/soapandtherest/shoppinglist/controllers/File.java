package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/files")
public class File {
	private static final int MAX_FILE_SIZE = 1048576;
	private static final int MIN_FILE_SIZE = 0;
	private static final int MAX_PARALLEL_UPLOAD = 10;

	public static final String FILES_LIST_SESSION_KEY = "files";

	private static Logger logger = LoggerFactory.getLogger(File.class);

	@POST
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD})
	@Produces(MediaType.APPLICATION_JSON)
	public com.soapandtherest.shoppinglist.view.File getFile(@Context HttpServletRequest req,
			com.soapandtherest.shoppinglist.view.request.File request) {
		logger.info("Request /file endpoint");
		if (request.getByteContentSize() >= MAX_FILE_SIZE) {
			logger.info("Le contenu a taille plus que le permit, le request est refuse");
			throw new IllegalArgumentException("Given content is bigger than size permit");
		}
		if (request.getByteContentSize() <= MIN_FILE_SIZE) {
			logger.info("Le contenu a taille moins que le permit, le request est refuse");
			throw new IllegalArgumentException("Given content is less than size permit");
		}
		com.soapandtherest.shoppinglist.data.file.File file = com.soapandtherest.shoppinglist.data.file.File
				.UploadPresignedURL(request.getByteContentSize());
		logger.info("Generated upload url: " + file.getUrl());
		if (req.getSession().getAttribute(FILES_LIST_SESSION_KEY) == null) {
			req.getSession().setAttribute(FILES_LIST_SESSION_KEY,
					new ArrayList<com.soapandtherest.shoppinglist.data.file.File>());
		}
		Object sessionUploadedFile = req.getSession().getAttribute(FILES_LIST_SESSION_KEY);
		if (sessionUploadedFile == null) {
			sessionUploadedFile = new ArrayList<com.soapandtherest.shoppinglist.data.file.File>();
		}
		if (sessionUploadedFile instanceof ArrayList) {
			while (((ArrayList) sessionUploadedFile).size() - MAX_PARALLEL_UPLOAD >= 0) {
				Object singleUploadedFile = ((ArrayList) sessionUploadedFile).get(0);
				if (singleUploadedFile instanceof com.soapandtherest.shoppinglist.data.file.File) {
					removeFile(req.getSession(), (com.soapandtherest.shoppinglist.data.file.File) singleUploadedFile,
							true);
				}
				((ArrayList) sessionUploadedFile).remove(0);
			}
			((ArrayList) sessionUploadedFile).add(file);
		}
		HttpSession session = req.getSession();
		session.setAttribute(FILES_LIST_SESSION_KEY, sessionUploadedFile);
		return new com.soapandtherest.shoppinglist.view.File(file.getUrl(), file.getObjectKey());
	}

	public static void removeFile(HttpSession session, com.soapandtherest.shoppinglist.data.file.File fileToRemove,
			boolean removeAlsoFromStorage) {
		if (session.getAttribute(FILES_LIST_SESSION_KEY) != null) {
			ArrayList sessionUploadedFile = (ArrayList) session.getAttribute(FILES_LIST_SESSION_KEY);
			if (removeAlsoFromStorage) {
				fileToRemove.DeleteFile();
			}
			sessionUploadedFile.remove(fileToRemove);
		}
	}

	public static boolean checkFileExist(HttpSession session, String objectKey) {
		Object sessionFileAttribute = session.getAttribute(FILES_LIST_SESSION_KEY);
		if (sessionFileAttribute instanceof ArrayList) {
			for (Object file : (ArrayList) sessionFileAttribute) {
				if (file instanceof com.soapandtherest.shoppinglist.data.file.File) {
					if (((com.soapandtherest.shoppinglist.data.file.File) file).getObjectKey().equals(objectKey)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
