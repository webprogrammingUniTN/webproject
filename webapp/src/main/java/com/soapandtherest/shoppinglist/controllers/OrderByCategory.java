/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductCategoriesPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.Product;
import com.soapandtherest.shoppinglist.view.ProductHasCategory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mousl
 */
@Path("/orderbycategory")
public class OrderByCategory {
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public Product[] orderByCategory(@PathParam("id") Long id) {
		ProductCategoriesPersistence persistenceService = new ProductCategoriesPersistenceJdbc();
		Product[] productsToSort = new AllProductList().getProductList(id);
		Collections.sort(Arrays.asList(productsToSort));

		return productsToSort;
	}
}
