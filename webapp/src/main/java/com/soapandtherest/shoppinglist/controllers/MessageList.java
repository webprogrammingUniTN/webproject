/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.MessagePersistence;
import com.soapandtherest.shoppinglist.data.persistence.MessageTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.MessagePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.MessageTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.MessageRecord;
import com.soapandtherest.shoppinglist.data.record.MessageTemplateRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.Message;
import com.soapandtherest.shoppinglist.view.MessageTemplate;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mousl
 */
@Path("/messagelist")
public class MessageList {
	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public MessageTemplate[] allMessageList(@PathParam("id") Long id) {
		MessagePersistence persistenceService = new MessagePersistenceJdbc();
		MessageTemplatePersistence persistenceTemplateService = new MessageTemplatePersistenceJdbc();

		List<MessageRecord> messageRecord = persistenceService.findByShoppingListId(id);
		List<Message> messageList = new LinkedList<>();
		messageRecord.forEach((record) -> {
			messageList.add(new Message(record));
		});

		List<MessageTemplateRecord> messageListDescription = new LinkedList<>();
		messageList.forEach((record) -> {
			messageListDescription.add(persistenceTemplateService.findById(record.getMessage_template_idmessage()));
		});

		List<MessageTemplate> templateList = new LinkedList<>();
		for (int i = 0; i < messageListDescription.size(); i++) {
			templateList.add(new MessageTemplate(messageListDescription.get(i), messageRecord.get(i).getUserSender()));
		}

		MessageTemplate[] messagesArray = new MessageTemplate[templateList.size()];
		return templateList.toArray(messagesArray);
	}
}
