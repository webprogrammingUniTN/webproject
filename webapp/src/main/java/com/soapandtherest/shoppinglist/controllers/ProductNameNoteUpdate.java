package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductRecord;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.SinglePhotoChecking;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/productnamenoteupdate")
public class ProductNameNoteUpdate {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse Update(com.soapandtherest.shoppinglist.view.request.ProductNameNoteUpdate requestBody) {
		ProductPersistenceJdbc provider = new ProductPersistenceJdbc();
		ProductRecord detail = new ProductRecord();
		detail.setIdproduct(requestBody.getIdProduct());
		if (!provider.load(detail))
			throw new IllegalArgumentException("Prodotto non trovato");
		if ((requestBody.getName() != null) && (!requestBody.getName().isEmpty()))
			detail.setName(requestBody.getName());
		if ((requestBody.getNote() != null) && (!requestBody.getNote().isEmpty()))
			detail.setNote(requestBody.getNote());
		provider.save(detail);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

}
