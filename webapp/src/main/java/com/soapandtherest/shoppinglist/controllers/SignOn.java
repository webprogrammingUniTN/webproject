package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.UserDetailPersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.data.record.UserRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.PasswordHashing;
import com.soapandtherest.shoppinglist.util.mail.MailSendingUtil;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.velocity.VelocityContext;
import org.glassfish.jersey.server.mvc.Viewable;
import org.slf4j.LoggerFactory;

@Path("/signon")
public class SignOn {

	public static final String EMAIL_VERIFICATION_SUBJECT = "Shopping List Account Verification";
	public static final String URL_CONTEXT_KEY = "url";
	public static final String BLACK_USER_AVATAR = "533cc8072612c34fee88dfee07fd9867be3aa9d668fca3b3555bdc822ea4c778";
	public static final String DEFAULT_USER_AVATAR = "a10defed10554999eb56acb50bb6e7ab17c28d5d031527e39090ef32556546ab";

	private static UserDetailPersistence detailProvider = new UserDetailPersistenceJdbc();
	private static UserPersistence userProvider = new UserPersistenceJdbc();

	@GET
	@Produces(MediaType.TEXT_HTML)
	@RolesAllowed({AuthRoles.ANONYMOUS})
	public Viewable ConfirmMail(@QueryParam("verificationNumber") String verificationNumber,
			@QueryParam("email") String email, @Context HttpServletRequest request) {
		UserDetailRecord detail = detailProvider.findByEmail(email);
		if (detail == null) {
			throw new IllegalArgumentException("Given email isn't valid");
		}
		Map<String, Object> params = new HashMap<>();
		params.put("redirectUrl", "/");
		return new Viewable("/redirect", params);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS})
	public StandardResponse Registration(com.soapandtherest.shoppinglist.view.request.SignOn requestBody) {
		String result = requestBody.isValid();
		if (!result.isEmpty()) {
			throw new IllegalArgumentException(result);
		}
		UserDetailRecord detail = detailProvider.findByEmail(requestBody.getEmail());
		// Try to search if password exist
		if (detail != null) {
			throw new IllegalArgumentException("Email già registrata.");
		}
		// Preparo i dati di dettaglio per l'inserimento
		detail = new UserDetailRecord();
		detail.setAdmin(Boolean.FALSE);
		detail.setName(requestBody.getName());
		detail.setSurname(requestBody.getSurname());
		detail.setEmail(requestBody.getEmail());
		detail.setAvatarImage(BLACK_USER_AVATAR);

		// Password encryption
		PasswordHashing ph = new PasswordHashing();
		String salt = ph.saltGenerate(ph.generateRandomInt(31, 40));
		int hashingTime = ph.generateRandomInt(12196, 14348);
		detail.setHashSalt(salt);
		detail.setHashHashingTime((long) hashingTime);
		try {
			detail.setHashLogin(ph.hashPassword(salt, hashingTime, requestBody.getPassword()));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeySpecException e) {
			throw new RuntimeException("Exception during hashing");
		}

		// Preparo i dati dell'utente per l'inserimento
		UserRecord user = new UserRecord();
		user.setAnonymous(Boolean.FALSE);

		// Prima inserisco USER e poi USER_DETAIL
		user = userProvider.create(user);
		detail.setUserIduser(user.getIduser());
		detailProvider.save(detail);

		// Invio Email
		VelocityContext context = new VelocityContext();
		context.put(URL_CONTEXT_KEY, "http://" + MailSendingUtil.getServiceHost() + "/signon");
		MailSendingUtil.mailSending(detail.getEmail(), EMAIL_VERIFICATION_SUBJECT, "signon", context);

		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}
}
