/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasFileRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.ShoppingList;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mousl
 */
@Path("/saveshoppinglist")
public class SaveShoppingList {
	private static ShoppingListPersistence persistenceService = new ShoppingListPersistenceJdbc();
	private static UserHasShoppingListPersistence userHasShoppingListPersistenceService = new UserHasShoppingListPersistenceJdbc();
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR})
	public boolean saveShoppingList(ShoppingList data, @Context HttpServletRequest request) {
		String error = data.isValid(request.getSession());
		if (!error.isEmpty()) {
			throw new IllegalArgumentException(error);
		}

		ShoppingListRecord record = new ShoppingListRecord();
		ShoppingListRecord saved = new ShoppingListRecord();
		record.setTemplateId(data.getTemplateId());
		record.setName(data.getName());
		record.setDescription(data.getDescription());
		if (data.getFile().isUploadFile()) {
			data.getFile().CompleteUpload();
		}
		record.setImage(data.getFile().getObjectKey());
		saved = persistenceService.create(record);

		UserHasShoppingListRecord userRecord = new UserHasShoppingListRecord();
		userRecord.setUserIduser(UserSession.getIdUser(request));
		userRecord.setShoppingListIdshoppingList(saved.getIdshoppingList());
		userRecord.setListOwner(Boolean.TRUE);
		userRecord.setCanAddElement(Boolean.TRUE);
		userRecord.setCanDeleteElement(Boolean.TRUE);
		userRecord.setCanDeleteList(Boolean.TRUE);
		userRecord.setCanEditElement(Boolean.TRUE);
		userRecord.setCanEditListDetail(Boolean.TRUE);
		userHasShoppingListPersistenceService.save(userRecord);

		return true;
	}
}
