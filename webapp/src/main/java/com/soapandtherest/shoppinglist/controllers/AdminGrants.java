package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.UserDetailPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/admingrants")
public class AdminGrants {

	@Context
	private HttpServletRequest request;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR})
	public StandardResponse AddAdminGrants(com.soapandtherest.shoppinglist.view.request.Forgotten requestBody) {
		String result = requestBody.isValid();
		if (!result.isEmpty())
			throw new IllegalArgumentException(result);
		result = setAdmin(requestBody.getEmail(), Boolean.TRUE);
		if (!result.isEmpty())
			throw new IllegalArgumentException(result);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, "Diritti di amministrazione concessi.");
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR})
	public StandardResponse RemoveAdminGrants(com.soapandtherest.shoppinglist.view.request.Forgotten requestBody) {
		String result = requestBody.isValid();
		if (!result.isEmpty())
			throw new IllegalArgumentException(result);
		if (requestBody.getEmail().equals(UserSession.get(request, "email")))
			throw new IllegalArgumentException("Non puoi rimuoverti i diritti. Deve farlo un altro amministratore.");
		result = setAdmin(requestBody.getEmail(), Boolean.FALSE);
		if (!result.isEmpty())
			throw new IllegalArgumentException(result);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, "Diritti di amministrazione revocati.");
	}

	private String setAdmin(String email, Boolean admin) {
		UserDetailPersistence detailProvider = new UserDetailPersistenceJdbc();
		UserDetailRecord detail = detailProvider.findByEmail(email);
		if (detail == null)
			return "Email non registrata";
		if (detail.getAdmin().equals(admin)) {
			if (admin)
				return "Non puoi concedere diritti di amministrazione a un amministratore.";
			else
				return "Non puoi revocare i diritti di amministrazione a un utente non amministratore.";
		}
		detail.setAdmin(admin);
		detailProvider.save(detail);
		return "";
	}
}
