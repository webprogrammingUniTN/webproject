/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserList;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vbeatrice
 */
@Path("/deleteshoppinglist")
public class DeleteShoppingList {
	@Context
	private HttpServletRequest request;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public boolean deleteShoppingList(@PathParam("id") Long id) {
		ShoppingListPersistence persistenceService = new ShoppingListPersistenceJdbc();
		UserHasShoppingListPersistence userPersistenceService = new UserHasShoppingListPersistenceJdbc();
		List<UserHasShoppingListRecord> userLists = userPersistenceService.findByShoppingListId(id);

		UserHasShoppingListRecord uhsl = UserList.userHasShoppingList(request, id);
		if (uhsl == null)
			throw new IllegalArgumentException("User cannot access to current list.");
		if (!uhsl.getCanDeleteList())
			throw new IllegalArgumentException("User cannot delete this list.");

		for (int i = 0; i < userLists.size(); i++)
			userPersistenceService.deleteById(userLists.get(i).getUserIduser(), id);
		persistenceService.deleteById(id);
		return true;
	}
}
