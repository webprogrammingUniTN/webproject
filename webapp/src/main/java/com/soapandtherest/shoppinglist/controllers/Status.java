package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.LoggerFactory;

@Path("/status")
public class Status {

	private static org.slf4j.Logger logger = LoggerFactory.getLogger(Status.class);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD})
	public String status() {
		logger.info("Request /status endpoint");
		return "status";
	}
}
