package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ProductTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductTemplateRecord;
import com.soapandtherest.shoppinglist.view.ProductTemplate;
import com.soapandtherest.shoppinglist.view.response.PartialElementList;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.slf4j.LoggerFactory;

@Path("/category/{categoryid}/products")
public class ShoppingListProductTemplate {
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(ShoppingListProductTemplate.class);

	private static ProductTemplatePersistence persistenceService = new ProductTemplatePersistenceJdbc();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	// TODO: add who is authorized
	public PartialElementList<ProductTemplate> getProductList(@PathParam("categoryid") long categoryId,
			@QueryParam("filter") String filter, @DefaultValue("1") @QueryParam("currentPage") int currentPage,
			@DefaultValue("15") @QueryParam("perPage") int perPage, @QueryParam("sortBy") String sortBy,
			@DefaultValue("false") @QueryParam("sortDesc") Boolean sortDesc) {
		logger.info("Request all shopping list template");
		List<ProductTemplateRecord> templateRecord = persistenceService.findByCategoryIdAndByFilterAndOrSortAndLimit(
				categoryId, filter, sortBy, sortDesc, perPage, currentPage);
		List<com.soapandtherest.shoppinglist.view.ProductTemplate> productTemplateList = new LinkedList<>();
		for (ProductTemplateRecord record : templateRecord) {
			productTemplateList.add(new com.soapandtherest.shoppinglist.view.ProductTemplate(record));
		}
		long numberOfTemplate = persistenceService.countAll();
		return new PartialElementList<>(numberOfTemplate, productTemplateList);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// TODO: add who is authorized
	public ProductTemplate addProductTemplate(ProductTemplate productTemplate, @Context HttpServletRequest request,
			@PathParam("categoryid") long categoryId) {
		String validation = productTemplate.isValidForAdd(request.getSession());
		if (!validation.isEmpty()) {
			throw new IllegalArgumentException(validation);
		}
		productTemplate.save(persistenceService, categoryId);
		return productTemplate;
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	// TODO: add who is authorized
	public ProductTemplate updateProductTemplate(ProductTemplate productTemplate, @Context HttpServletRequest request,
			@PathParam("categoryid") long categoryId) {
		ProductTemplateRecord record = persistenceService.findById(productTemplate.getId());
		if (record == null) {
			throw new IllegalArgumentException("Id not found");
		}
		String validation = productTemplate.isValidForUpdate(request.getSession(), record);
		if (!validation.isEmpty()) {
			throw new IllegalArgumentException(validation);
		}
		productTemplate.save(persistenceService, categoryId);
		return productTemplate;
	}

	@DELETE
	@Path("/{producttemplateid}")
	@Produces(MediaType.APPLICATION_JSON)
	// TODO: add who is authorized
	public StandardResponse deleteProductTemplate(@PathParam("categoryid") long categoryId,
			@PathParam("producttemplateid") long productTemplateId) {
		Boolean result = persistenceService.deleteById(productTemplateId);
		if (!result) {
			throw new IllegalArgumentException("Element you select don't exist");
		}
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}
}
