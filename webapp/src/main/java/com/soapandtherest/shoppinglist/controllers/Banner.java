package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserCookie;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/banner")
public class Banner {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse IsAccepted() {
		if (UserCookie.isAccepted(request))
			return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, "Y");
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, "N");
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse Accept() {
		UserCookie.accept(request, response);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

}
