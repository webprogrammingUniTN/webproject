package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.file.Image.ImageSize;
import com.soapandtherest.shoppinglist.data.persistence.PlaceTypePersistence;
import com.soapandtherest.shoppinglist.data.persistence.ProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplateHasFilePersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplateHasProductCategoriesPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.PlaceTypePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductCategoriesPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListTemplateHasFilePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListTemplateHasProductCategoriesPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasFileRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.ShoppingListTemplateComplete;
import com.soapandtherest.shoppinglist.view.response.PartialElementList;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.slf4j.LoggerFactory;

@Path("/template")
public class ShoppingListTemplate {

	public static final ImageSize IMAGES_SIZE = ImageSize.MEDIUM;
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(ShoppingListTemplate.class);

	private static ShoppingListTemplatePersistence persistenceService = new ShoppingListTemplatePersistenceJdbc();
	private static ShoppingListTemplateHasFilePersistence filePersistenceService = new ShoppingListTemplateHasFilePersistenceJdbc();
	private static ProductCategoriesPersistence categoryPersistenceService = new ProductCategoriesPersistenceJdbc();
	private static ShoppingListTemplateHasProductCategoriesPersistence categoryBindingPersistenceService = new ShoppingListTemplateHasProductCategoriesPersistenceJdbc();
	private static PlaceTypePersistence placeTypePersistence = new PlaceTypePersistenceJdbc();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR, AuthRoles.ANONYMOUS})
	public PartialElementList<com.soapandtherest.shoppinglist.view.ShoppingListTemplate> allTemplateList(
			@QueryParam("filter") String filter, @DefaultValue("1") @QueryParam("currentPage") int currentPage,
			@DefaultValue("15") @QueryParam("perPage") int perPage, @QueryParam("sortBy") String sortBy,
			@DefaultValue("false") @QueryParam("sortDesc") Boolean sortDesc) {
		logger.info("Request all shopping list template");
		List<ShoppingListTemplateRecord> templateRecord = persistenceService.findByFilterAndOrSortAndLimit(filter,
				sortBy, sortDesc, perPage, currentPage);
		List<com.soapandtherest.shoppinglist.view.ShoppingListTemplate> templateList = new LinkedList<>();
		for (ShoppingListTemplateRecord record : templateRecord) {
			templateList.add(new com.soapandtherest.shoppinglist.view.ShoppingListTemplate(record));
		}
		long numberOfTemplate = persistenceService.countAll();
		return new PartialElementList<>(numberOfTemplate, templateList);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR, AuthRoles.ANONYMOUS})
	public ShoppingListTemplateComplete singleTemplateList(@PathParam("id") Long id) {
		ShoppingListTemplateRecord record = persistenceService.findById(id);
		if (record == null) {
			throw new IllegalArgumentException("Given id isn't valid");
		}
		List<ShoppingListTemplateHasFileRecord> pairedImages = filePersistenceService.findByTemplateId(id);
		List<ProductCategoriesRecord> categories = categoryPersistenceService.findByTemplateId(id);
		ShoppingListTemplateComplete view = new ShoppingListTemplateComplete(record, pairedImages, categories);
		view.generatePreSignedURLs(IMAGES_SIZE);
		return view;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR})
	public ShoppingListTemplateComplete addTemplateList(ShoppingListTemplateComplete template,
			@Context HttpServletRequest request) {
		String isValidResponse = template.isValidForAdd(request.getSession(), categoryPersistenceService,
				placeTypePersistence);
		if (!isValidResponse.isEmpty()) {
			throw new IllegalArgumentException(isValidResponse);
		}
		template.save(persistenceService, filePersistenceService, categoryBindingPersistenceService);
		return template;
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR})
	public ShoppingListTemplateComplete updateTemplateList(@Context HttpServletRequest request,
			ShoppingListTemplateComplete template) {
		String isValidResponse = template.isValidForUpdate(persistenceService, filePersistenceService,
				request.getSession(), categoryPersistenceService, placeTypePersistence);
		if (!isValidResponse.isEmpty()) {
			throw new IllegalArgumentException(isValidResponse);
		}
		template.save(persistenceService, filePersistenceService, categoryBindingPersistenceService);
		return template;
	}
}
