package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.file.Image.ImageSize;
import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.exception.UnAuthorizedException;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.util.user.UserInterface;
import com.soapandtherest.shoppinglist.util.user.UserInterfaceFactory;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Path("/shoppinglist/{shoppingListId}/sharing")
public class ShoppingListSharing {
	private static final Logger logger = LogManager.getLogger(ShoppingListSharing.class.getName());
	public static final ImageSize AVATAR_IMAGE_SIZE = ImageSize.SMALL;

	private static UserHasShoppingListPersistence persistenceService = new UserHasShoppingListPersistenceJdbc();
	private static UserInterface userInterface = UserInterfaceFactory.BuildUserInterface();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public List<com.soapandtherest.shoppinglist.view.response.ShoppingListSharing> allSharing(
			@PathParam("shoppingListId") Long shoppingListId, @Context HttpServletRequest request)
			throws UnAuthorizedException {
		if (shoppingListId == null) {
			throw new IllegalArgumentException("Invalid shopping list ID");
		}
		Long idUser = UserSession.getLong(request, "iduser");
		if (idUser == null) {
			throw new IllegalArgumentException("Can't recovery user identifier");
		}
		boolean found = false;
		List<UserHasShoppingListRecord> sharingList = persistenceService.findByShoppingListId(shoppingListId);
		List<com.soapandtherest.shoppinglist.view.response.ShoppingListSharing> sharing = new LinkedList<>();
		for (UserHasShoppingListRecord record : sharingList) {
			if (idUser.equals(record.getUserIduser()) & record.getListOwner()) {
				found = true;
			}
			sharing.add(new com.soapandtherest.shoppinglist.view.response.ShoppingListSharing(record));
		}
		if (!found) {
			throw new UnAuthorizedException("User can't access to this shoppinglist sharing information");
		}
		return sharing;
	}

	// They work without problem at the same time as update and add API call
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public com.soapandtherest.shoppinglist.view.response.ShoppingListSharing addSharing(
			@PathParam("shoppingListId") Long shoppingListId,
			com.soapandtherest.shoppinglist.view.request.EditShoppingListSharing requestToAdd,
			@Context HttpServletRequest request) throws UnAuthorizedException {
		if (shoppingListId == null) {
			throw new IllegalArgumentException("Invalid shopping list ID");
		}
		Long idUser = UserSession.getLong(request, "iduser");
		if (idUser == null) {
			throw new IllegalArgumentException("Can't recovery user identifier");
		}
		com.soapandtherest.shoppinglist.view.response.ShoppingListSharing sharingElement = new com.soapandtherest.shoppinglist.view.response.ShoppingListSharing(
				requestToAdd, shoppingListId);
		String isValid = sharingElement.isValid();
		if (!isValid.isEmpty()) {
			logger.info("Invalid fields ", isValid);
			throw new IllegalArgumentException(isValid);
		}
		if (checkIfUserCanCRUDSharing(sharingElement.getUserId(), shoppingListId, idUser)) {
			throw new UnAuthorizedException("USer can edit settings of this album");
		}
		persistenceService.save(sharingElement.getUserHasShoppingListRecord());
		return sharingElement;
	}

	@DELETE
	@Path("/{email}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse deleteSharing(@PathParam("shoppingListId") Long shoppingListId,
			@PathParam("email") String email, @Context HttpServletRequest request) throws UnAuthorizedException {
		if (shoppingListId == null && email == null) {
			throw new IllegalArgumentException("Invalid shopping list ID or email");
		}
		Long idUser = UserSession.getLong(request, "iduser");
		if (idUser == null) {
			throw new IllegalArgumentException("Can't recovery user identifier");
		}
		UserDetailRecord record = userInterface.getUserByEmail(email);
		if (record == null) {
			throw new IllegalArgumentException("Invalid shopping list ID");
		}
		if (checkIfUserCanCRUDSharing(record.getUserIduser(), shoppingListId, idUser)) {
			throw new UnAuthorizedException("User can edit settings of this album");
		}
		UserHasShoppingListRecord sharingRecord = new UserHasShoppingListRecord();
		sharingRecord.setUserIduser(record.getUserIduser());
		sharingRecord.setShoppingListIdshoppingList(shoppingListId);
		persistenceService.delete(sharingRecord);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

	private boolean checkIfUserCanCRUDSharing(Long userIduser, Long shoppingListIdshoppingList, Long currentUserID) {
		UserHasShoppingListRecord element = persistenceService.findById(userIduser, shoppingListIdshoppingList);
		if (element == null) {
			return false;
		}
		if (element.getListOwner()) {
			return false;
		}
		if (currentUserID.equals(userIduser)) {
			return false;
		}
		return element.getListOwner();
	}
}
