/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.MessagePersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserDetailPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.MessagePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.MessageRecord;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.UserSender;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mousl
 */

@Path("/senderlist")
public class SenderList {

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public UserSender[] SenderList(@PathParam("id") Long id) {
		MessagePersistence persistenceService = new MessagePersistenceJdbc();
		UserDetailPersistence userPersistenceService = new UserDetailPersistenceJdbc();

		List<MessageRecord> messageRecord = persistenceService.findByShoppingListId(id);

		List<Long> senderList = new LinkedList<>();
		messageRecord.forEach((record) -> {
			senderList.add(record.getUserSender());
		});

		List<UserDetailRecord> userRecord = new LinkedList<>();
		senderList.forEach((i) -> {
			userRecord.add(userPersistenceService.findById(i));
		});

		List<UserSender> usernames = new LinkedList<>();
		userRecord.forEach((record) -> {
			usernames.add(new UserSender(record));
		});

		UserSender[] usernameArray = new UserSender[usernames.size()];
		return usernames.toArray(usernameArray);
	}
}
