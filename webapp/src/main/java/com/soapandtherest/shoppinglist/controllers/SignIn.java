package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.UserDetailPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserDetailPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.UserDetailRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.PasswordHashing;
import com.soapandtherest.shoppinglist.util.UserComputer;
import com.soapandtherest.shoppinglist.util.UserCookie;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.response.UserSessionResponse;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.slf4j.LoggerFactory;

@Path("/signin")
public class SignIn {
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(ShoppingListProductTemplate.class);

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS})
	public UserSessionResponse DoIt(com.soapandtherest.shoppinglist.view.request.SignIn requestBody) {

		logger.info("Begin of SignIn with DoIt");

		String valid = requestBody.isValid();
		if (!valid.isEmpty()) {
			logger.error("End with syntax error on Email or password");
			throw new IllegalArgumentException(valid);
		}
		UserDetailPersistence detailProvider = new UserDetailPersistenceJdbc();
		UserDetailRecord detail = detailProvider.findByEmail(requestBody.getEmail());
		if (detail == null) {
			logger.error("End with Email not found");
			throw new IllegalArgumentException("Utente e/o password non validi.");
		}

		// Email trovata...verifico la password indicata
		// Criptazione della password
		PasswordHashing ph = new PasswordHashing();
		String salt = detail.getHashSalt();
		Long hashingTime = detail.getHashHashingTime();
		String hash;
		try {
			hash = ph.hashPassword(salt, hashingTime.intValue(), requestBody.getPassword());
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException | InvalidKeySpecException e) {
			logger.error("End with exception during hashing");
			throw new RuntimeException("Exception during hashing");
		}
		if (!hash.equals(detail.getHashLogin())) {
			logger.error("End with password not correct");
			logger.debug("Calculate hash is: " + hash);
			logger.debug("Given hash is: " + detail.getHashLogin());
			throw new IllegalArgumentException("Utente e/o PassWord non validi.");
		}

		// La password corrisponde...
		// Aggiorno il cookie IDUSER (mantengo il cookie UUID se esistente)
		// e imposto la sessione
		Long idUser = detail.getUserIduser();
		if (requestBody.isRememberMe())
			UserCookie.setRegisteredIdUser(request, response, idUser);
		else
			UserCookie.clearRegisteredIdUser(request, response);

		String cookieUUID = UserCookie.getSetUUID(request, response);

		UserSession.set(request, idUser, Boolean.FALSE, requestBody.isRememberMe(), detail);

		// Aggiorno userComputer sulla scelta "rememberMe"
		if (!UserComputer.getSet(cookieUUID, idUser, requestBody.isRememberMe()))
			UserComputer.set(cookieUUID, idUser, requestBody.isRememberMe());

		logger.info("End with success");
		return new UserSessionResponse(detail, requestBody.isRememberMe() ? "Y" : "N");
	}

}
