/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ProductPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListHasProductPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListHasProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListHasProductRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.Product;
import com.soapandtherest.shoppinglist.view.ProductForAlphabet;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mousl
 */
@Path("/orderbyalphabet")
public class OrderByAlphabet {
	@Context
	private HttpServletRequest request;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public Product[] orderByAlphabet(@PathParam("id") Long id) {

		if (id.equals(new Long(-1)))
			id = UserSession.getCurrentList(request);

		ShoppingListHasProductPersistence persistenceService = new ShoppingListHasProductPersistenceJdbc();
		ProductPersistence productPersistenceService = new ProductPersistenceJdbc();

		List<ShoppingListHasProductRecord> productRecord = persistenceService.findByShoppingListId(id);
		ProductRecord productRecords;
		List<ProductForAlphabet> productList = new LinkedList<>();
		for (int i = 0; i < productRecord.size(); i++) {
			if (productRecord.get(i).getDone() != true) {
				productRecords = productPersistenceService.findById(productRecord.get(i).getProductIdproduct());
				productList.add(new ProductForAlphabet(productRecords));
			}
		}

		Product[] productsToSort = new ProductForAlphabet[productList.size()];
		productsToSort = productList.toArray(productsToSort);
		Collections.sort(Arrays.asList(productsToSort));
		return productsToSort;
	}

}
