package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.server.mvc.Viewable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/")
public class Home {
	private static Logger logger = LoggerFactory.getLogger(Admin.class);

	@GET
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD})
	@Produces(MediaType.TEXT_HTML)
	public Viewable homeJSP() {
		logger.info("Request /home endpoint");
		return new Viewable("/home");
	}
}
