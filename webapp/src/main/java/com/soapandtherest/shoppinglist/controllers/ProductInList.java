package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListHasProductPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListHasProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductRecord;
import com.soapandtherest.shoppinglist.data.record.ProductTemplateRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListHasProductRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserList;
import com.soapandtherest.shoppinglist.util.sharing.SharingInterface;
import com.soapandtherest.shoppinglist.util.sharing.SharingInterfaceFactory;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/productinlist")
public class ProductInList {
	private static final SharingInterface interfaceSharing = SharingInterfaceFactory.BuildSharingInterface();

	@Context
	private HttpServletRequest request;

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse Add(com.soapandtherest.shoppinglist.view.request.ProductInList requestBody) {
		// Se idShoppingList = -1 allora si usa la CurrentList di sessione
		UserHasShoppingListRecord uhsl = UserList.userHasShoppingList(request, requestBody.getShoppinlistId());
		if (uhsl == null)
			return new StandardResponse(0, "User cannot access to current list.");
		if (!uhsl.getCanAddElement())
			return new StandardResponse(0, "User cannot add elements to current list.");

		Long idShoppingList = uhsl.getShoppingListIdshoppingList();
		save(idShoppingList, requestBody.getProductId(), Boolean.FALSE, null);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse Remove(com.soapandtherest.shoppinglist.view.request.ProductInList requestBody) {
		// Se idShoppingList = -1 allora si usa la CurrentList di sessione
		UserHasShoppingListRecord uhsl = UserList.userHasShoppingList(request, requestBody.getShoppinlistId());
		if (uhsl == null)
			return new StandardResponse(0, "User cannot access to current list.");
		if (!uhsl.getCanDeleteElement())
			return new StandardResponse(0, "User cannot delete elements to current list.");
		Long idShoppingList = uhsl.getShoppingListIdshoppingList();
		delete(idShoppingList, requestBody.getProductId());
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse Done(com.soapandtherest.shoppinglist.view.request.ProductInList requestBody) {
		// Se idShoppingList = -1 allora si usa la CurrentList di sessione
		UserHasShoppingListRecord uhsl = UserList.userHasShoppingList(request, requestBody.getShoppinlistId());
		if (uhsl == null)
			return new StandardResponse(0, "User cannot access to current list.");
		if (!uhsl.getCanEditElement())
			return new StandardResponse(0, "User cannot edit elements to current list.");

		Long idShoppingList = uhsl.getShoppingListIdshoppingList();
		save(idShoppingList, requestBody.getProductId(), Boolean.FALSE, null);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

	private void save(Long shoppinglistId, Long productId, Boolean done, Date removeTime) {
		if (productId < 0) {
			// Aggiungo un nuovo prodotto in "product" perchè ho scelto un modello
			productId = createProductFromTemplate(-productId);
		}
		ShoppingListHasProductRecord r = new ShoppingListHasProductRecord();
		r.setShoppingListIdshoppingList(shoppinglistId);
		r.setProductIdproduct(productId);
		r.setDone(done);
		r.setRemovetime(removeTime);
		new ShoppingListHasProductPersistenceJdbc().save(r);
	}

	private void delete(Long shoppinglistId, Long productId) {
		ShoppingListHasProductPersistence persistence = new ShoppingListHasProductPersistenceJdbc();
		ShoppingListHasProductRecord r = persistence.findByIdWithDoneToFalse(shoppinglistId, productId);
		r.setRemovetime(new Date());
		r.setDone(Boolean.TRUE);
		persistence.update(r);
	}

	private Long createProductFromTemplate(Long templateId) {
		ProductTemplateRecord template = new ProductTemplatePersistenceJdbc().findById(templateId);
		ProductRecord product = new ProductRecord();
		ProductPersistenceJdbc ProductPersist = new ProductPersistenceJdbc();
		product.setName(template.getName());
		product.setNote(template.getDescription());
		product.setLogo(template.getLogo());
		product.setImage(template.getLogo());
		product.setProductTemplateIdproductTemplate(template.getIdproductTemplate());
		return ProductPersist.insert(product);
	}
}
