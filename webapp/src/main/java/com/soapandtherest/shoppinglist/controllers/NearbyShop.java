package com.soapandtherest.shoppinglist.controllers;

import com.google.maps.model.PlacesSearchResult;
import com.soapandtherest.shoppinglist.data.maps.Maps;
import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.mail.MailSendingUtil;
import com.soapandtherest.shoppinglist.util.sharing.SharingInterface;
import com.soapandtherest.shoppinglist.util.sharing.SharingInterfaceFactory;
import com.soapandtherest.shoppinglist.view.mail.Shop;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.velocity.VelocityContext;
import org.slf4j.LoggerFactory;

@Path("/nearbyshop")
public class NearbyShop {
	private static org.slf4j.Logger logger = LoggerFactory.getLogger(NearbyShop.class);

	private static final String POSITION_ATTRIBUTE_KEY = "last_checked_position";
	private static final UserHasShoppingListPersistence persistenceService = new UserHasShoppingListPersistenceJdbc();
	private static final String SUBJECT_GEOLOCATION = "You need to buy something, you can buy theirs into this shops nearby you...";
	private static final String BASE_URL_GOOGLE_MAPS_SEARCH_BY_PLACE_ID = "https://www.google.com/maps/place/?q=place_id:";

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse nearbyShop(com.soapandtherest.shoppinglist.view.request.NearbyShop nearbyShop,
			@Context HttpServletRequest request) {
		Object attribute = request.getSession().getAttribute(POSITION_ATTRIBUTE_KEY);
		if (attribute != null) {
			if (attribute instanceof com.soapandtherest.shoppinglist.view.request.NearbyShop) {
				if (nearbyShop.distance((com.soapandtherest.shoppinglist.view.request.NearbyShop) attribute) <= 500) {
					return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE,
							StandardResponse.VALID_MESSAGE_RESPONSE);
				}
			}
		}
		attribute = request.getSession().getAttribute("iduser");
		Long idUser = null;
		if (attribute != null) {
			if (attribute instanceof Long) {
				idUser = (Long) attribute;
			} else {
				throw new ClassCastException("Attribute recovery isn't a Long");
			}
		} else {
			logger.error("Attribute iduser can't be null");
			return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
		}
		attribute = request.getSession().getAttribute("email");
		String userMail = null;
		if (attribute != null) {
			if (attribute instanceof String) {
				userMail = (String) attribute;
			} else {
				throw new ClassCastException("Attribute recovery isn't a String");
			}
		} else {
			logger.error("Attribute email can't be null");
			return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
		}
		SharingInterface sharing = SharingInterfaceFactory.BuildSharingInterface();
		List<String> types = persistenceService.findShoppingListTypesByUserId(idUser);
		if (types.size() == 0) {
			logger.info("No types for the shopping list");
			return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
		}
		List<PlacesSearchResult> places = new LinkedList<>();
		PlacesSearchResult[] placesResult = Maps.nearbyShop(nearbyShop.getLat(), nearbyShop.getLon(), types);
		if (placesResult.length == 0) {
			logger.info("No place result");
			return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
		}
		List<Shop> shops = new LinkedList<>();
		for (PlacesSearchResult result : placesResult) {
			Shop shop = new Shop(result.name, result.types[0], BASE_URL_GOOGLE_MAPS_SEARCH_BY_PLACE_ID + result.placeId,
					result.icon.toString());
			shops.add(shop);
		}
		VelocityContext context = new VelocityContext();
		context.put("shopList", shops);
		MailSendingUtil.mailSending(userMail, SUBJECT_GEOLOCATION, "geolocation", context);
		request.getSession().setAttribute(POSITION_ATTRIBUTE_KEY, nearbyShop);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}
}
