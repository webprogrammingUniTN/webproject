/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserSession;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Gionni_2
 */

@Path("/currentlist")
public class CurrentList {

	@Context
	private HttpServletRequest request;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public StandardResponse SetList(@PathParam("id") Long idShoppingList) {
		UserSession.setCurrentList(request, idShoppingList);
		return new StandardResponse(StandardResponse.VALID_CODE_RESPONSE, StandardResponse.VALID_MESSAGE_RESPONSE);
	}

}
