/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.UserHasShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.UserHasShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.ShoppingList;
import com.soapandtherest.shoppinglist.util.UserSession;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mousl
 */
@Path("/allshoppinglists")
public class AllShoppingLists {
	@Context
	private HttpServletRequest request;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR})
	public ShoppingList[] getAllShoppingLists() {

		ShoppingListPersistence persistenceService = new ShoppingListPersistenceJdbc();
		UserHasShoppingListPersistence userHasShoppingListPersistenceService = new UserHasShoppingListPersistenceJdbc();
		List<UserHasShoppingListRecord> userLists = userHasShoppingListPersistenceService
				.findByUserId(UserSession.getIdUser(request));
		List<ShoppingListRecord> userRecords = new LinkedList<>();

		for (int i = 0; i < userLists.size(); i++) {
			userRecords.add(persistenceService.findById(userLists.get(i).getShoppingListIdshoppingList()));
		}

		List<ShoppingList> shoppingListIdList = new LinkedList<>();
		for (int i = 0; i < userRecords.size(); i++) {
			shoppingListIdList.add(new ShoppingList(userRecords.get(i), userLists.get(i)));
		}

		ShoppingList[] shoppingListIdArray = new ShoppingList[shoppingListIdList.size()];
		return shoppingListIdList.toArray(shoppingListIdArray);
	}
}
