/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.ShoppingList;
import com.soapandtherest.shoppinglist.util.UserList;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vbeatrice
 */
@Path("/shoppinglistinfo")
public class ShoppingListInfo {
	@Context
	private HttpServletRequest request;
	private final long VALUE = -1;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR})
	public ShoppingList getShoppingListInfo(@PathParam("id") Long id) {
		ShoppingListPersistence persistenceService = new ShoppingListPersistenceJdbc();
		ShoppingListRecord record = persistenceService.findById(id);
		/*
		 * try { UserHasShoppingListRecord userRecord =
		 * UserList.userHasShoppingList(this.request, this.VALUE); record =
		 * persistenceService.findById(userRecord.getShoppingListIdshoppingList()); }
		 * catch (NullPointerException npe) { System.out.println(request.toString()); }
		 */
		ShoppingList sp = new ShoppingList(record);
		return sp;
	}
}