/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ProductPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ProductTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListHasProductPersistence;
import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListHasProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductRecord;
import com.soapandtherest.shoppinglist.data.record.ProductTemplateRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListHasProductRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.Product;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
/**
 *
 * @author vbeatrice
 */
@Path("/productlist")
public class ProductList {
	private final int PRODUCTS_PREVIEW = 5;

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR})
	public Product[] getProductList(@PathParam("id") Long id) {
		ShoppingListHasProductPersistence persistenceService = new ShoppingListHasProductPersistenceJdbc();
		ProductPersistence productPersistenceService = new ProductPersistenceJdbc();

		List<ShoppingListHasProductRecord> productRecord = persistenceService.findByShoppingListId(id);

		ProductRecord productRecords;
		List<Product> productList = new LinkedList<>();
		// need tp get each list product ID
		for (int i = 0; i < productRecord.size(); i++) {
			if (productRecord.get(i).getDone() != true) {
				productRecords = productPersistenceService.findById(productRecord.get(i).getProductIdproduct());
				productList.add(new Product(productRecords));
			}
		}

		List<Product> productListPreview = new LinkedList<>();
		if (productList.size() >= PRODUCTS_PREVIEW) {
			for (int i = 0; i < PRODUCTS_PREVIEW; i++) {
				productListPreview.add(productList.get(i));
			}
		} else {
			if (!productList.isEmpty()) {
				for (int i = 0; i < productList.size(); i++) {
					productListPreview.add(productList.get(i));
				}
			}
		}

		Product[] productArray = new Product[productListPreview.size()];
		return productListPreview.toArray(productArray);
	}
}
