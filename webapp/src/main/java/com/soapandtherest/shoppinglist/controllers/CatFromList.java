package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.file.Image;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductCategoriesPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ProductTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListTemplateHasProductCategoriesPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ProductCategoriesRecord;
import com.soapandtherest.shoppinglist.data.record.ProductRecord;
import com.soapandtherest.shoppinglist.data.record.ProductTemplateRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateHasProductCategoriesRecord;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateRecord;
import com.soapandtherest.shoppinglist.data.record.UserHasShoppingListRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.util.UserList;
import com.soapandtherest.shoppinglist.view.response.CatFromListResponse;
import com.soapandtherest.shoppinglist.view.response.Item;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/catfromlist")
public class CatFromList {

	static final boolean INCLUDE_PRODUCTS = false;
	static final boolean INCLUDE_IMAGES = false;

	@Context
	private HttpServletRequest request;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	@RolesAllowed({AuthRoles.ANONYMOUS, AuthRoles.STANDARD, AuthRoles.ADMINISTRATOR})
	public CatFromListResponse GetCatList(@PathParam("id") Long idShoppingList) {

		// Se idShoppingList = -1 allora si usa la CurrentList di sessione
		UserHasShoppingListRecord uhsl = UserList.userHasShoppingList(request, idShoppingList);
		if (uhsl == null)
			return new CatFromListResponse(0, "Non hai accesso alla lista.");

		idShoppingList = uhsl.getShoppingListIdshoppingList();

		ShoppingListRecord sl = getShoppingList(idShoppingList);
		if (sl == null)
			return new CatFromListResponse(0, "ShoppingList not found.");

		ShoppingListTemplateRecord template = getShoppingListTemplate(sl.getTemplateId());
		if (template == null)
			return new CatFromListResponse(0, "ShoppingListTemplate not found.");

		Long[] catList = getslCat(sl.getTemplateId());
		if (catList == null)
			return new CatFromListResponse(0, "ShoppingListTemplateHasProductCategories not found.");

		String[] catNames = getCatNames(catList);
		if (catNames == null)
			return new CatFromListResponse(0, "ProductCategories not found.");

		List<ProductTemplateRecord> temp = getProductTemplates(catList);
		if (temp == null)
			return new CatFromListResponse(0, "ProductTemplate not found.");
		Long[] productTemplateIds = new Long[temp.size()];
		String[] productTemplateNames = new String[temp.size()];
		Long[] productTemplateCatIds = new Long[temp.size()];
		String[] productTemplateCatNames = new String[temp.size()];
		int i = 0;
		for (Iterator<ProductTemplateRecord> itr = temp.listIterator(); itr.hasNext(); i++) {
			ProductTemplateRecord p = itr.next();
			productTemplateIds[i] = p.getIdproductTemplate();
			productTemplateNames[i] = p.getName();
			productTemplateCatIds[i] = p.getProductCategoriesIdproductCategories();
			int idx = 0;
			for (int y = 0; y < catList.length; y++)
				if (catList[y].equals(productTemplateCatIds[i]))
					idx = y;
			productTemplateCatNames[i] = catNames[idx];
		}

		Item[] items;
		int itemNo = 0;
		CatFromListResponse result = new CatFromListResponse();

		if (INCLUDE_PRODUCTS) {
			List<ProductRecord> prod = getProducts(productTemplateIds);
			if (prod == null)
				return new CatFromListResponse(0, "Product not found.");
			Long[] productIds = new Long[prod.size()];
			String[] productNames = new String[prod.size()];
			Long[] productCatIds = new Long[prod.size()];
			String[] productCatNames = new String[prod.size()];
			items = new Item[prod.size() + temp.size()];

			// Inserisco gli item dei product
			for (Iterator<ProductRecord> itr = prod.listIterator(); itr.hasNext(); itemNo++) {
				ProductRecord p = itr.next();
				productIds[itemNo] = p.getIdproduct();
				productNames[itemNo] = p.getName();
				Long templateId = p.getProductTemplateIdproductTemplate();
				int idx = 0;
				for (int y = 0; y < productTemplateIds.length; y++)
					if (productTemplateIds[y].equals(templateId))
						idx = y;
				productCatIds[itemNo] = productTemplateCatIds[idx];
				productCatNames[itemNo] = productTemplateCatNames[idx];
				items[itemNo] = new Item();
				items[itemNo].setId(p.getIdproduct());
				items[itemNo].setCat(productTemplateCatNames[idx]);
				items[itemNo].setName(p.getName());
				items[itemNo].setDescription(p.getNote());

				if (INCLUDE_IMAGES) {
					com.soapandtherest.shoppinglist.view.File[] image = new com.soapandtherest.shoppinglist.view.File[1];
					image[0] = new com.soapandtherest.shoppinglist.view.File(
							new Image(p.getImage()).GetPresignedURL(Image.ImageSize.SMALL), p.getImage());
					items[itemNo].setImage(image);
				}

				result.setProductIds(productIds);
				result.setProductNames(productNames);
				result.setProductCatIds(productCatIds);
				result.setProductCatNames(productCatNames);
			}

		} else
			items = new Item[temp.size()];

		// Inserisco gli item dei product_template
		i = 0;
		for (Iterator<ProductTemplateRecord> itr = temp.listIterator(); itr.hasNext(); itemNo++, i++) {
			ProductTemplateRecord p = itr.next();
			items[itemNo] = new Item();
			items[itemNo].setId(-productTemplateIds[i]);
			items[itemNo].setCat(productTemplateCatNames[i]);
			if (INCLUDE_PRODUCTS)
				items[itemNo].setName("(template) - ".concat(productTemplateNames[i]));
			else
				items[itemNo].setName(productTemplateNames[i]);

			items[itemNo].setDescription(p.getDescription());

			if (INCLUDE_IMAGES) {
				com.soapandtherest.shoppinglist.view.File[] image = new com.soapandtherest.shoppinglist.view.File[1];
				image[0] = new com.soapandtherest.shoppinglist.view.File(
						new Image(p.getLogo()).GetPresignedURL(Image.ImageSize.SMALL), p.getLogo());
				items[itemNo].setImage(image);
			}
		}

		result.setSlId(idShoppingList);
		result.setSlName(sl.getName());
		result.setSlTemplateId(sl.getTemplateId());
		result.setSlTemplateName(template.getName());
		result.setCatIds(catList);
		result.setCatNames(catNames);
		result.setProductTemplateIds(productTemplateIds);
		result.setProductTemplateNames(productTemplateNames);
		result.setProductTemplateCatIds(productTemplateCatIds);
		result.setProductTemplateCatNames(productTemplateCatNames);
		result.setItems(items);
		return result;
	}

	private ShoppingListRecord getShoppingList(Long id) {
		return new ShoppingListPersistenceJdbc().findById(id);
	}

	private ShoppingListTemplateRecord getShoppingListTemplate(Long id) {
		return new ShoppingListTemplatePersistenceJdbc().findById(id);
	}

	private Long[] getslCat(Long idSl) {
		List<ShoppingListTemplateHasProductCategoriesRecord> cat = new ShoppingListTemplateHasProductCategoriesPersistenceJdbc()
				.findAll();
		if (cat == null)
			return null;
		cat.removeIf(p -> !p.getShoppingListTemplateIdshoppingListTemplate().equals(idSl));
		Long[] result = new Long[cat.size()];
		int i = 0;
		for (Iterator<ShoppingListTemplateHasProductCategoriesRecord> itr = cat.listIterator(); itr.hasNext(); i++)
			result[i] = itr.next().getProductCategoriesIdproductCategories();
		return result;
	}

	private String[] getCatNames(Long[] id) {
		List<ProductCategoriesRecord> cat = new ProductCategoriesPersistenceJdbc().findAll();
		if (cat == null)
			return null;
		String[] result = new String[id.length];
		int i = 0;
		for (Iterator<ProductCategoriesRecord> itr = cat.listIterator(); itr.hasNext();) {
			ProductCategoriesRecord p = itr.next();
			for (Long id1 : id) {
				if (id1.equals(p.getIdproductCategories()))
					result[i++] = p.getName();
			}
		}
		return result;
	}

	private List<ProductTemplateRecord> getProductTemplates(Long[] ids) {
		List<ProductTemplateRecord> temp = new ProductTemplatePersistenceJdbc().findAll();
		if (temp == null)
			return null;
		int i = 0;
		for (Iterator<ProductTemplateRecord> itr = temp.listIterator(); itr.hasNext();) {
			ProductTemplateRecord p = itr.next();
			boolean found = false;
			for (Long id1 : ids)
				if (id1.equals(p.getProductCategoriesIdproductCategories()))
					found = true;
			if (!found)
				itr.remove();
		}
		return temp;
	}

	private List<ProductRecord> getProducts(Long[] ids) {
		List<ProductRecord> prod = new ProductPersistenceJdbc().findAll();
		if (prod == null)
			return null;
		int i = 0;
		for (Iterator<ProductRecord> itr = prod.listIterator(); itr.hasNext();) {
			ProductRecord p = itr.next();
			boolean found = false;
			for (Long id1 : ids)
				if (id1.equals(p.getProductTemplateIdproductTemplate()))
					found = true;
			if (!found)
				itr.remove();
		}
		return prod;
	}
}
