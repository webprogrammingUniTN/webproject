/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.MessageTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.MessageTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.MessageTemplateRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.MessageTemplate;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mousl
 */
@Path("/messagetemplatelist")
public class MessageTemplateList {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD})
	public MessageTemplate[] allMessageList() {
		MessageTemplatePersistence persistenceService = new MessageTemplatePersistenceJdbc();
		List<MessageTemplateRecord> templateRecord = persistenceService.findAll();
		List<MessageTemplate> templateList = new LinkedList<>();
		for (MessageTemplateRecord record : templateRecord) {
			templateList.add(new MessageTemplate(record));
		}
		MessageTemplate[] templatesArray = new MessageTemplate[templateList.size()];
		return templateList.toArray(templatesArray);
	}
}
