/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListTemplatePersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListTemplatePersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListTemplateRecord;
import com.soapandtherest.shoppinglist.view.ShoppingListTemplate;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.ShoppingListTemplate;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vbeatrice
 */
@Path("/templatelistinfo")
public class TemplateList {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.ADMINISTRATOR, AuthRoles.STANDARD, AuthRoles.ANONYMOUS})
	public ShoppingListTemplate[] allTemplateList() {

		ShoppingListTemplatePersistence persistenceService = new ShoppingListTemplatePersistenceJdbc();
		List<ShoppingListTemplateRecord> templateRecord = persistenceService.findAll();
		List<ShoppingListTemplate> templateList = new LinkedList<>();
		for (ShoppingListTemplateRecord record : templateRecord) {
			templateList.add(new ShoppingListTemplate(record));
		}
		ShoppingListTemplate[] templatesArray = new ShoppingListTemplate[templateList.size()];
		return templateList.toArray(templatesArray);

	}
}
