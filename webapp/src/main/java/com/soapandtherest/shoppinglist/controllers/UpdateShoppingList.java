/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.soapandtherest.shoppinglist.controllers;

import com.soapandtherest.shoppinglist.data.persistence.ShoppingListPersistence;
import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.ShoppingListPersistenceJdbc;
import com.soapandtherest.shoppinglist.data.record.ShoppingListRecord;
import com.soapandtherest.shoppinglist.filters.AuthRoles;
import com.soapandtherest.shoppinglist.filters.RolesAllowed;
import com.soapandtherest.shoppinglist.view.ShoppingList;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
/**
 *
 * @author vbeatrice
 */
@Path("/updateshoppinglist")
public class UpdateShoppingList {
	private static ShoppingListPersistence persistenceService = new ShoppingListPersistenceJdbc();
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@RolesAllowed({AuthRoles.STANDARD, AuthRoles.ANONYMOUS, AuthRoles.ADMINISTRATOR})
	public boolean updateShoppingList(ShoppingList data, @Context HttpServletRequest request) {
		String error = data.isValid(request.getSession());
		if (!error.isEmpty()) {
			throw new IllegalArgumentException(error);
		}

		ShoppingListRecord record = new ShoppingListRecord();
		record.setIdshoppingList(data.getId());
		record.setTemplateId(data.getTemplateId());
		record.setName(data.getName());
		record.setDescription(data.getDescription());
		if (data.getFile().isUploadFile()) {
			data.getFile().CompleteUpload();
		}
		record.setImage(data.getFile().getObjectKey());
		persistenceService.update(record);
		return true;
	}
}
