package com.soapandtherest.shoppinglist.errorhandling;

import com.google.gson.JsonSyntaxException;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class MalformedJsonExceptionMapper implements ExceptionMapper<JsonSyntaxException> {
	private static final Logger logger = LogManager.getLogger(MalformedJsonExceptionMapper.class.getName());

	@Override
	public Response toResponse(JsonSyntaxException e) {
		logger.info("Wrong JSON ", e);
		return Response.status(Status.BAD_REQUEST)
				.entity(new StandardResponse(StandardResponse.INVALID_FORMAT_CODE, "Invalid JSON"))
				.type(MediaType.APPLICATION_JSON).build();
	}
}
