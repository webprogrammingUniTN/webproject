package com.soapandtherest.shoppinglist.errorhandling;

import com.soapandtherest.shoppinglist.data.persistence.impl.jdbc.commons.GenericJdbcDAO;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {
	private static final Logger logger = LogManager.getLogger(IllegalArgumentExceptionMapper.class.getName());

	@Override
	public Response toResponse(IllegalArgumentException e) {
		logger.info("Illegal argument exception mapped: ", e);
		return Response.status(400).entity(new StandardResponse(StandardResponse.INVALID_FORMAT_CODE, e.getMessage()))
				.type(MediaType.APPLICATION_JSON).build();
	}
}
