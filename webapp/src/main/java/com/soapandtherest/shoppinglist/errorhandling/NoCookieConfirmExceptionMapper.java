package com.soapandtherest.shoppinglist.errorhandling;

import com.soapandtherest.shoppinglist.exception.NoCookieConfirmException;
import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider
public class NoCookieConfirmExceptionMapper implements ExceptionMapper<NoCookieConfirmException> {
	private static final Logger logger = LogManager.getLogger(MalformedJsonExceptionMapper.class.getName());

	@Override
	public Response toResponse(NoCookieConfirmException e) {
		logger.info("User haven't set cookie ", e);
		return Response.status(Status.BAD_REQUEST)
				.entity(new StandardResponse(StandardResponse.UNAUTHORIZED_FORMAT_CODE, "Please confirm use of cookie"))
				.type(MediaType.APPLICATION_JSON).build();
	}
}
