package com.soapandtherest.shoppinglist.errorhandling;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UncaughtExceptionMapper extends Throwable implements ExceptionMapper<Throwable> {
	private static final Logger logger = LogManager.getLogger(UncaughtExceptionMapper.class.getName());

	@Override
	public Response toResponse(Throwable exception) {
		logger.error("Uncaught exception ", exception);
		return Response.status(500).entity("Something bad happened. Please try again !!").type("text/plain").build();
	}
}
