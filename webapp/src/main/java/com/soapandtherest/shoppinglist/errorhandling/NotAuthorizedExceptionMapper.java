package com.soapandtherest.shoppinglist.errorhandling;

import com.soapandtherest.shoppinglist.view.response.StandardResponse;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Provider

public class NotAuthorizedExceptionMapper implements ExceptionMapper<NotAuthorizedException> {
	private static final Logger LOGGER = LogManager.getLogger(NotAuthorizedExceptionMapper.class.getName());

	@Override
	public Response toResponse(NotAuthorizedException e) {
		LOGGER.info("User can't access to request resource ", e);
		return Response.status(Status.FORBIDDEN).entity(new StandardResponse(StandardResponse.UNAUTHORIZED_FORMAT_CODE,
				"You can't execute requested operation")).type(MediaType.APPLICATION_JSON).build();
	}
}
