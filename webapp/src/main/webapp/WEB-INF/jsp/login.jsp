<%--
  Created by IntelliJ IDEA.
  User: bortolonmatteo
  Date: 14/05/2018
  Time: 09:37
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/webpack.tld" %>
<html>
  <head>
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <custom:ManifestResource filename="login.css"/>
  </head>
  <body>
    <custom:ApplicationParameter/>
    <div id="app"></div>
    <custom:ManifestResource filename="manifest.js"/>
    <custom:ManifestResource filename="vendor.js"/>
    <custom:ManifestResource filename="login.js"/>
  </body>
</html>
