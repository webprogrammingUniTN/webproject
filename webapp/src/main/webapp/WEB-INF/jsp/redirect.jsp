<%--
  Created by IntelliJ IDEA.
  User: bortolonmatteo
  Date: 29/06/2018
  Time: 19:28
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Redirect</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
  <c:redirect url = "${map.url}"/>
  <h1><a href="<c:out value = "${map.url}"/>">Click here...</a></h1>
</body>
</html>
