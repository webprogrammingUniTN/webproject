<%--
  Created by IntelliJ IDEA.
  User: bortolonmatteo
  Date: 14/05/2018
  Time: 09:33
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="custom" uri="/WEB-INF/tlds/webpack.tld" %>
<html>
  <head>
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <custom:ManifestResource filename="admin.css"/>
    <link rel="apple-touch-icon" sizes="57x57" href="${pageContext.request.contextPath}/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="${pageContext.request.contextPath}/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="${pageContext.request.contextPath}/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="${pageContext.request.contextPath}/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="${pageContext.request.contextPath}/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="${pageContext.request.contextPath}/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="${pageContext.request.contextPath}/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/favicon/apple-icon-180x180.png">
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/favicon/apple-touch-icon-precomposed.png">
    <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/favicon/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="36x36" href="${pageContext.request.contextPath}/favicon/android-icon-36x36.png">
    <link rel="icon" type="image/png" sizes="48x48" href="${pageContext.request.contextPath}/favicon/android-icon-48x48.png">
    <link rel="icon" type="image/png" sizes="72x72" href="${pageContext.request.contextPath}/favicon/android-icon-72x72.png">
    <link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/favicon/android-icon-96x96.png">
    <link rel="icon" type="image/png" sizes="144x144" href="${pageContext.request.contextPath}/favicon/android-icon-144x144.png">
    <link rel="icon" type="image/png" sizes="192x192" href="${pageContext.request.contextPath}/favicon/android-icon-192x192.png">
    <link rel="icon" href="${pageContext.request.contextPath}/favicon/favicon.ico" type="image/x-icon">
  </head>
  <body>
    <custom:ApplicationParameter/>
    <div id="app"></div>
    <custom:ManifestResource filename="manifest.js"/>
    <custom:ManifestResource filename="vendor.js"/>
    <custom:ManifestResource filename="admin.js"/>
  </body>
</html>
