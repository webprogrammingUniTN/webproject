'use strict';
// Template version: 1.3.1
// see http://vuejs-templates.github.io/webpack for documentation.

const path = require('path');

module.exports = {
  dev: {
    // Paths
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {
      "/messaging": {
        target: "ws://localhost:8080/shoppinglist",
        ws: true,
      },
      "/shoppinglist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/updateshoppinglist": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/messagetemplatelist": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/templatelistinfo": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/template": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/files": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/saveshoppinglist": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/templatelist": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/allproductlist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/senderlist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/shoppinglistinfo/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/nearbyshop": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/login": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/signon": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/signin": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/signout": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/userimage": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/productimage": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/productnamenoteupdate": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/productlist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/currentlist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/banner": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/enter": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/allshoppinglists": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/orderbycategory/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/orderbyalphabet/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/deleteshoppinglist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/messagelist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/catfromlist/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/template/**": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/category": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/productinlist": {
        target: "http://localhost:8080/shoppinglist",
      },
      "/currentiduser": {
        target: "http://localhost:8080/shoppinglist",
      }
    },

    addHtmlExtensionToRequestThatIsEqualTo: [
      'login',
      'admin',
    ],

    // Various Dev Server settings
    host: 'localhost', // can be overwritten by process.env.HOST
    port: 1616, // can be overwritten by process.env.PORT, if port is in use, a free one will be determined
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-

    // Use Eslint Loader?
    // If true, your code will be linted during bundling and
    // linting errors and warnings will be shown in the console.
    useEslint: true,
    // If true, eslint errors and warnings will also be shown in the error overlay
    // in the browser.
    showEslintErrorsInOverlay: true,

    /**
     * Source Maps
     */

    // https://webpack.js.org/configuration/devtool/#development
    devtool: 'cheap-module-eval-source-map',

    // If you have problems debugging vue-files in devtools,
    // set this to false - it *may* help
    // https://vue-loader.vuejs.org/en/options.html#cachebusting
    cacheBusting: true,

    cssSourceMap: true
  },

  build: {
    createHTMLPage: {
      home: path.resolve(__dirname, '../target/javascriptDist/index.html'),
      login: path.resolve(__dirname, '../target/javascriptDist/login.html'),
      admin: path.resolve(__dirname, '../target/javascriptDist/admin.html'),
    },

    // Paths
    assetsRoot: path.resolve(__dirname, '../target/shoppinglist/'),
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',

    /**
     * Source Maps
     */

    productionSourceMap: true,
    // https://webpack.js.org/configuration/devtool/#production
    devtool: '#source-map',

    // Gzip off by default as many popular static hosts such as
    // Surge or Netlify already gzip all static assets for you.
    // Before setting to `true`, make sure to:
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],

    // Run the build command with an extra argument to
    // View the bundle analyzer report after build finishes:
    // `npm run build --report`
    // Set to `true` or `false` to always turn it on or off
    bundleAnalyzerReport: process.env.npm_config_report
  }
};
